﻿class Recommendation extends ZnodeBase {
    GenerateRecommendationData(portalId: number, isBuildPartial: string): any {
        $.ajax({
            url: "/Recommendation/GenerateRecommendationData?portalId=" + portalId + "&isBuildPartial=" + isBuildPartial,
            type: 'POST',
            success: function (response) {
                if (!response.hasError) {                    
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, "success", isFadeOut, fadeOutTime);
                }
                else {
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, "error", isFadeOut, fadeOutTime);
                }
            }
        });
    }
}