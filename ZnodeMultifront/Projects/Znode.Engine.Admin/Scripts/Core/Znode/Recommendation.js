var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Recommendation = /** @class */ (function (_super) {
    __extends(Recommendation, _super);
    function Recommendation() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Recommendation.prototype.GenerateRecommendationData = function (portalId, isBuildPartial) {
        $.ajax({
            url: "/Recommendation/GenerateRecommendationData?portalId=" + portalId + "&isBuildPartial=" + isBuildPartial,
            type: 'POST',
            success: function (response) {
                if (!response.hasError) {
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, "success", isFadeOut, fadeOutTime);
                }
                else {
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, "error", isFadeOut, fadeOutTime);
                }
            }
        });
    };
    return Recommendation;
}(ZnodeBase));
//# sourceMappingURL=Recommendation.js.map