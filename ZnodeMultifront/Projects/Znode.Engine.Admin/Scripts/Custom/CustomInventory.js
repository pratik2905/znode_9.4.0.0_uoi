var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var CustomInventory = /** @class */ (function (_super) {
    __extends(CustomInventory, _super);
    function CustomInventory() {
        return _super.call(this) || this;
    }
    CustomInventory.prototype.Init = function () {
    };
    CustomInventory.prototype.ValidateAndPost = function () {
        if (CustomInventory.prototype.ValidateModel()) {
            CustomInventory.prototype.CreateAndPostModel();
            return false;
        }
        else {
            return false;
        }
    };
    CustomInventory.prototype.ValidateModel = function () {
        if (CustomInventory.prototype.ValidateImportFileType()) {
            return true;
        }
        else {
            return false;
        }
    };
    CustomInventory.prototype.CreateAndPostModel = function () {
        var ImportViewModels = CustomInventory.prototype.CreateModel();
        CustomInventory.prototype.PostData(ImportViewModels);
    };
    CustomInventory.prototype.ValidateImportFileType = function () {
        if ($("#txtUpload").val() != "") {
            if ($("#fileName").html().split(".")[1] == "csv") {
                $("#error-file-upload").html("");
                return true;
            }
            else {
                $("#error-file-upload").html(ZnodeBase.prototype.getResourceByKeyName("SelectCSVFileError"));
                return false;
            }
        }
        else {
            $("#error-file-upload").html(ZnodeBase.prototype.getResourceByKeyName("FileNotPresentError"));
            return false;
        }
    };
    CustomInventory.prototype.CreateModel = function () {
        var ImportViewModels = {
            CountryCode: $("input[name='SKU']").val(),
            FileName: $("#ChangedFileName").val(),
        };
        return ImportViewModels;
    };
    CustomInventory.prototype.PostData = function (importModel) {
        ZnodeBase.prototype.ShowLoader();
        CustomEndpoint.prototype.UploadkeysPost(importModel, function (res) {
            setTimeout(function () {
                ZnodeBase.prototype.HideLoader();
                window.location.href = $("#ReturnUrl").val();
            }, 900);
        });
    };
    return CustomInventory;
}(ZnodeBase));
//# sourceMappingURL=CustomInventory.js.map