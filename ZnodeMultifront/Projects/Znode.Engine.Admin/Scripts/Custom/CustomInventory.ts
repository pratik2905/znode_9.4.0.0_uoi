﻿class CustomInventory extends ZnodeBase {
    _endPoint: Endpoint;

    constructor() {
        super();
    }

    Init() {
    }

    ValidateAndPost(): boolean {
      
        if (CustomInventory.prototype.ValidateModel()) {
            CustomInventory.prototype.CreateAndPostModel();
            return false;
        } else {
            return false;
        }
    }
    ValidateModel(): boolean {
        if (CustomInventory.prototype.ValidateImportFileType()) {
            return true;
        } else {
            return false;
        }
    }
    CreateAndPostModel(): any {
        var ImportViewModels = CustomInventory.prototype.CreateModel();
        CustomInventory.prototype.PostData(ImportViewModels);
    }
    ValidateImportFileType(): boolean {
        if ($("#txtUpload").val() != "") {
            if ($("#fileName").html().split(".")[1] == "csv") {
                $("#error-file-upload").html("");
                return true;
            }
            else {
                $("#error-file-upload").html(ZnodeBase.prototype.getResourceByKeyName("SelectCSVFileError"));
                return false;
            }
        } else {
            $("#error-file-upload").html(ZnodeBase.prototype.getResourceByKeyName("FileNotPresentError"));
            return false;
        }
    }
    CreateModel(): any {
        var ImportViewModels = {
            CountryCode: $("input[name='SKU']").val(),
            FileName: $("#ChangedFileName").val(),
        }
        return ImportViewModels;
    }
    PostData(importModel): any {
        ZnodeBase.prototype.ShowLoader();
        CustomEndpoint.prototype.UploadkeysPost(importModel, function (res) {
            setTimeout(function () {
                ZnodeBase.prototype.HideLoader();
                window.location.href = $("#ReturnUrl").val();
            }, 900);
        });
    }

}