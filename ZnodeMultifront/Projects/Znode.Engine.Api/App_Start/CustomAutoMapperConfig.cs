﻿using AutoMapper;
using Znode.Custom.Data;
using Znode.Engine.Api.Model.Custom;

namespace Znode.Engine.Api
{
    public static class CustomAutoMapperConfig
    {
        public static void Execute()
        {
            Mapper.CreateMap<ZnodeCustomPortalDetail, CustomPortalDetailModel>().ReverseMap();
        }
    }
}