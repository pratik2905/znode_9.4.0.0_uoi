﻿using Illinois.Libraries.iPay;
using System;
using System.Web.Mvc;
using Znode.Engine.Api.Model.Custom;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.Framework.Business;
using Znode.WebStore.Custom.Agents.Agents;

namespace Znode.Engine.WebStore.Controllers
{
    public class CustomCheckoutController : CheckoutController
    {
        const string SubmitOrderModelKey = "SUBMIT_ORDER_MODEL_KEY";
        IPaymentAgent _paymentAgent;
        ICheckoutAgent _checkoutAgent;
        ICartAgent _cartAgent;
        iPayAgent _ipayAgent;


        public CustomCheckoutController(IUserAgent userAgent, ICheckoutAgent checkoutAgent, ICartAgent cartAgent, IPaymentAgent paymentAgent) : 
            base(userAgent, checkoutAgent, cartAgent, paymentAgent)
        {
            _paymentAgent = paymentAgent;
            _checkoutAgent = checkoutAgent;
            _cartAgent = cartAgent;
            _ipayAgent = new iPayAgent();
        }

        //[HttpGet]
        //public override ActionResult GetPaymentDetails(int paymentSettingId)
        //{
        //    return Json(_paymentAgent.GetPaymentDetails(paymentSettingId), JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public virtual JsonResult ProcessIPayPayment(SubmitOrderViewModel orderpaymentmodel)
        //{
        //    //Write custom logic, set up iPay and return redirect
        //    PaymentSettingViewModel iPaySetting = _paymentAgent.GetPaymentSetting(orderpaymentmodel.PaymentApplicationSettingId);

        //    iPayGateway iPayGW = new iPayGateway(iPaySetting, orderpaymentmodel);

        //    iPayGW.Register();
            
        //    //Save iPay information
            
            
            
        //    JsonResult returnResult = new JsonResult();

        //    returnResult.Data = iPayGW.RedirectUrl;




        //    return returnResult;
        //}
        //[HttpGet]
        //public virtual ActionResult SubmitIPayOrder(int shippingAddressId, int billingAddressId,
        //int shippingOptionId, int paymentSettingId, string additionalInstruction, string token)
        //{
        //    //Write custom login when control return from your payment gateway and Save Order
        //    // This is the sample code
        //    //Return from iPay here
        //    SubmitOrderViewModel submitOrderViewModel =
        //    _checkoutAgent.SetPayPalToken(token, shippingAddressId, billingAddressId,
        //    shippingOptionId, paymentSettingId, additionalInstruction);
        //    var status = SubmitOrder(submitOrderViewModel);
        //    _cartAgent.RemoveAllCartItems();

        //    //go to after payment action, whatever it is (assuming Receipt)
        //    return RedirectToAction("Receipt");
        //}

        /// <summary>
        /// Override of SubmitOrder that takes custom action for iPay
        /// </summary>
        /// <param name="submitOrderViewModel"></param>
        /// <returns></returns>
        public override ActionResult SubmitOrder(SubmitOrderViewModel submitOrderViewModel)
        {
            try
            {
                //Get store (paymentoption)
                if (submitOrderViewModel.PaymentSettingId != 0)
                {
                    PaymentSettingViewModel PaySetting = _paymentAgent.GetPaymentSetting(submitOrderViewModel.PaymentSettingId);

                    if (PaySetting.PaymentTypeName.ToLower() == "ipay")
                    {
                        //initialize gateway with ipay settings and purchase data
                        iPayGateway iPayGW = new iPayGateway(PaySetting, submitOrderViewModel);

                        iPayGW.Register();

                        //Save iPay information
                        decimal resultAmount = 0;
                        decimal.TryParse(iPayGW.Amount.ToString(), out resultAmount);
                        _ipayAgent.CreateiPayRecord(new iPayRecordModel
                        {
                            Amount = resultAmount,
                            BannerAccount = iPayGW.BannerAccount,
                            CaptureResponseCode = iPayGW.CaptureResponseCode,
                            ReferenceId = iPayGW.ReferenceId,
                            RegistrationResponseCode = iPayGW.RegistrationResponseCode,
                            ResultResponseCode = iPayGW.ResultResponseCode,
                            Testing = iPayGW.IsTestMode,
                            Token = iPayGW.Token,
                            TransactionId = iPayGW.TransactionId,
                            TransactionStartTime = DateTime.Now
                        });


                        submitOrderViewModel.PaymentToken = iPayGW.Token;
                        //Save SOVM for return from iPay
                        Session.Add(iPayGW.Token, submitOrderViewModel);

                        //Go to iPay
                        return Redirect(iPayGW.RedirectUrl);

                    }
                    else return base.SubmitOrder(submitOrderViewModel);
                }
                else return base.SubmitOrder(submitOrderViewModel);
            }
            catch (Exception ex)
            {
                return Content(string.Format("Error: {0}, src: {1}", ex.Message, ex.Source));
                //submitOrderViewModel.HasError = true;
                //submitOrderViewModel.ErrorMessage = ex.Message;
                //return View("Index", submitOrderViewModel);
            }

                
            
            

        }

        /// <summary>
        /// Controller for the return from iPay, it will go to this site /ipay?token=xxxxxxxxxx
        /// </summary>
        /// <param name="token"token passed around referring to this order and this payment</param>
        /// <param name="reason" optional reason returned for non-success returns</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult IPay(string token, string reason)
        {
            string otherError = "Bad Parameters";

            if (!string.IsNullOrEmpty(token))
            {
                try
                {
                    //Get the SOVM to pass into base submitorder controller
                    SubmitOrderViewModel submitOrderViewModel = (SubmitOrderViewModel)Session[token];
                    PaymentSettingViewModel iPaySetting = _paymentAgent.GetPaymentSetting(submitOrderViewModel.PaymentSettingId);
                    //Initialize iPay Gateway with this store/paymentsettings custom values
                    iPayGateway iPayGW = new iPayGateway(iPaySetting, submitOrderViewModel);

                    //Get this payment's log record
                    var ipayRecord = _ipayAgent.GetiPayRecordByToken(token);

                    //Check to see if payment was successful, failed, or cancelled
                    iPayGW.Result();

                    //Record results
                    ipayRecord.ResultResponseCode = iPayGW.ResultResponseCode;
                    ipayRecord.TransactionId = (iPayGW.ResultResponseCode ==0 ? iPayGW.TransactionId : (string.IsNullOrEmpty(reason) ? string.Empty : reason));
                    _ipayAgent.UpdateiPayRecord(ipayRecord);

                    if (iPayGW.ResultResponseCode == 0)  //Success
                    {
                        //Capture funds
                        iPayGW.Capture();

                        //Record results of capture
                        ipayRecord.CaptureResponseCode = iPayGW.CaptureResponseCode;
                        _ipayAgent.UpdateiPayRecord(ipayRecord);
                        if (iPayGW.CaptureResponseCode == 0) //capture success
                        {
                            submitOrderViewModel.PaymentToken = token;

                            //Return control to Znode to complete order submission
                            return base.SubmitOrder(submitOrderViewModel);
                        }
                    }
                }
                catch (Exception ex)
                {
                    otherError = ex.Message;
                    if (ex.InnerException != null) otherError += "ie: " + ex.InnerException.Message;
                }
            }
            //Something went wrong, don't submit order
            token = (string.IsNullOrEmpty(token) ? "Empty" : token);
            reason = (string.IsNullOrEmpty(reason) ? "Empty" : reason);
            ZnodeLogging.LogMessage(string.Format("Error returning from iPay.  token:{0}, reason:{1}, other error:{2}", token, reason, otherError),"iPay");
            return RedirectToAction("Index"); //Need better place to redirect
        }
    }
}
