﻿using System;
using System.Web.Mvc;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using Znode.WebStore.Core.Agents;

namespace Znode.Engine.WebStore.Controllers
{
    public class ShibUserController : UserController
    {
        #region Private Read-only members
        private readonly IAuthenticationHelper _authenticationHelper;
        private readonly IUserAgent _userAgent;
        private readonly ICartAgent _cartAgent;
        private readonly IPaymentAgent _paymentAgent;
        private readonly IImportAgent _importAgent;
        private readonly IFormBuilderAgent _formBuilderAgent;
        private const int CREATED_BY_SHIBBOLETH = -213;
        #endregion

        #region Public Constructor     
        public ShibUserController(IUserAgent userAgent, ICartAgent cartAgent, IAuthenticationHelper authenticationHelper, IPaymentAgent paymentAgent, IImportAgent importAgent, IFormBuilderAgent formBuilderAgent)
       : base(userAgent, cartAgent, authenticationHelper, paymentAgent, importAgent, formBuilderAgent)
        {
            _authenticationHelper = authenticationHelper;
            _userAgent = userAgent;
            _cartAgent = cartAgent;
            _paymentAgent = paymentAgent;
            _importAgent = importAgent;
            _formBuilderAgent = formBuilderAgent;
        }

        #endregion

        [AllowAnonymous]
        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public override ActionResult Login(string returnUrl)
        {
            //ZnodeLogging.LogMessage("In Custom Login controller");

            if (!string.IsNullOrEmpty(Request.Headers["eppn"]))
            {
                //ZnodeLogging.LogMessage("Valid eppn found");
                LogShibInfo();
                LoginViewModel shibModel = new LoginViewModel();
                shibModel.Username = IllinoisHelper.NormalizeEPPN(Request.Headers["eppn"]);
                shibModel.Password = IllinoisHelper.PasswordMixup(shibModel.Username);
                //Mark as shib generated
                shibModel.SuccessMessage = "ShibUser";
                shibModel.CreatedBy = CREATED_BY_SHIBBOLETH;

                //ZnodeLogging.LogMessage("Shibmodel created, passing to Login routine");
                return Login(shibModel, returnUrl);
            }
            return base.Login(returnUrl); //View("Login", new LoginViewModel());
        }

        [AllowAnonymous]
        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult LoginShib(string returnUrl)
        {
            return Login(returnUrl);
        }

        private ActionResult LoginExt(string returnUrl, CustomRegisterViewModel model)
        {
            //ZnodeLogging.LogMessage("In Custom Signup Login controller-");


            LoginViewModel shibModel = new LoginViewModel();
            shibModel.Username = IllinoisHelper.NormalizeEPPN(model.UofIUserName + "@illinois.edu");
            shibModel.Password = IllinoisHelper.PasswordMixup(shibModel.Username);
            //Mark as shib generated
            shibModel.SuccessMessage = "ShibUser";
            shibModel.CreatedBy = CREATED_BY_SHIBBOLETH;
            shibModel.EditParamField = model.Email;

            //ZnodeLogging.LogMessage("Shibmodel created from Registration, passing to Login routine");
            return PerformExtLogin(shibModel, model, returnUrl);

        }

        private ActionResult PerformExtLogin(LoginViewModel loginModel, CustomRegisterViewModel regModel, string returnURL)
        {

            var loginviewModel = _userAgent.Login(loginModel);

            //ZnodeLogging.LogMessage("UserAgent Login executed");

            if (!loginviewModel.HasError || (loginviewModel.IsResetPassword && loginModel.SuccessMessage == "ShibUser")) //ignore reset password for Illinois Users, handled inside
            {
                //reset password as needed
                if (loginviewModel.IsResetPassword)
                {

                    //ZnodeLogging.LogMessage(new Exception("Reset Illinois password requested"));
                    ResetIllinoisPassword(loginModel.Username);
                    //now login again to set session right
                    loginviewModel = _userAgent.Login(loginModel);

                }

                //Finish filling out user information if it is blank
                UserViewModel localUser = _userAgent.GetUserViewModelFromSession();
                //ZnodeLogging.LogMessage(string.Format("Checking name information to fill out " + localUser.LastName));
                if (string.IsNullOrEmpty(localUser.LastName)) _userAgent.UpdateProfile(GenerateUserProfileExt(localUser, regModel), true); //update name
                //ZnodeLogging.LogMessage(string.Format("LocalUser now has lastname {0}, Session has {1}", localUser.LastName, _userAgent.GetUserViewModelFromSession().LastName));
                //ZnodeLogging.LogMessage(string.Format("Checking address information to fill out (commented)"));
                try
                {
                    localUser = _userAgent.GetUserViewModelFromSession();
                    if (localUser.Addresses.Count == 0 && !string.IsNullOrEmpty(localUser.LastName))
                    {
                        _userAgent.CreateUpdateAddress(GenerateAddressExt(regModel));
                        //localUser.Addresses.Add(GenerateAddress());
                        //_userAgent.UpdateProfile(localUser, true); //_userAgent.CreateUpdateAddress(GenerateAddress());
                        //ZnodeLogging.LogMessage("Added a billing address");
                    }
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage(string.Format("Error working addresses: Source:{0} Message:{1} IEx:{2}", ex.Source, ex.Message, ex.InnerException.Message));
                }

                SetAuthCookie(loginModel.Username, _userAgent.GetUserViewModelFromSession().UserId.ToString()); //Was trying to get AccountID, which may not be

                //Remember me
                if (checked(loginModel.RememberMe) == true)
                {
                    SaveLoginRememberMeCookie(loginModel.Username);
                }

                var isMerged = _cartAgent.MergeCart();

                if (isMerged)
                {
                    //ZnodeLogging.LogMessage(new Exception("Cart Merged, redirecting to Index/Cart"));
                    return RedirectToAction("Index", "Cart");
                }

                if (string.IsNullOrEmpty(returnURL))
                {
                    //ZnodeLogging.LogMessage(new Exception("No ReturnUrl, redirecting to Dashboard"));
                    return RedirectToAction("Dashboard");
                }

                //ZnodeLogging.LogMessage("Redirecting from Login Page");
                _authenticationHelper.RedirectFromLoginPage(loginModel.Username, true);
                return new EmptyResult();
            }

            ZnodeLogging.LogMessage(new Exception("Problem: Not marked as a Shib user"));

            if (loginviewModel != null && loginviewModel.IsResetPassword)
            {
                if (!string.IsNullOrEmpty(loginviewModel.Username) && !string.IsNullOrEmpty(loginviewModel.PasswordResetToken))
                {
                    //ZnodeLogging.LogMessage(new Exception("Reset Pasword - valid username and password reset token"));
                    return View("Login", loginviewModel);
                }
                //ZnodeLogging.LogMessage(new Exception("Reset Pasword - invalid username or password reset token"));
                return View("ResetPassword", new ChangePasswordViewModel());
            }

            ZnodeLogging.LogMessage(new Exception(string.Format("Problem: loginviewmodel error: {0}, {1} or settings for shib not right: Reset Pass-{2}, PassQuest-{3} ", loginviewModel.HasError, loginviewModel.ErrorMessage, loginviewModel.IsResetPassword, loginModel.SuccessMessage)));
            return View("Login", loginviewModel);
        }

        [AllowAnonymous]
        [HttpPost]
        public override ActionResult Login(LoginViewModel model, string returnUrl, bool isSinglePageCheckout = false)
        {
            if (ModelState.IsValid)
            {
                //ZnodeLogging.LogMessage("Valid modelstate, calling useragent login");

                var loginviewModel = _userAgent.Login(model);

                //ZnodeLogging.LogMessage("UserAgent Login executed");

                if (!loginviewModel.HasError || (loginviewModel.IsResetPassword && model.SuccessMessage == "ShibUser")) //ignore reset password for Illinois Users, handled inside
                {
                    //reset password as needed
                    if (loginviewModel.IsResetPassword)
                    {

                        //ZnodeLogging.LogMessage(new Exception("Reset Illinois password requested"));
                        ResetIllinoisPassword(model.Username);
                        //now login again to set session right
                        loginviewModel = _userAgent.Login(model);

                    }

                    //Finish filling out user information if it is blank
                    UserViewModel localUser = _userAgent.GetUserViewModelFromSession();
                    //ZnodeLogging.LogMessage(string.Format("Checking name information to fill out"));
                    if (string.IsNullOrEmpty(localUser?.LastName)) _userAgent.UpdateProfile(GenerateUserProfile(localUser), true); //update name
                    //ZnodeLogging.LogMessage(string.Format("LocalUser now has lastname {0}, Session has {1}", localUser.LastName, _userAgent.GetUserViewModelFromSession().LastName));
                    //ZnodeLogging.LogMessage(string.Format("Checking address information to fill out (commented)"));
                    try
                    {
                        localUser = _userAgent.GetUserViewModelFromSession();
                        if (localUser.Addresses.Count == 0 && !string.IsNullOrEmpty(localUser.LastName))
                        {
                            _userAgent.CreateUpdateAddress(GenerateAddress());
                            //localUser.Addresses.Add(GenerateAddress());
                            //_userAgent.UpdateProfile(localUser, true); //_userAgent.CreateUpdateAddress(GenerateAddress());
                            //ZnodeLogging.LogMessage("Added a billing address");
                        }
                    }
                    catch (Exception ex)
                    {
                        ZnodeLogging.LogMessage(string.Format("Error working addresses: Source:{0} Message:{1} IEx:{2}", ex.Source, ex.Message, ex.InnerException.Message));
                    }

                    SetAuthCookie(model.Username, _userAgent.GetUserViewModelFromSession().UserId.ToString()); //Was trying to get AccountID, which may not be

                    //Remember me
                    if (checked(model.RememberMe) == true)
                    {
                        SaveLoginRememberMeCookie(model.Username);
                    }

                    var isMerged = _cartAgent.MergeCart();

                    if (isMerged)
                    {
                        //ZnodeLogging.LogMessage(new Exception("Cart Merged, redirecting to Index/Cart"));
                        return RedirectToAction("Index", "Cart");
                    }

                    if (string.IsNullOrEmpty(returnUrl))
                    {
                        //ZnodeLogging.LogMessage(new Exception("No ReturnUrl, redirecting to Dashboard"));
                        return RedirectToAction("Dashboard");
                    }

                    //ZnodeLogging.LogMessage("Redirecting from Login Page");
                    _authenticationHelper.RedirectFromLoginPage(model.Username, true);
                    return new EmptyResult();
                }

                //ZnodeLogging.LogMessage(new Exception("Problem: Not marked as a Shib user"));

                if (loginviewModel != null && loginviewModel.IsResetPassword)
                {
                    if (!string.IsNullOrEmpty(loginviewModel.Username) && !string.IsNullOrEmpty(loginviewModel.PasswordResetToken))
                    {
                        //ZnodeLogging.LogMessage(new Exception("Reset Pasword - valid username and password reset token"));
                        return View("Login", loginviewModel);
                    }
                    //ZnodeLogging.LogMessage(new Exception("Reset Pasword - invalid username or password reset token"));
                    return View("ResetPassword", new ChangePasswordViewModel());
                }

                ZnodeLogging.LogMessage(new Exception(string.Format("Problem: loginviewmodel error: {0}, {1} or settings for shib not right: Reset Pass-{2}, PassQuest-{3} ", loginviewModel.HasError, loginviewModel.ErrorMessage, loginviewModel.IsResetPassword, model.SuccessMessage)));
                return View("Login", loginviewModel);
            }

            ZnodeLogging.LogMessage(new Exception("Invalid Login Model State"));
            return View("Login", model);

        }

        public override ActionResult Logout()
        {
            _userAgent.Logout();

            //Go to defined route that hands off Shib Logout to the Shib SP
            //return RedirectToRoute("ShibLogout");
            return Redirect("~/Shibboleth.sso/Logout?https://shibboleth.illinois.edu/idp/profile/Logout");

        }

        [AllowAnonymous]
        [HttpGet]
        public override ActionResult Signup()
        {
            return View("Register", new CustomRegisterViewModel());
        }

        [AllowAnonymous]
        [HttpPost]
        public override ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            if (UpdateADPassword(model))
                return base.ChangePassword(model);
            else
                return View();
        }

        public override ActionResult ResetPassword(ChangePasswordViewModel model)
        {
            if (UpdateADPassword(model))
            {
                model.NewPassword = IllinoisHelper.PasswordMixup(IllinoisHelper.NormalizeEPPN(model.UserName));
                model.ReTypeNewPassword = model.NewPassword;
                return base.ResetPassword(model);
            }

            else
                return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public override ActionResult ForgotPassword(UserViewModel model)
        {

            return base.ForgotPassword(model);
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult SignupExt()
        {
            return View("Register", new CustomRegisterViewModel());
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult SignupExt(CustomRegisterViewModel model, string returnUrl, bool isSinglePageCheckout = false)
        {

            string errMessage = "Error Creating user: ";

            try
            {
                //CustomRegisterViewModel customModel = (CustomRegisterViewModel)model;
                //ZnodeLogging.LogMessage(string.Format("model contents: {0}", customModel.ToString()));
                if (CreateADuser(model))
                {
                    ZnodeLogging.LogMessage("Successful call to CreateADuser " + model.UofIUserName);

                    ////move calculated userid into main userid
                    //model.UserName = model.UofIUserName + "@illinois.edu";
                    ////change password to Znode internal password
                    //model.Password = IllinoisHelper.PasswordMixup(model.UserName);
                    //model.ReTypePassword = IllinoisHelper.PasswordMixup(model.UserName);


                    //RegisterViewModel returnmodel = _userAgent.SignUp(model);

                    //if (returnmodel.HasError)
                    //    errMessage += returnmodel.ErrorMessage;
                    //else                    
                    return LoginExt(returnUrl, model);
                }
                else errMessage += "User already exists, please enter a different email";
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(string.Format("Error: {0}", ex.Message));
                //NodeLogging.LogMessage(string.Format("Error: {0}{1}IE:{2}{1}{3}", ex.Message, Environment.NewLine, ex.InnerException.Message, ex.StackTrace));
            }

            model.HasError = true;
            model.ErrorMessage = errMessage;
            return View("Register", model);

        }

        //Get user's additional attribute details.
        [HttpGet]
        public virtual ActionResult GetAdditionalUserAttributes(int entityId, string entityType) =>
         ActionView("../User/GlobalAttributeEntity", _formBuilderAgent.GetEntityAttributeDetails(entityId, entityType));

        //Save user's additional attribute details based on user's entity id & entity type. 
        [HttpPost]
        public override ActionResult SaveEntityDetails([ModelBinder(typeof(ControlsModelBinder))] BindDataModel model)
        {
            string errorMessage = string.Empty;
            EntityAttributeViewModel entityAttributeViewModel = _formBuilderAgent.SaveEntityAttributeDetails(model, out errorMessage);
            SetNotificationMessage(!entityAttributeViewModel.HasError ? GetSuccessNotificationMessage(WebStore_Resources.UpdateMessage) : GetErrorNotificationMessage(errorMessage));
            return RedirectToAction<ShibUserController>(x => x.GetAdditionalUserAttributes(entityAttributeViewModel.EntityValueId, entityAttributeViewModel.EntityType));
        }

        private bool CreateADuser(CustomRegisterViewModel model)
        {
            //ZnodeLogging.LogMessage("Entering CreateADuser");
            ADFunctions ADWorker = new ADFunctions();

            if (!ADWorker.IsUserExisiting(model.UofIUserName, "illinois.edu"))
            {
                //ZnodeLogging.LogMessage("Existing user not found, creating");
                ADWorker.CreateUser(model.UofIUserName, model.FirstName, model.LastName, model.Password, model.Email);
                return true;
            }
            else
                return false;
        }

        private bool UpdateADPassword(ChangePasswordViewModel model)
        {
            string netID = IllinoisHelper.TrimNetid(model.UserName);
            ADFunctions ADWorker = new ADFunctions();

            if (ADWorker.IsUserExisiting(netID, "illinois.edu"))
            {
                ADWorker.UpdatePassword(netID, "illinois.edu", model.NewPassword);
                return true;
            }
            else return false;
        }

        private ActionResult ProcessLoginFor(string fullnetid, string returnUrl)
        {
            LoginViewModel shibModel = new LoginViewModel();
            shibModel.Username = IllinoisHelper.NormalizeEPPN(fullnetid);
            shibModel.Password = IllinoisHelper.PasswordMixup(shibModel.Username);
            //Mark as shib generated
            shibModel.SuccessMessage = "ShibUser";
            shibModel.CreatedBy = CREATED_BY_SHIBBOLETH;

            //ZnodeLogging.LogMessage("Shibmodel created, passing to Login routine");
            return Login(shibModel, returnUrl);
        }

        private void SetAuthCookie(string userName, string id)
        {
            //Set the Authentication Cookie.           
            _authenticationHelper.SetAuthCookie(userName, true);
        }

        private bool ResetIllinoisPassword(string userName)
        {
            string passwordMixup = IllinoisHelper.PasswordMixup(userName);
            ChangePasswordViewModel RIPmodel = new ChangePasswordViewModel();
            RIPmodel.UserName = userName;
            RIPmodel.OldPassword = passwordMixup;
            RIPmodel.NewPassword = passwordMixup + "1";
            RIPmodel.ReTypeNewPassword = RIPmodel.NewPassword;

            //ZnodeLogging.LogMessage(string.Format("ChangePasswordViewModel created for {0}, sending to userAgent call", userName));
            RIPmodel = _userAgent.ChangePassword(RIPmodel);

            if (!RIPmodel.HasError)
            {
                RIPmodel = new ChangePasswordViewModel();
                RIPmodel.UserName = userName;
                RIPmodel.OldPassword = passwordMixup + "1";
                RIPmodel.NewPassword = passwordMixup;
                RIPmodel.ReTypeNewPassword = passwordMixup;
                RIPmodel = _userAgent.ChangePassword(RIPmodel);
            }
            if (RIPmodel.HasError)
            {
                //log or display something
                ZnodeLogging.LogMessage(string.Format("Error changine password for {0}: {1}", userName, RIPmodel.ErrorMessage));
            }


            return !RIPmodel.HasError;

        }

        private AddressViewModel GenerateAddress()
        {
            AddressViewModel returnViewModel = new AddressViewModel();

            //ZnodeLogging.LogMessage("Generating Mailing Address");
            if (!string.IsNullOrEmpty(Request.Headers["postalAddress"]))
                returnViewModel.Address1 = Request.Headers["postalAddress"];
            else
                returnViewModel.Address1 = "WebStore";

            returnViewModel.DisplayName = "Auto";
            returnViewModel.UserId = _userAgent.GetUserViewModelFromSession().UserId;
            if (!string.IsNullOrEmpty(Request.Headers["givenName"]))
                returnViewModel.FirstName = Request.Headers["givenName"];
            else
                returnViewModel.FirstName = "WebStore";

            if (!string.IsNullOrEmpty(Request.Headers["sn"]))
                returnViewModel.LastName = Request.Headers["sn"];
            else
                returnViewModel.LastName = "User";

            returnViewModel.CityName = "Urbana";
            returnViewModel.StateName = "IL";
            returnViewModel.PostalCode = "61801";
            returnViewModel.CountryName = "US";
            if (!string.IsNullOrEmpty(Request.Headers["telephoneNumber"]))
                returnViewModel.PhoneNumber = Request.Headers["telephoneNumber"];
            else
                returnViewModel.PhoneNumber = "000-000-0000";

            returnViewModel.IsBilling = true;
            returnViewModel.IsShipping = true;
            returnViewModel.UseSameAsShippingAddress = true;
            returnViewModel.IsDefaultBilling = true;
            returnViewModel.IsDefaultShipping = true;
            returnViewModel.IsActive = true;


            //ZnodeLogging.LogMessage("Mailing Address Generated");

            return returnViewModel;
        }

        private UserViewModel GenerateUserProfile(UserViewModel localUser)
        {
            //ZnodeLogging.LogMessage("Entering GenerateUserProfile");
            UserViewModel returnViewModel = localUser;

            //ZnodeLogging.LogMessage("made it past assignment");

            if (!string.IsNullOrEmpty(Request.Headers["givenName"]))
                returnViewModel.FirstName = Request.Headers["givenName"];
            else
                returnViewModel.FirstName = "WebStore";

            if (!string.IsNullOrEmpty(Request.Headers["sn"]))
                returnViewModel.LastName = Request.Headers["sn"];
            else
                returnViewModel.LastName = "User";

            if (!string.IsNullOrEmpty(Request.Headers["affiliation"]))
                returnViewModel.ExternalId = Request.Headers["affiliation"];
            else
                returnViewModel.ExternalId = "Unknown";

            if (!string.IsNullOrEmpty(Request.Headers["telephoneNumber"]))
                returnViewModel.PhoneNumber = Request.Headers["telephoneNumber"];


            //ZnodeLogging.LogMessage("User Profile filled out");

            return returnViewModel;
        }

        private AddressViewModel GenerateAddressExt(CustomRegisterViewModel regModel)
        {
            AddressViewModel returnViewModel = new AddressViewModel();

            //ZnodeLogging.LogMessage("Generating Mailing Address");

            returnViewModel.Address1 = regModel.Address1;
            returnViewModel.Address2 = regModel.Address2;

            returnViewModel.DisplayName = "Registered";
            returnViewModel.UserId = _userAgent.GetUserViewModelFromSession().UserId;

            returnViewModel.FirstName = regModel.FirstName;

            returnViewModel.LastName = regModel.LastName;

            returnViewModel.CityName = regModel.CityName;
            returnViewModel.StateName = regModel.StateName;
            returnViewModel.PostalCode = regModel.PostalCode;
            returnViewModel.CountryName = "US";

            returnViewModel.PhoneNumber = "000-000-0000";

            returnViewModel.IsBilling = true;
            returnViewModel.IsShipping = true;
            returnViewModel.UseSameAsShippingAddress = true;
            returnViewModel.IsDefaultBilling = true;
            returnViewModel.IsDefaultShipping = true;
            returnViewModel.IsActive = true;


            //ZnodeLogging.LogMessage("Mailing Address Generated");

            return returnViewModel;
        }

        private UserViewModel GenerateUserProfileExt(UserViewModel localUser, CustomRegisterViewModel regModel)
        {
            //ZnodeLogging.LogMessage("Entering GenerateUserProfileExt");
            UserViewModel returnViewModel = localUser;


            returnViewModel.FirstName = regModel.FirstName;

            returnViewModel.LastName = regModel.LastName;

            returnViewModel.ExternalId = "External";


            //ZnodeLogging.LogMessage("User Profile filled out");

            return returnViewModel;
        }

        private void LogShibInfo()
        {
            string infoString = string.Empty;

            if (!string.IsNullOrEmpty(Request.Headers["postalAddress"]))
                infoString += string.Format("postalAddress: {0}{1}",Request.Headers["postalAddress"], Environment.NewLine);

            if (!string.IsNullOrEmpty(Request.Headers["givenName"]))
                infoString += string.Format("givenName: {0}{1}", Request.Headers["givenName"], Environment.NewLine);

            if (!string.IsNullOrEmpty(Request.Headers["sn"]))
                infoString += string.Format("sn: {0}{1}", Request.Headers["sn"], Environment.NewLine);

            if (!string.IsNullOrEmpty(Request.Headers["unscoped-affiliation"]))
                infoString += string.Format("eduPersonAffiliation(unscoped-affiliation): {0}{1}", Request.Headers["unscoped-affiliation"], Environment.NewLine);

            if (!string.IsNullOrEmpty(Request.Headers["iTrustAffiliation"]))
                infoString += string.Format("iTrustAffiliation: {0}{1}", Request.Headers["iTrustAffiliation"], Environment.NewLine);

            if(!string.IsNullOrEmpty(Request.Headers["isMemberOf"]))
                infoString += string.Format("isMemberOf: {0}{1}", Request.Headers["isMemberOf"], Environment.NewLine);

            if (!string.IsNullOrEmpty(Request.Headers["primary-affiliation"]))
                infoString += string.Format("eduPersonPrimaryAffiliation(primary-affiliation): {0}{1}", Request.Headers["primary-affiliation"], Environment.NewLine);

            if (!string.IsNullOrEmpty(Request.Headers["affiliation"]))
                infoString += string.Format("eduPersonScopedAffiliation(affiliation): {0}{1}", Request.Headers["affiliation"], Environment.NewLine);

            if (!string.IsNullOrEmpty(Request.Headers["telephoneNumber"]))
                infoString += string.Format("telephoneNumber: {0}{1}", Request.Headers["telephoneNumber"], Environment.NewLine);

            if (!string.IsNullOrEmpty(Request.Headers["organizationName"]))
                infoString += string.Format("organizationName: {0}{1}", Request.Headers["organizationName"], Environment.NewLine);

            if (!string.IsNullOrEmpty(Request.Headers["mail"]))
                infoString += string.Format("mail: {0}{1}", Request.Headers["mail"], Environment.NewLine);

            ZnodeLogging.LogMessage(string.Format("Shib variables found: {0}", infoString));

        }
    }
}