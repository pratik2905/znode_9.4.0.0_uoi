﻿using System;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Maps;
using Znode.Engine.WebStore.ViewModels;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.WebStore.Maps
{
    public class IllinoisUserViewModelMap : UserViewModelMap
    {
        public new static UserModel ToUserModel(UserViewModel model)
        {
            return new UserModel()
            {
                UserId = model.UserId,
                Email = string.IsNullOrEmpty(model.Email) ? model.UserName : model.Email,
                User = new LoginUserModel()
                {
                    Username = model.UserName,
                    Email = string.IsNullOrEmpty(model.Email) ? model.UserName : model.Email,
                    UserId = model.AspNetUserId,
                },
                BaseUrl = model.BaseUrl,
                PortalId = PortalAgent.CurrentPortal.PortalId,
                LocaleId = PortalAgent.LocaleId
            };
        }

        public new static UserModel ToSignUpModel(RegisterViewModel model)
        {
            var signUpModel = new UserModel()
            {
                User = new LoginUserModel()
                {
                    Username = model.UserName,
                    Password = model.Password,
                    Email = string.IsNullOrEmpty(model.Email) ? model.UserName : model.Email,
                },
                EmailOptIn = model.EmailOptIn,
                IsWebStoreUser = model.IsWebStoreUser,
                PortalId = PortalAgent.CurrentPortal.PortalId,
                LocaleId = PortalAgent.LocaleId,
                Email = model.Email,
                ExternalId = model.ExternalId
            };

            return signUpModel;
        }
    }
}
