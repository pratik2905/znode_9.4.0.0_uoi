﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.WebStore.ViewModels;

namespace Znode.WebStore.Custom.Agents
{
    interface IIllinoisUserAgent
    {
        GlobalAttributeEntityDetailsViewModel GetEntityAttributeDetails(int entityId, string entityType);
    }
}
