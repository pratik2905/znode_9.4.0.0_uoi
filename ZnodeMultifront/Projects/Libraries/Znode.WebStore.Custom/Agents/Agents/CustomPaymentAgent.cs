﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.WebStore.Agents
{
    public class CustomPaymentAgent : PaymentAgent
    {
        private IPaymentClient _paymentClient;

        public CustomPaymentAgent(IPaymentClient paymentClient, IOrderClient orderClient) : base(paymentClient, orderClient)
        {
            _paymentClient = GetClient<IPaymentClient>(paymentClient);
        }

        public override PaymentSettingViewModel GetPaymentSetting(int paymentSettingId, int portalId = 0)
        {
            PaymentSettingViewModel returnModel = new PaymentSettingViewModel();
            try
            {
                returnModel = base.GetPaymentSetting(paymentSettingId);
                //Get missing information from the payment application payment setting record
               
                PaymentSettingModel paymentSettingViewModel = _paymentClient.GetPaymentSettingByPaymentCode(returnModel?.PaymentCode);
                returnModel.Title = paymentSettingViewModel.Vendor;
                returnModel.ViewParamField = paymentSettingViewModel.TestMode.ToString();
            }
            catch (Exception ex)
            {
                returnModel.TransactionKey = string.Format("Error: {0}", ex.Message);
            }


            return returnModel;
        }


        //public override PaymentSettingViewModel GetPaymentSetting(int paymentSettingId)
        //{
        //    PaymentSettingViewModel paymentSettingViewModel = null;
        //    try
        //    {

        //        PaymentSettingModel paymentSettingModel = _paymentClient.GetPaymentSetting(paymentSettingId, false, new ExpandCollection { ZnodePaymentSettingEnum.ZnodePaymentType.ToString() });

        //        if (paymentSettingModel?.PaymentApplicationSettingId > 0)
        //        {

        //            paymentSettingViewModel = _paymentClient.GetPaymentSetting(paymentSettingModel.PaymentApplicationSettingId, true)?.ToViewModel<PaymentSettingViewModel>();
        //           //paymentSettingViewModel = base.GetPaymentSetting(paymentSettingModel.PaymentApplicationSettingId, true, null);

        //            if (HelperUtility.IsNotNull(paymentSettingViewModel))
        //            {
        //                //paymentSettingViewModel.PaymentApplicationSettingId = paymentSettingViewModel.PaymentSettingId;
        //                //paymentSettingViewModel.PaymentSettingId = paymentSettingId;
        //                paymentSettingViewModel.Title = paymentSettingModel.Vendor;
        //                paymentSettingViewModel.ViewParamField = paymentSettingModel.TestMode.ToString();

        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        paymentSettingViewModel.TransactionKey = string.Format("Error: {0}", ex.Message);
        //    }

        //    return paymentSettingViewModel;

        //}




        //var _foo = new Znode.Engine.Api.Client.PaymentClient();
        //_foo.GetPaymentSettingCredentials
    }
}
