﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;

namespace Znode.Engine.WebStore.Agents
{
    class IllinoisProductAgent : ProductAgent
    {
        #region Public Constructor
        public IllinoisProductAgent(ICustomerReviewClient reviewClient, IPublishProductClient productClient, IWebStoreProductClient webstoreProductClient,
            ISearchClient searchClient, IHighlightClient highlightClient, IPublishCategoryClient publishCategoryClient)
            : base(reviewClient, productClient, webstoreProductClient, searchClient, highlightClient, publishCategoryClient)
        {
            //no custom creator code
            
        }
        #endregion

        #region Overrides

       
        public override void CheckInventory(ProductViewModel viewModel, decimal? quantity)
        {
            string OutOfStockOption = !string.IsNullOrEmpty(viewModel.Attributes?.CodeFromSelectValue("OutOfStockOptions")) ? viewModel.Attributes?.CodeFromSelectValue("OutOfStockOptions") : string.Empty;
            bool AllowBackOrder = false;
            bool TrackInventory = true;
            decimal selectedQuantity = quantity.GetValueOrDefault();   // quantity field is parameter to the overridden method

            const string DontTrackInventory = "DontTrackInventory";
            const string DisablePurchasing = "DisablePurchasing";
            const string AllowBackOrdering = "AllowBackOrdering";

            string sku = string.IsNullOrEmpty(viewModel.ConfigurableProductSKU) ? viewModel.SKU : viewModel.ConfigurableProductSKU;

            decimal cartQuantity = GetOrderedItemQuantity(sku);
            decimal combinedQuantity = selectedQuantity + cartQuantity;


            AllowBackOrder = (OutOfStockOption == AllowBackOrdering);
            TrackInventory = (OutOfStockOption != DontTrackInventory);
            

            //ZNodeLogging.LogMessage(string.Format("Cart: {0}, combined: {1}, existing: {2} OutOfStockOption: {3}, ShowAddtoCart: {4}", cartQuantity, combinedQuantity, viewModel.Quantity, OutOfStockOption, viewModel.ShowAddToCart));

            if (viewModel.Quantity < combinedQuantity && !AllowBackOrder && TrackInventory)
            {
                //ZNodeLogging.LogMessage(string.Format("Has product OOS message: {0}", !string.IsNullOrEmpty(viewModel.Attributes?.Value("OUTOFSTOCK"))));
                viewModel.InventoryMessage = !string.IsNullOrEmpty(viewModel.Attributes?.Value("OUTOFSTOCK")) ? viewModel.Attributes?.Value("OUTOFSTOCK") : (!string.IsNullOrEmpty(viewModel.OutOfStockMessage) ? viewModel.OutOfStockMessage : WebStore_Resources.TextOutofStock);     //OUTOFSTOCK is the custom text attribute 
                viewModel.ShowAddToCart = false;
                return;
            }
            else
            {
                viewModel.InventoryMessage = !string.IsNullOrEmpty(viewModel.InStockMessage) ? viewModel.InStockMessage : WebStore_Resources.TextInstock;
                viewModel.ShowAddToCart = true;
            }
        }
        #endregion
    }
}
