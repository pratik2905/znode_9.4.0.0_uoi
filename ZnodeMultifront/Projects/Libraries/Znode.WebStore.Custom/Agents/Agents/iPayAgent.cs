﻿using Znode.Api.Client.Custom.Clients;
using Znode.Engine.Api.Model.Custom;
using Znode.Engine.WebStore;

namespace Znode.WebStore.Custom.Agents.Agents
{
    class iPayAgent : BaseAgent
    {
        ICustomiPayClient _iPayClient;
        public iPayAgent()
        {
            _iPayClient = new CustomiPayClient();
        }

        public iPayRecordModel CreateiPayRecord(iPayRecordModel model)
        {
            return _iPayClient.CreateIllinoisiPay(model);
        }

        public iPayRecordModel GetiPayRecordByReference(string ReferenceId)
        {
            return _iPayClient.GetIllinoisiPay(ReferenceId);
        }

        public iPayRecordModel GetiPayRecordByToken(string Token)
        {
            return _iPayClient.GetIllinoisiPayByToken(Token);
        }

        public iPayRecordModel UpdateiPayRecord(iPayRecordModel model)
        {
            return _iPayClient.UpdateIllinoisiPay(model);
        }
    }
}
