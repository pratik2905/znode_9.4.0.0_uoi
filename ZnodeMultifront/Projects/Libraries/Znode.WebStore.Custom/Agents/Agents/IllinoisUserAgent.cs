﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.WebStore.Maps;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;


namespace Znode.Engine.WebStore.Agents
{
    class IllinoisUserAgent : UserAgent
    {
        #region private variables
        private IUserClient _userClient;
        private  IOrderClient _orderClient;
        private  IShippingClient _shippingClient;
        #endregion

        #region Public Contructor
        public IllinoisUserAgent(ICountryClient countryClient, IWebStoreUserClient webStoreAccountClient, IWishListClient wishListClient, IUserClient userClient,
            IPublishProductClient productClient, ICustomerReviewClient customerReviewClient, IOrderClient orderClient, IGiftCardClient giftCardClient,
            IAccountClient accountClient, IAccountQuoteClient accountQuoteClient, IOrderStateClient orderStateClient, IPortalCountryClient portalCountryClient,
            IShippingClient shippingClient, IPaymentClient paymentClient, ICustomerClient customerClient, IStateClient stateClient, IPortalProfileClient portalProfileClient) :
            base( countryClient,  webStoreAccountClient,  wishListClient,  userClient,  productClient,  customerReviewClient,  orderClient,  giftCardClient,  accountClient,  accountQuoteClient,  orderStateClient,  portalCountryClient,  shippingClient,  paymentClient,  customerClient,  stateClient,  portalProfileClient)
        {
            _userClient = userClient;
            _orderClient = GetClient<IOrderClient>(orderClient);
            _shippingClient = GetClient<IShippingClient>(shippingClient);
        }


        //ICountryClient countryClient, IWebStoreUserClient webStoreAccountClient, IWishListClient wishListClient, IUserClient userClient, IPublishProductClient productClient, ICustomerReviewClient customerReviewClient, IOrderClient orderClient, IGiftCardClient giftCardClient, IAccountClient accountClient, IAccountQuoteClient accountQuoteClient, IOrderStateClient orderStateClient, IPortalCountryClient portalCountryClient, IShippingClient shippingClient, IPaymentClient paymentClient, ICustomerClient customerClient, IStateClient stateClient, IPortalProfileClient portalProfileClient
        #endregion

        #region Overrides
        public override LoginViewModel Login(LoginViewModel model)
        {
            UserModel apimodel = new UserModel();
            apimodel = model.ToModel<UserModel>();
            apimodel.Email = string.IsNullOrEmpty(model.EditParamField) ? model.Username : model.EditParamField;
            apimodel.PortalId = PortalAgent.CurrentPortal?.PortalId;
            UserModel returnModel = _userClient.Login(apimodel, new Api.Client.Expands.ExpandCollection());
            //return Maps.UserViewModelMap.ToLoginViewModel(returnModel);
            return base.Login(model);
        }

        public override UserViewModel ForgotPassword(UserViewModel model)
        {
            //Fill out the email part of the model
            //UserModel userModel = _userClient.GetAccountByUser(model.UserName);
            //if (HelperUtility.IsNotNull(userModel))
            //    model.Email = userModel.Email;

            if (HelperUtility.IsNotNull(model))
            {
                try
                {
                    //Get the User Details based on username.
                    UserModel userModel = _userClient.GetAccountByUser(model.UserName);
                    if (HelperUtility.IsNotNull(userModel))
                    {
                        if (!model.HasError)
                        {
                            //Check if the Username Validates or not.
                            if (string.Equals(model.UserName, userModel.UserName, StringComparison.InvariantCultureIgnoreCase))
                            {
                                //Set the Current Domain Url.
                                model.BaseUrl = GetDomainUrl();

                                //Set the email properly
                                model.Email = userModel.Email;

                                //Reset the Password for the user, to send the Reset Email Password Link.
                                model = ResetPassword(model);
                                return model;
                            }
                            else
                                return SetErrorProperties(model, WebStore_Resources.ValidEmailAddress);
                        }
                    }
                    else
                        return SetErrorProperties(model, WebStore_Resources.InvalidAccountInformation);
                }
                catch (ZnodeException ex)
                {
                    ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                    return SetErrorProperties(model, WebStore_Resources.InvalidAccountInformation);
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                    return SetErrorProperties(model, WebStore_Resources.InvalidAccountInformation);
                }
            }

            return model;
        }

        
        //Get order details to generate the order reciept.
        public override OrdersViewModel GetOrderDetails(int orderId, int portalId = 0)
        {
            UserViewModel userViewModel = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);

            if (HelperUtility.IsNotNull(userViewModel) && orderId > 0)
            {
                OrderModel orderModel = portalId > 0 ? GetOrderBasedOnPortalId(orderId, portalId) : _orderClient.GetOrderById(orderId, SetExpandsForReceipt());

                if (userViewModel.UserId == orderModel.UserId || ((orderModel.IsQuoteOrder || userViewModel?.AccountId.GetValueOrDefault() > 0) && (string.Equals(userViewModel.RoleName, ZnodeRoleEnum.Administrator.ToString(), StringComparison.CurrentCultureIgnoreCase) || string.Equals(userViewModel.RoleName, ZnodeRoleEnum.Manager.ToString(), StringComparison.CurrentCultureIgnoreCase))))
                {
                    List<OrderLineItemModel> orderLineItemListModel = new List<OrderLineItemModel>();

                    //Create new order line item model.
                    CreateSingleOrderLineItem(orderModel, orderLineItemListModel);

                    orderModel.OrderLineItems = orderLineItemListModel;
                    orderModel.TrackingNumber = SetTrackingUrl(orderModel.TrackingNumber, orderModel.ShippingId);
                    if (orderModel?.OrderLineItems?.Count > 0)
                    {
                        OrdersViewModel orderDetails = orderModel?.ToViewModel<OrdersViewModel>();
                        orderDetails.CurrencyCode = PortalAgent.CurrentPortal?.CurrencyCode;
                        orderDetails.CouponCode = orderDetails.CouponCode?.Replace("<br/>", ", ");

                        orderDetails?.OrderLineItems.ForEach(u =>
                        {
                            u.UOM = orderModel?.ShoppingCartModel?.ShoppingCartItems?.FirstOrDefault(y => y.SKU == u.Sku)?.UOM;
                            u.Custom1 = orderModel?.OrderLineItems.FirstOrDefault(x => x.Sku == u.Sku).Attributes.FirstOrDefault(x => x.AttributeCode == "ReceiptCustomText")?.AttributeValue;
                        });

                        orderDetails?.OrderLineItems?.ForEach(x => x.TrackingNumber = SetTrackingUrl(x.TrackingNumber, orderModel.ShippingId));
                        return orderDetails;
                    }
                }
                return null;
            }
            return new OrdersViewModel();
        }

        //Method gets the Domain Base Url.
        private string GetDomainUrl()
            => (!string.IsNullOrEmpty(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)))
            ? HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) : string.Empty;


        //Reset the Password for the user.
        private UserViewModel ResetPassword(UserViewModel model)
        {
            try
            {
                _userClient.ForgotPassword(IllinoisUserViewModelMap.ToUserModel(model));
                model.SuccessMessage = WebStore_Resources.SuccessResetPassword;
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                SetErrorProperties(model, ex.ErrorMessage);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
            }
            return model;
        }

        //Update the Model with error properties.Return the updated model in UserViewModelFormat.
        private UserViewModel SetErrorProperties(UserViewModel model, string errorMessage)
        {
            model.HasError = true;
            model.ErrorMessage = errorMessage;
            return model;
        }

        //Get order details based on portalId.
        private OrderModel GetOrderBasedOnPortalId(int orderId, int portalId)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(ZnodePortalEnum.PortalId.ToString(), FilterOperators.Equals, portalId.ToString()));
            return _orderClient.GetOrderById(orderId, SetExpandsForReceipt(), filters);
        }

        //Set Expands For Order Receipt.
        private static ExpandCollection SetExpandsForReceipt()
        {
            ExpandCollection expands = new ExpandCollection();
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderLineItems.ToString());
            expands.Add(ExpandKeys.Store);
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodePaymentType.ToString());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodePaymentSetting.ToString());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderState.ToString());
            expands.Add(ExpandKeys.ZnodeShipping);
            expands.Add(ExpandKeys.IsFromOrderReceipt);
            expands.Add(ExpandKeys.PortalTrackingPixel);
            expands.Add(ExpandKeys.IsWebStoreOrderReciept);
            return expands;
        }

        //Set tracking url.
        private string SetTrackingUrl(string trackingNo, int shippingId)
        {
            string trackingUrl = GetTrackingUrlByShippingId(shippingId);
            return string.IsNullOrEmpty(trackingUrl) ? trackingNo : "<a target=_blank href=" + GetTrackingUrlByShippingId(shippingId) + trackingNo + ">" + trackingNo + "</a>";
        }

        //Get TrackingUrl by ShippingId.
        private string GetTrackingUrlByShippingId(int shippingId)
            => _shippingClient.GetShipping(shippingId)?.TrackingUrl;
        #endregion
    }
}
