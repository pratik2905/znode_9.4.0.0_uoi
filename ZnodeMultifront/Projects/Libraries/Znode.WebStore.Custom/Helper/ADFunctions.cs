﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices.AccountManagement;

namespace Znode.Engine.WebStore
{
    class ADFunctions
    {

        #region Variables
        //UIUC
        private string UOFILdapUser;
        private string UOFILdapPassword;
        private string UOFIRootOU;
        private string UOFIGroupOU;
        private string UOFIDomain;
        //UIC
        private string UICLdapUser;
        private string UICLdapPassword;
        private string UICRootOU;
        private string UICDomain;
        private string UICGroupOU;
        //UIS
        private string UISLdapUser;
        private string UISLdapPassword;
        private string UISRootOU;
        private string UISDomain;
        private string UISGroupOU;


        #endregion

        public ADFunctions()
        {
            UOFILdapUser = Crypto.DecryptStringAES(ConfigurationManager.AppSettings["UofILDAPUser"], ConfigurationManager.AppSettings["addsvalue"]);
            UOFILdapPassword = Crypto.DecryptStringAES(ConfigurationManager.AppSettings["UofILDAPPassword"], ConfigurationManager.AppSettings["addsvalue"]);
            UOFIDomain = ConfigurationManager.AppSettings["UofIDomain"];
            UOFIGroupOU = ConfigurationManager.AppSettings["UofIGroupOU"];
            UOFIRootOU = ConfigurationManager.AppSettings["UofIRootOU"];

            UICLdapUser = Crypto.DecryptStringAES(ConfigurationManager.AppSettings["UICLDAPUser"], ConfigurationManager.AppSettings["addsvalue"]);
            UICLdapPassword = Crypto.DecryptStringAES(ConfigurationManager.AppSettings["UICLDAPPassword"], ConfigurationManager.AppSettings["addsvalue"]);
            UICDomain = ConfigurationManager.AppSettings["UICDomain"];
            UICGroupOU = ConfigurationManager.AppSettings["UICGroupOU"];
            UICRootOU = ConfigurationManager.AppSettings["UICRootOU"];

            UISLdapUser = Crypto.DecryptStringAES(ConfigurationManager.AppSettings["UISLDAPUser"], ConfigurationManager.AppSettings["addsvalue"]);
            UISLdapPassword = Crypto.DecryptStringAES(ConfigurationManager.AppSettings["UISLDAPPassword"], ConfigurationManager.AppSettings["addsvalue"]);
            UISDomain = ConfigurationManager.AppSettings["UISDomain"];
            UISGroupOU = ConfigurationManager.AppSettings["UISGroupOU"];
            UISRootOU = ConfigurationManager.AppSettings["UISRootOU"];
        }

        #region Validate Methods


        public bool IsUserExisiting(string sUserName, string netidDomain)
        {
            if (GetUser(sUserName, netidDomain) == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        #endregion

        #region Search Methods

        /// <summary>
        /// Gets a certain user on Active Directory
        /// </summary>
        /// <param name="sUserName">The username to get</param>
        /// <returns>Returns the UserPrincipal Object</returns>
        public UserPrincipal GetUser(string sUserName, string netidDomain)
        {
            PrincipalContext oPrincipalContext = GetPrincipalContext(netidDomain);

            UserPrincipal oUserPrincipal = UserPrincipal.FindByIdentity(oPrincipalContext, IdentityType.SamAccountName, sUserName);
            return oUserPrincipal;
        }

        /// <summary>
        /// Gets a certain group on Active Directory
        /// </summary>
        /// <param name="sGroupName">The group to get</param>
        /// <returns>Returns the GroupPrincipal Object</returns>
        public GroupPrincipal GetGroup(string sGroupName, string campus)
        {
            PrincipalContext oPrincipalContext = GetPrincipalContextForGroups(campus);

            GroupPrincipal oGroupPrincipal = GroupPrincipal.FindByIdentity(oPrincipalContext, sGroupName);
            return oGroupPrincipal;
        }

        #endregion

        #region Group Methods

        public bool SynchronizeUsersInGroup(List<string> users, string netidDomain, string GroupName)
        {
            try
            {
                UserPrincipal oUserPrincipal;
                GroupPrincipal oGroupPrincipal = GetGroup(GroupName, netidDomain);
                List<Principal> oRemoved = new List<Principal>();
                int counter = 0;

                if (oGroupPrincipal != null)
                {
                    foreach (var groupmember in oGroupPrincipal.Members)
                    {
                        if (groupmember is UserPrincipal)
                        {
                            if (!users.Contains(groupmember.Name.ToLower()))
                            {
                                oRemoved.Add(groupmember);
                                //oGroupPrincipal.Members.Remove(groupmember);
                                counter++;
                            }
                            else
                            {
                                //Already a group member, don't try to add again
                                users.Remove(groupmember.Name.ToLower());
                            }
                        }
                    }

                    //save the changes to the group, if any were made.
                    if (counter > 0)
                    {
                        foreach (var member in oRemoved)
                        { oGroupPrincipal.Members.Remove(member); }
                        oGroupPrincipal.Save();
                    }

                    Console.WriteLine(string.Format("{0:H:mm:ss} {1} users removed from group, {2} users remain to be added", DateTime.Now, counter, users.Count));
                    counter = 0;

                    foreach (var netid in users)
                    {
                        oUserPrincipal = GetUser(netid, netidDomain);

                        if (oUserPrincipal != null)
                        {
                            //Not checking for current inclusion in group, that has been validated above that all remaining in list don't exist in group
                            oGroupPrincipal.Members.Add(oUserPrincipal);
                            counter++;
                            if (counter % 500 == 0)
                            {
                                oGroupPrincipal.Save();
                                Console.WriteLine(string.Format("{0:H:mm:ss} {1} users added", DateTime.Now, counter));
                            }

                        }
                        else Console.WriteLine(string.Format("{0:H:mm:ss} User {1} not found in AD", DateTime.Now, netid));
                    }

                    if (counter > 0)
                    {
                        oGroupPrincipal.Save();
                        Console.WriteLine(string.Format("{0:H:mm:ss} Complete: {1} users added", DateTime.Now, counter));
                    }
                }

                return true;
            }
            catch
            {
                Console.WriteLine("Problem in Synchronizing AD group");
                return false;
            }

        }

        /// <summary>
        /// Adds the user for a given group
        /// </summary>
        /// <param name="sUserName">The user you want to add to a group</param>
        /// <param name="sGroupName">The group you want the user to be added in</param>
        /// <returns>Returns true if successful</returns>
        public bool AddUserToGroup(string sUserName, string netidDomain, string sGroupName)
        {
            try
            {
                UserPrincipal oUserPrincipal = GetUser(sUserName, netidDomain);
                GroupPrincipal oGroupPrincipal = GetGroup(sGroupName, netidDomain);
                if (oUserPrincipal != null && oGroupPrincipal != null)
                {
                    if (!IsUserGroupMember(sUserName, netidDomain, sGroupName))
                    {
                        oGroupPrincipal.Members.Add(oUserPrincipal);
                        oGroupPrincipal.Save();
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }


        public bool RemoveAllUsersFromGroup(string sGroupName, string campus)
        {
            try
            {
                GroupPrincipal oGroupPrincipal = GetGroup(sGroupName, campus);
                if (oGroupPrincipal != null)
                {

                    oGroupPrincipal.Members.Clear();
                    oGroupPrincipal.Save();

                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Checks if user is a member of a given group
        /// </summary>
        /// <param name="sUserName">The user you want to validate</param>
        /// <param name="sGroupName">The group you want to check the membership of the user</param>
        /// <returns>Returns true if user is a group member</returns>
        public bool IsUserGroupMember(string sUserName, string netidDomain, string sGroupName)
        {
            UserPrincipal oUserPrincipal = GetUser(sUserName, netidDomain);
            GroupPrincipal oGroupPrincipal = GetGroup(sGroupName, netidDomain);

            if (oUserPrincipal != null && oGroupPrincipal != null)
            {
                return oGroupPrincipal.Members.Contains(oUserPrincipal);
            }
            else
            {
                return false;
            }
        }


        #endregion

        #region Creation Methods
        public void CreateUser(string username, string givenName, string sn, string password, string email)
        {
            using (var pc = GetPrincipalContext("illinois.edu"))
            {
                using (var up = new UserPrincipal(pc))
                {
                    up.SamAccountName = username;
                    up.EmailAddress = email;
                    up.SetPassword(password);
                    up.GivenName = givenName;
                    up.Surname = sn;
                    up.Enabled = true;
                    up.Description = "Automatically created by WebStore";
                    //maybe a requirement below?
                    up.AccountExpirationDate = DateTime.Now.AddYears(1);
                    up.Save();
                }
            }
        }
        #endregion

        #region Update Methods
        public void UpdatePassword(string username, string netidDomain, string password)
        {
            UserPrincipal up = GetUser(username, netidDomain);
            up.SetPassword(password);
            up.Save();
        }
        #endregion
        #region Helper Methods

        public PrincipalContext GetPrincipalContext(string campus)
        {
            string ContextDomain = UOFIDomain;
            string ContextOU = UOFIRootOU;
            string ContextUser = UOFILdapUser;
            string ContextPassword = UOFILdapPassword;
            PrincipalContext oPrincipalContext = new PrincipalContext(ContextType.Domain);

            //figure out parameters
            switch (campus)
            {
                case "illinois.edu":
                    ContextDomain = UOFIDomain;
                    ContextOU = UOFIRootOU;
                    ContextUser = UOFILdapUser;
                    ContextPassword = UOFILdapPassword;
                    break;
                case "uis.edu":
                    ContextDomain = UISDomain;
                    ContextOU = UISRootOU;
                    ContextUser = UISLdapUser;
                    ContextPassword = UISLdapPassword;
                    break;
                case "uic.edu":
                    ContextDomain = UICDomain;
                    ContextOU = UICRootOU;
                    ContextUser = UICLdapUser;
                    ContextPassword = UICLdapPassword;
                    break;
                default:
                    break;
            }

            //make the call
            switch (campus)
            {
                case "illinois.edu":
                case "uis.edu":
                case "uic.edu":
                    oPrincipalContext = new PrincipalContext(ContextType.Domain, ContextDomain, ContextOU, ContextOptions.Negotiate, ContextUser, ContextPassword);
                    break;
                //case "uic.edu":
                //    oPrincipalContext = new PrincipalContext(ContextType.Domain, ContextDomain, ContextOU);
                //    break;
                default:
                    break;
            }

            return oPrincipalContext;
        }

        /// <summary>
        /// Gets the base principal context
        /// </summary>
        /// <returns>Retruns the PrincipalContext object</returns>
        public PrincipalContext GetPrincipalContextForGroups(string campus)
        {
            string ContextDomain = UOFIDomain;
            string ContextOU = UOFIRootOU;
            string ContextUser = UOFILdapUser;
            string ContextPassword = UOFILdapPassword;
            PrincipalContext oPrincipalContext = new PrincipalContext(ContextType.Domain);

            //figure out parameters
            switch (campus)
            {
                case "illinois.edu":
                    ContextDomain = UOFIDomain;
                    ContextOU = UOFIGroupOU;
                    ContextUser = UOFILdapUser;
                    ContextPassword = UOFILdapPassword;
                    break;
                case "uis.edu":
                    ContextDomain = UISDomain;
                    ContextOU = UISGroupOU;
                    ContextUser = UISLdapUser;
                    ContextPassword = UISLdapPassword;
                    break;
                case "uic.edu":
                    ContextDomain = UICDomain;
                    ContextOU = UICGroupOU;
                    ContextUser = UICLdapUser;
                    ContextPassword = UICLdapPassword;
                    break;
                default:
                    break;
            }

            //make the call
            switch (campus)
            {
                case "illinois.edu":
                case "uis.edu":
                case "uic.edu":
                    oPrincipalContext = new PrincipalContext(ContextType.Domain, ContextDomain, ContextOU, ContextOptions.Negotiate, ContextUser, ContextPassword);
                    break;
                //case "uic.edu":
                //    oPrincipalContext = new PrincipalContext(ContextType.Domain, ContextDomain, ContextOU);
                //    break;
                default:
                    break;
            }

            return oPrincipalContext;
        }



        #endregion

    }
}