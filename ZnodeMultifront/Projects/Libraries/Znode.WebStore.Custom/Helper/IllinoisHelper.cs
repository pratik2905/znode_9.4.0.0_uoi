﻿using System;
using System.Security.Cryptography;

namespace Znode.Engine.WebStore
{
    public static class IllinoisHelper
    {
        /// <summary>
        /// Does the login end with an Illinois related domain
        /// </summary>
        /// <param name="login">Login with or without domain postfixed</param>
        /// <returns></returns>
        public static bool IsAnIllinoisLogin(string login)
        {
            string x = login.Trim().ToLower();
            return x.EndsWith("@illinois.edu") || x.EndsWith("@uiuc.edu") || x.EndsWith("@uic.edu") ||
                   x.EndsWith("@uis.edu");
        }

        /// <summary>
        /// Account for uiuc.edu passed in, translate to illinois.edu
        /// </summary>
        /// <param name="login"></param>
        /// <returns>"fixed" uiuc netid</returns>
        public static string NormalizeEPPN(string login)
        {
            login = login.ToLower();
            // translate uiuc.edu to illinois.edu
            if (login.EndsWith("@uiuc.edu"))
                login = login.Replace("@uiuc.edu", "@illinois.edu");
            return login;
        }

        /// <summary>
        /// Take a userID and create a password to be stuffed in the LoginProvider for
        /// users who have logged in "outside" of the system.  This allows us to include Externally
        /// Authneticated users in the Identity DB and gain the benefits
        /// of its RoleProvider and Personalization features.
        /// </summary>
        /// <param name="userID">user ID in form netid@domain.edu</param>
        /// <returns>Generated Password for userID</returns>
        public static string PasswordMixup(string userID)
        {
            string xtra = userID + "/xt93H";
            byte[] data = new byte[xtra.Length];

            for (int i = 0; i < xtra.Length; i++)
                data[i] = Convert.ToByte(xtra[i]);

            SHA1 sha = new SHA1CryptoServiceProvider();
            byte[] result = sha.ComputeHash(data);
            return Convert.ToBase64String(result);
        }

        public static string TrimNetid(string userID)
        {
            if (userID.Contains("@"))
                return userID.Substring(0, userID.IndexOf("@"));
            else
                return userID;
        }
        public static string GeneratedSecurityQuestion()
        { return "What Security Question?"; }

        public static string GeneratedSecurityAnswer(string userID)
        { return PasswordMixup(userID); }
    }
}
