﻿using Autofac;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Controllers;
using Znode.Engine.WebStore.Maps;
using Znode.Libraries.Framework.Business;
namespace Znode.Engine.WebStore
{
    public class CustomDependancyRegistration : IDependencyRegistration
    {
        public virtual void Register(ContainerBuilder builder)
        {
            //builder.RegisterType<CustomUserController>().As<UserController>().InstancePerDependency();
            builder.RegisterType<ShibUserController>().As<UserController>().InstancePerDependency();
            builder.RegisterType<IllinoisProductAgent>().As<IProductAgent>().InstancePerDependency();
            builder.RegisterType<CustomCheckoutController>().As<CheckoutController>().InstancePerDependency();
            builder.RegisterType<IllinoisUserAgent>().As<IUserAgent>().InstancePerDependency();
            builder.RegisterType<CustomPaymentAgent>().As<IPaymentAgent>().InstancePerDependency();
            builder.RegisterType<IllinoisUserViewModelMap>().As<UserViewModelMap>().InstancePerDependency();
        }
        public int Order
        {
            get { return 1; }
        }
    }
}