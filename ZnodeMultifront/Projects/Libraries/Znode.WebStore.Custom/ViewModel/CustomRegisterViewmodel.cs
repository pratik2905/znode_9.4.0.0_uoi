﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.WebStore.ViewModels
{
    public class CustomRegisterViewModel : RegisterViewModel
    {
        [Required(ErrorMessage="First Name is Required")]
        public string FirstName { get; set; }
        [Required(ErrorMessage ="Last Name is Required")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Address is Required")]
        public string Address1 { get; set; }
      
        public string Address2 { get; set; }
        [Required(ErrorMessage = "State is Required")]
        public string StateName { get; set; }
        [Required(ErrorMessage = "City is Required")]
        public string CityName { get; set; }
        [Required(ErrorMessage = "Postal Code is Required")]
        public string PostalCode { get; set; }
        //[Required(ErrorMessage = "Email is Required")]
        //public string email { get; set; }

        public string UofIUserName { get { return GenerateUserName(); } }

        private string GenerateUserName()
        {
            string trimmedEmail = Email.Trim();
            int atIndex = trimmedEmail.IndexOf("@");
            int domainIndex = trimmedEmail.LastIndexOf(".");
            //take up to the first 5 characters of the domain (or all of it if it is <= 5)
            int amountofdomain = (domainIndex - (atIndex + 1)) > 5 ? 5 : (domainIndex - (atIndex + 1));
            //Max netid is 20 characters, so take 17 of login+domain and append XUI
            string returnedNetidbase = string.Format("{0}{1}", trimmedEmail.Substring(0, atIndex), trimmedEmail.Substring(atIndex + 1, amountofdomain));
            int amountofbasetotake = returnedNetidbase.Length > 17 ? 17 : returnedNetidbase.Length;
            return string.Format("{0}XUI", returnedNetidbase.Substring(0,amountofbasetotake));
        }

    }
}
