﻿using System;
using System.Collections.Generic;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Model.Custom
{
  
    public class iPayRecordModel : BaseModel
    {
        public string ReferenceId { get; set; }
        public Nullable<System.DateTime> TransactionStartTime { get; set; }
        public string Token { get; set; }
        public string TransactionId { get; set; }
        public string BannerAccount { get; set; }
        public decimal Amount { get; set; }
        public Nullable<int> RegistrationResponseCode { get; set; }
        public Nullable<int> ResultResponseCode { get; set; }
        public Nullable<int> CaptureResponseCode { get; set; }
        public bool Testing { get; set; }
    }
}
