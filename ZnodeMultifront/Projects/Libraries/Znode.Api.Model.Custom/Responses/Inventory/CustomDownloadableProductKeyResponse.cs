﻿using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Model.Custom.Responses
{
    public class CustomDownloadableProductKeyResponse : BaseResponse
    {
        public CustomDownloadableProductKeyModel CusotmDownloadableProductKey { get; set; }
    }
}
