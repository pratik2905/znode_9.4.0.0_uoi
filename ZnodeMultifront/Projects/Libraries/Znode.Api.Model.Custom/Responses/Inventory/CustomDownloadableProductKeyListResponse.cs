﻿
using System.Collections.Generic;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Model.Custom.Responses
{
    public class CustomDownloadableProductKeyListResponse : BaseListResponse
    {
        public List<CustomDownloadableProductKeyModel> CustomDownloadableProductKeys { get; set; }
    }
}