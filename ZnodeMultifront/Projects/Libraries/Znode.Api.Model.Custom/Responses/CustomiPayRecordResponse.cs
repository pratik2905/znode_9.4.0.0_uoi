﻿using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Model.Custom.Responses
{
    public class CustomiPayRecordResponse : BaseResponse
    {
            public iPayRecordModel iPayRecord { get; set; }
        
    }
}
