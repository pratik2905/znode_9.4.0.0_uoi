﻿using System.Collections.Generic;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Model.Custom
{
    public class CustomPortalDetailListModel : PortalListModel
    {
        public List<CustomPortalDetailModel> CustomPortalDetailList { get; set; }
    }
}
