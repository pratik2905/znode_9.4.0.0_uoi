﻿using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Model.Custom
{
    public class CustomPortalModel: BaseModel
    {
        public PortalModel Portal { get; set; }
        public CustomPortalDetailModel PortalCustomDetail { get; set; }
    }
}
