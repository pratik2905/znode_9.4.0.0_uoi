﻿using System.Collections.Generic;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Model.Custom
{
    public class CustomDownloadableProductKeyListModel : DownloadableProductKeyListModel
    {
        public List<CustomDownloadableProductKeyModel> CustomDownloadableProductKeys { get; set; }

        public CustomDownloadableProductKeyListModel()
        {
            CustomDownloadableProductKeys = new List<CustomDownloadableProductKeyModel>();
        }
    }
}
