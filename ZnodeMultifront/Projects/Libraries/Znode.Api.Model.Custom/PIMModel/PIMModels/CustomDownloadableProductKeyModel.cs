﻿using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Model.Custom
{
    public class CustomDownloadableProductKeyModel : DownloadableProductKeyModel
    {
        public string Username { get; set; }
    }
}
