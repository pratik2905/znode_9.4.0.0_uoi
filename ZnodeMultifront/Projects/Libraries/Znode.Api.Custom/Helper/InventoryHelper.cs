﻿using GenericParsing;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Znode.Engine.Api.Models;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Api.Custom.Helper
{
    public class InventoryHelper
    {
        #region Public Methods
        //This method will process the data uploaded from the file.
        public int ProcessData(ImportModel model, int userId)
        {
            try
            {
                string uniqueIdentifier = string.Empty;
                string tableName = ReadAndCreateTable(model.FileName, model.ImportType, out uniqueIdentifier);
               // int templateId = UpsertTemplateDetails(model);
                return ProcessImportData(model, tableName, 1, uniqueIdentifier, userId);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage($"An error occured in Import Porcess. Error - {ex.Message.ToString()}", ZnodeLogging.Components.Import.ToString());
                return 0;
            }
        }
        
        //check the import status
        public virtual bool CheckImportStatus()
        {
            IZnodeViewRepository<View_ReturnBoolean> objStoredProc = new ZnodeViewRepository<View_ReturnBoolean>();
            objStoredProc.SetParameter("@Status", null, ParameterDirection.Output, DbType.Int32);
            int status = 0;

            IList<View_ReturnBoolean> result = objStoredProc.ExecuteStoredProcedureList("Znode_GetImportProcessStatus  @Status OUT", null, out status);

            //check if the records present or not. if no records present then don't show the error message
            if (result.FirstOrDefault().Id.Equals(0))
                return true;
            else
                return result.FirstOrDefault().Status.Value;
        }
        #endregion

        #region Private Methods
        //This method will read the file and create the table with the help of first row.  First row will act as a table column.
        //private string ReadAndCreateTable(string fileName, string importType, out string colNames)
        private string ReadAndCreateTable(string fileName, string importType, out string uniqueIdentifier)
        {
            string tableName = string.Empty;
            uniqueIdentifier = string.Empty;
            DataTable dt = new DataTable();
            importType = "ProductKeyUrl";
            try
            {
                using (GenericParserAdapter parser = new GenericParserAdapter(fileName))
                {
                    parser.ColumnDelimiter = ZnodeConstant.ColumnDelimiter;
                    parser.FirstRowHasHeader = true;
                    dt = parser.GetDataTable();
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage($"Something went wrong. Error message:- {ex.Message.ToString()}", ZnodeLogging.Components.Import.ToString());
                throw;
            }

            string[] columnNames = (from dc in dt.Columns.Cast<DataColumn>()
                                    select dc.ColumnName).ToArray();

            if (IsNotNull(columnNames) && columnNames.Count() > 0)
            {
                Guid tableGuid = Guid.NewGuid();
                //generate the doble hash temp table name
                tableName = $"tempdb..[##{importType}_{tableGuid}]";

                //add the same guid in datatable
                DataColumn guidCol = new DataColumn("guid", typeof(String));
                guidCol.DefaultValue = tableGuid;
                dt.Columns.Add(guidCol);

                //create the doble hash temp table
                MakeTable(tableName, GetTableColumnsFromFirstLine(columnNames));

                //assign the guid to out parameter
                uniqueIdentifier = Convert.ToString(tableGuid);
            }

            //If table created and it has headers then dump the CSV data in doble hash temp table
            if (HelperUtility.IsNotNull(tableName) && columnNames.Count() > 0)
                SaveDataInChunk(dt, tableName);

            return tableName;
        }

        //If TemplateId greater than 0 then we will update the template details else new template will get saved
       //This method will create ##temp table in SQL
        private void MakeTable(string tableName, string columnList)
        {
            SqlConnection conn = GetSqlConnection();
            SqlCommand cmd = new SqlCommand("CREATE TABLE " + tableName + "  " + columnList + " ", conn);

            if (conn.State.Equals(ConnectionState.Closed))
                conn.Open();

            cmd.ExecuteNonQuery();
        }

        //This method will divide the data in chunk.  The chunk size is 5000.
        private void SaveDataInChunk(DataTable dt, string tableName)
        {
            if (IsNotNull(dt) && dt.Rows.Count > 0)
            {
                int chunkSize = int.Parse(ConfigurationManager.AppSettings["ZnodeImportChunkLimit"].ToString());
                int startIndex = 0;
                int totalRows = dt.Rows.Count;
                int totalRowsCount = totalRows / chunkSize;

                if (totalRows % chunkSize > 0)
                    totalRowsCount++;

                for (int iCount = 0; iCount < totalRowsCount; iCount++)
                {
                    DataTable fileData = dt.Rows.Cast<DataRow>().Skip(startIndex).Take(chunkSize).CopyToDataTable();
                    startIndex = startIndex + chunkSize;
                    InsertData(tableName, fileData);
                }
            }
        }

        //This method will save the chunk data in ##temp table using Bulk upload
        private void InsertData(string tableName, DataTable fileData)
        {
            SqlConnection conn = GetSqlConnection();
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
            {
                bulkCopy.DestinationTableName = tableName;

                if (conn.State.Equals(ConnectionState.Closed))
                    conn.Open();

                bulkCopy.WriteToServer(fileData);
                conn.Close();
            }
        }

        //This method will create the table columns and append the datatype to the column headers
        private string GetTableColumnsFromFirstLine(string[] firstLine)
        {
            StringBuilder sbColumnList = new StringBuilder();
            sbColumnList.Append("(");
            sbColumnList.Append(string.Join(" nvarchar(max) , ", firstLine));
            sbColumnList.Append(" nvarchar(max), guid nvarchar(max) )");
            return sbColumnList.ToString();
        }

        //This method will execute the data inserted in the ##temp table
        //private void ProcessImportData(string importType, string tableName, string colNames)
        private int ProcessImportData(ImportModel model, string tableName, int templateId, string uniqueIdentifier, int userId)
        {
            try
            {
                string errorFileName = string.Empty;
                SqlConnection conn = GetSqlConnection();
                SqlCommand cmd = new SqlCommand("Znode_ImportPimDownloadableProduct", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TableName", tableName);
                cmd.Parameters.AddWithValue("@SKU", model.CountryCode);
                cmd.Parameters.AddWithValue("@Status", uniqueIdentifier);
                cmd.Parameters.AddWithValue("@UserId", userId);
                cmd.Parameters["@Status"].Direction = ParameterDirection.Output;

                if (conn.State.Equals(ConnectionState.Closed))
                    conn.Open();
                cmd.ExecuteReader();
                conn.Close();
                return 1;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message.ToString(), ZnodeLogging.Components.Import.ToString());
                return 0;
            }
        }

        //This method will provide the SQL connection
        private SqlConnection GetSqlConnection() => new SqlConnection(HelperMethods.ConnectionString);
        #endregion
    }
}
