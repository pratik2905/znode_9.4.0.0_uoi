﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Libraries.Admin;
using Znode.Libraries.ECommerce.Fulfillment;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Api.Custom.Helper
{
    public class CustomOrderReceipt : ZnodeOrderReceipt
    {
        #region Private Variables

        private readonly ZnodeOrderHelper orderHelper;
        private string _cultureCode = string.Empty;

        #endregion Private Variables

        #region Public Properties

        public new ZnodeOrderFulfillment Order { get; set; }
        public new ZnodeShoppingCart ShoppingCart { get; set; }
        public new string FeedbackUrl { get; set; }
        public new bool FromApi { get; set; }
        public new ZnodeShoppingCart ApiShoppingCart { get; set; }
        public new OrderModel OrderModel { get; set; }

        #endregion Public Properties

        #region Constructors

        public CustomOrderReceipt (ZnodeOrderFulfillment order):base(order)
        {
            Order = order;
        }

        public CustomOrderReceipt(OrderModel order) : base(order)
        {
            OrderModel = order;
            orderHelper = new ZnodeOrderHelper();
        }

        public CustomOrderReceipt(ZnodeOrderFulfillment order, ZnodeShoppingCart shoppingCart) : base(order, shoppingCart)
        {
            Order = order;
            ShoppingCart = shoppingCart;
        }

        public CustomOrderReceipt(ZnodeOrderFulfillment order, string feedbackUrl) : base(order, feedbackUrl)
        {
            Order = order;
            FeedbackUrl = feedbackUrl;
        }

        #endregion Constructors

        #region Private Methods

        //Create datatable for shipping
        public override DataTable CreateShippingTable(DataTable shippingTable)
        {
            // Additional info
            shippingTable.Columns.Add("BillingFirstName");
            shippingTable.Columns.Add("BillingLastName");
            shippingTable.Columns.Add("TrackingMessage");
            shippingTable.Columns.Add("Message");

            return shippingTable;
        }

        //Generates the HTML used in email receipts.
        public override string GenerateVendorProductOrderReceipt(string templateContent, string vendorCode)
            //TO set initial template for Order Receipt
            => CreateVendorProductOrderReceipt(templateContent, vendorCode);

        public override string GenerateHtmlResendReceiptWithParser(string receiptHtml)
        {
            if (string.IsNullOrEmpty(receiptHtml))
                return receiptHtml;

            //order to bind order details in data tabel
            DataTable orderTable = SetOrderData(OrderModel);

            //create order line Item
            DataTable orderlineItemTable = CreateOrderLineItemTable();

            // create returned order line Item
            DataTable returnedOrderlineItemTable = CreateReturnedOrderLineItemTable();

            //order to bind order amount details in data tabel
            DataTable orderAmountTable = SetOrderAmountData(OrderModel);

            // order to bind returned order amount details in data tabel
            DataTable returnedOrderAmountTable = SetReturnedOrderAmountData(OrderModel);

            //create multiple Address
            DataTable multipleAddressTable = CreateOrderAddressTable();

            //create multiple tax address
            DataTable multipleTaxAddressTable = CreateOrderTaxAddressTable();

            //bind line item data
            BuildOrderLineItem(multipleAddressTable, orderlineItemTable, multipleTaxAddressTable, OrderModel, returnedOrderlineItemTable);

            ZnodeReceiptHelper receiptHelper = new ZnodeReceiptHelper(receiptHtml);

            // Parse order table
            receiptHelper.Parse(orderTable.CreateDataReader());

            // Parse order line items table
            receiptHelper.Parse("AddressItems", multipleAddressTable.CreateDataReader());
            foreach (DataRow address in multipleAddressTable.Rows)
            {
                // Parse OrderLineItem
                var filterData = orderlineItemTable.DefaultView;
                filterData.RowFilter = $"OmsOrderShipmentID={address["OmsOrderShipmentID"]}";
                receiptHelper.Parse("LineItems" + address["OmsOrderShipmentID"], filterData.ToTable().CreateDataReader());

                //Parse Tax based on order shipment
                var amountFilterData = multipleTaxAddressTable.DefaultView;
                amountFilterData.RowFilter = $"OmsOrderShipmentID={address["OmsOrderShipmentID"]}";
                receiptHelper.Parse($"AmountLineItems{address["OmsOrderShipmentID"]}", amountFilterData.ToTable().CreateDataReader());
            }

            // Parse returned OrderLineItem
            var returnFilterData = returnedOrderlineItemTable.DefaultView;
            if (returnFilterData.Count > 0 && IsNotNull(returnFilterData))
                receiptHelper.Parse("ReturnLineItems", returnFilterData.ToTable().CreateDataReader());

            // Parse order amount table
            receiptHelper.Parse("GrandAmountLineItems", orderAmountTable.CreateDataReader());

            // Parse returned order amount table
            if (returnedOrderAmountTable.Rows.Count > 0 && IsNotNull(returnedOrderAmountTable))
                receiptHelper.Parse("ReturnedGrandAmountLineItems", returnedOrderAmountTable.CreateDataReader());
            //Replace the Email Template Keys, based on the passed email template parameters.

            // Return the HTML output
            return receiptHelper.Output;
        }

        // Builds the order line item table.
        public override void BuildOrderLineItem(DataTable multipleAddressTable, DataTable orderLineItemTable, DataTable multipleTaxAddressTable, OrderModel Order, DataTable returnedOrderlineItemTable)
        {
            List<OrderLineItemModel> OrderLineItemList = Order?.OrderLineItems.GroupBy(p => new { p.OmsOrderShipmentId }).Select(g => g.First()).ToList();
            IEnumerable<OrderShipmentModel> orderShipments = OrderLineItemList.Select(s => s.ZnodeOmsOrderShipment);

            int shipmentCounter = 1;

            foreach (OrderShipmentModel orderShipment in orderShipments)
            {
                DataRow addressRow = multipleAddressTable.NewRow();

                // If multiple shipping addresses then display the address for each group
                if (orderShipments.Count() > 1)
                {
                    addressRow["ShipmentNo"] = $"Shipment #{shipmentCounter++}{orderShipment.ShipName}";
                    addressRow["ShipTo"] = GetOrderShipmentAddress(orderShipment);
                }

                addressRow["OmsOrderShipmentID"] = orderShipment?.OmsOrderShipmentId;

                foreach (OrderLineItemModel lineitem in Order?.OrderLineItems?.Where(x => x.OmsOrderShipmentId == orderShipment.OmsOrderShipmentId)?.Reverse())
                {
                   
                    if (orderLineItemTable != null)
                        orderLineItemTable.Rows.Add(SetOrderLineItemTable(orderLineItemTable, lineitem,  lineitem.Price * lineitem.Quantity));
                }

                var globalResourceObject = HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleShipmentSubTotal");
                if (IsNotNull(globalResourceObject))
                    BuildOrderShipmentTotalLineItem(globalResourceObject.ToString(), Order.SubTotal, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                if (orderShipments.Count() > 1)
                {
                    BuildOrderShipmentTotalLineItem($"Shipping Cost({Order.ShippingTypeName})", Order.ShippingCost, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnSalesTax")) ? Admin_Resources.LabelSalesTax : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnSalesTax").ToString(), Order.SalesTax, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnVAT")) ? Admin_Resources.LabelVAT : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnVAT").ToString(), Order.VAT, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGST")) ? Admin_Resources.LabelGST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGST").ToString(), Order.GST, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnHST")) ? Admin_Resources.LabelHST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnHST").ToString(), Order.HST, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnPST")) ? Admin_Resources.LabelPST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnPST").ToString(), Order.PST, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);
                }
                multipleAddressTable.Rows.Add(addressRow);
            }

            // Returned line item
            foreach (OrderLineItemModel lineitem in Order?.ReturnedOrderLineItems ?? new List<OrderLineItemModel>())
            {
               
                if (returnedOrderlineItemTable != null)
                    returnedOrderlineItemTable.Rows.Add(SetReturnedOrderLineItemTable(returnedOrderlineItemTable, lineitem,  lineitem.Price * lineitem.Quantity));
            }
        }

        //Set Order Line Item Table.
        public override DataRow SetOrderLineItemTable(DataTable orderLineItemTable, OrderLineItemModel lineitem,  decimal extendedPrice)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(lineitem.ProductName + "<br />");
            DataRow orderlineItemDbRow = orderLineItemTable.NewRow();
            orderlineItemDbRow["Name"] = sb.ToString();
            orderlineItemDbRow["SKU"] = string.IsNullOrEmpty(lineitem?.OrderLineItemCollection?.FirstOrDefault()?.Sku) ? lineitem.Sku : lineitem?.OrderLineItemCollection.FirstOrDefault().Sku;
            orderlineItemDbRow["Description"] = lineitem.Description;
            orderlineItemDbRow["UOMDescription"] = string.Empty;
            orderlineItemDbRow["Quantity"] = Convert.ToString(lineitem.Quantity > 0 ? lineitem.Quantity : lineitem?.OrderLineItemCollection?.FirstOrDefault(x => x.Quantity > 0)?.Quantity);
            orderlineItemDbRow["Price"] = GetFormatPriceWithCurrency(lineitem.Price);
            orderlineItemDbRow["ExtendedPrice"] = GetFormatPriceWithCurrency(extendedPrice);
            orderlineItemDbRow["OmsOrderShipmentID"] = lineitem.OmsOrderShipmentId;
            orderlineItemDbRow["ShortDescription"] = string.Empty;
            orderlineItemDbRow["OrderLineItemState"] = lineitem.OrderLineItemState;
            orderlineItemDbRow["TrackingNumber"] = lineitem.TrackingNumber;
            orderlineItemDbRow["Column1"] = HttpUtility.HtmlDecode(lineitem.Attributes?.FirstOrDefault(x => x.AttributeCode.Equals("ReceiptCustomText"))?.AttributeValue);
            orderlineItemDbRow["Column2"] = string.Empty;
            orderlineItemDbRow["Column3"] = string.Empty;
            orderlineItemDbRow["Column4"] = string.Empty;
            orderlineItemDbRow["Column5"] = string.Empty;
            return orderlineItemDbRow;
        }

        // Set Returned Order Line Item Table.
        public override DataRow SetReturnedOrderLineItemTable(DataTable returnedOrderLineItemTable, OrderLineItemModel lineitem,  decimal extendedPrice)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(lineitem.ProductName + "<br />");
            DataRow orderlineItemDbRow = returnedOrderLineItemTable.NewRow();
            orderlineItemDbRow["ReturnedName"] = sb.ToString();
            orderlineItemDbRow["ReturnedSKU"] = lineitem.Sku;
            orderlineItemDbRow["ReturnedDescription"] = lineitem.Description;
            orderlineItemDbRow["ReturnedUOMDescription"] = string.Empty;
            orderlineItemDbRow["ReturnedQuantity"] = Convert.ToString(double.Parse(Convert.ToString(lineitem.Quantity)));
            orderlineItemDbRow["ReturnedPrice"] = GetFormatPriceWithCurrency(lineitem.Price);
            orderlineItemDbRow["ReturnedExtendedPrice"] = GetFormatPriceWithCurrency(extendedPrice);
            orderlineItemDbRow["ReturnedOmsOrderShipmentID"] = lineitem.OmsOrderShipmentId;
            orderlineItemDbRow["ReturnedShortDescription"] = string.Empty;
            return orderlineItemDbRow;
        }

        //to set order amount data
        public override DataTable SetOrderAmountData(OrderModel Order)
        {
            // Create order amount table
            DataTable orderAmountTable = CreateOrderAmountTable();

            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSubTotal")) ? Admin_Resources.LabelSubTotal : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSubTotal").ToString(), Order.SubTotal, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleTotalShippingCost")) ? $"{Admin_Resources.LabelTotalShippingCost}({ Order.ShippingTypeName })" : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleTotalShippingCost").ToString(), (Order.ShippingCost + Order.ShippingDifference), orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleTotalTaxCost")) ? Admin_Resources.LabelTaxCost : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleTotalTaxCost").ToString(), Order.TaxCost, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSalesTax")) ? Admin_Resources.LabelSalesTax : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSalesTax").ToString(), 0, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnVAT")) ? Admin_Resources.LabelVAT : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnVAT").ToString(), 0, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnHST")) ? Admin_Resources.LabelHST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnHST").ToString(), 0, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnPST")) ? Admin_Resources.LabelPST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnPST").ToString(), 0, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGST")) ? Admin_Resources.LabelGST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGST").ToString(), 0, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleDiscountAmount")) ? Admin_Resources.LabelDiscountAmount : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleDiscountAmount").ToString(), -Order.DiscountAmount, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleCSRDiscountAmount")) ? Admin_Resources.LabelCSRDiscount : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleCSRDiscountAmount").ToString(), -Order.CSRDiscountAmount, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGiftCardAmount")) ? Admin_Resources.LabelGiftCardAmount : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGiftCardAmount").ToString(), -Order.GiftCardAmount, orderAmountTable);

            return orderAmountTable;
        }

        // To set returned order amount data
        public override DataTable SetReturnedOrderAmountData(OrderModel order)
        {
            // Create order amount table
            DataTable orderAmountTable = CreateReturnedOrderAmountTable();
            BuildReturnedOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSubTotal")) ? Admin_Resources.LabelSubTotal : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSubTotal").ToString(), order.ReturnSubTotal, orderAmountTable);

            if (order?.ReturnItemList?.ReturnItemList.Any(o => o.IsShippingReturn) == true && order?.ReturnItemList?.ReturnItemList?.Sum(o => Convert.ToDecimal(o.ShippingCost)) > 0)
                BuildReturnedOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleTotalShippingCost")) ? $"{Admin_Resources.LabelTotalShippingCost}({ order.ShippingTypeName })" : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleTotalShippingCost").ToString(), order?.ReturnItemList?.ReturnItemList?.Where(o => o.IsShippingReturn == true)?.Sum(o => Convert.ToDecimal(o.ShippingCost)) ?? 0, orderAmountTable);

            BuildReturnedOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleTotalTaxCost")) ? Admin_Resources.LabelTaxCost : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleTotalTaxCost").ToString(), order.ReturnTaxCost, orderAmountTable);
            return orderAmountTable;
        }

        //to set order details
        public override DataTable SetOrderData(OrderModel Order)
        {
            // Create new row
            DataTable orderTable = CreateOrderTable();
            // Create new  for Shipping
            orderTable = CreateShippingTable(orderTable);
            DataRow orderRow = orderTable.NewRow();
            PortalModel portal = orderHelper.GetPortalDetailsByPortalId(Order.PortalId);
            _cultureCode = Order.CultureCode;
            // Additional info
            orderRow["SiteName"] = portal?.StoreName ?? ZnodeConfigManager.SiteConfig.StoreName;
            orderRow["StoreLogo"] = orderHelper.SetPortalLogo(Order.PortalId);
            orderRow["ReceiptText"] = string.Empty;
            orderRow["CustomerServiceEmail"] = FormatStringComma(portal?.CustomerServiceEmail) ?? FormatStringComma(ZnodeConfigManager.SiteConfig.CustomerServiceEmail);
            orderRow["CustomerServicePhoneNumber"] = portal?.CustomerServicePhoneNumber.Trim() ?? ZnodeConfigManager.SiteConfig.CustomerServicePhoneNumber.Trim();
            orderRow["FeedBack"] = FeedbackUrl;
            orderRow["ShippingName"] = Order?.ShippingTypeName;

            //Payment info
            if (!String.IsNullOrEmpty(Order.PaymentTransactionToken))
            {
                orderRow["CardTransactionID"] = Order.PaymentTransactionToken;
                orderRow["CardTransactionLabel"] = Admin_Resources.LabelTransactionId;
            }

            orderRow["PaymentName"] = Order.PaymentDisplayName;

            if (!String.IsNullOrEmpty(Order.PurchaseOrderNumber))
            {
                orderRow["PONumber"] = Order.PurchaseOrderNumber;
                orderRow["PurchaseNumberLabel"] = Admin_Resources.LabelPurchaseOrderNumber;
            }

            //Customer info
            orderRow["OrderId"] = Order.OrderNumber;
            orderRow["OrderDate"] = Order.OrderDate.ToShortDateString();

            orderRow["BillingAddress"] = Order.BillingAddressHtml;
            orderRow["PromotionCode"] = Order.CouponCode;

            orderRow["ShippingAddress"] = Order.OrderLineItems.FirstOrDefault()?.ShippingAddressHtml;

            orderRow["TotalCost"] = GetFormatPriceWithCurrency(Order.Total);

            // Returned total amount
            orderRow["ReturnedTotalCost"] = GetFormatPriceWithCurrency(Order.ReturnTotal + (Order?.ReturnItemList?.ReturnItemList?.Where(o => o.IsShippingReturn == true)?.Sum(o => Convert.ToDecimal(o.ShippingCost)) ?? 0));
            if (Order.AdditionalInstructions != null)
            {
                orderRow["AdditionalInstructions"] = Order.AdditionalInstructions;
                orderRow["AdditionalInstructLabel"] = Admin_Resources.LabelAdditionalNotes;
            }

            // Additional info for shipping
            orderRow["TrackingNumber"] = Order?.TrackingNumber;
            orderRow["BillingFirstName"] = Order?.BillingAddress?.FirstName;
            orderRow["BillingLastName"] = Order?.BillingAddress?.LastName;

            var shippedLineItemId = Order?.OrderLineItems?.Where(y => y?.OrderLineItemState?.ToLower() == ZnodeOrderStatusEnum.SHIPPED.ToString().ToLower()).Select(x => x.OmsOrderLineItemsId).ToArray();

            if (!string.IsNullOrEmpty(Order?.TrackingNumber))
            {
                orderRow["TrackingMessage"] = Equals(Order.OrderState, ZnodeOrderStatusEnum.SHIPPED.ToString()) ? Admin_Resources.ShippingTrackingNoMessage + SetTrackingUrl(Order?.TrackingNumber, Order?.TrackingUrl) : string.Empty;
                orderRow["Message"] = string.Format(Admin_Resources.ShippingStatusMessage, Order?.OrderState?.ToLower()) + (Equals(Order?.OrderState, ZnodeOrderStatusEnum.SHIPPED.ToString()) ? Admin_Resources.TrackingPackageMessage : string.Empty);
            }
            else if (shippedLineItemId.Count() > 0)
            {
                orderRow["TrackingMessage"] = string.Empty;
                orderRow["Message"] = string.Format(Admin_Resources.ShippingStatusMessage, ZnodeOrderStatusEnum.SHIPPED.ToString().ToLower()) + Admin_Resources.TrackingPackageMessage;
            }
            else
            {
                orderRow["TrackingMessage"] = string.Empty;
                orderRow["Message"] = string.Format(Admin_Resources.ShippingStatusMessage, Order?.OrderState?.ToLower());
            }

            // Add rows to order table
            orderTable.Rows.Add(orderRow);
            return orderTable;
        }

        public override string CreateOrderReceipt(string template)
        {
            if (string.IsNullOrEmpty(template))
                return template;

            //order to bind order details in data tabel
            System.Data.DataTable orderTable = SetOrderData();

            //create order line Item
            DataTable orderlineItemTable = CreateOrderLineItemTable();

            //order to bind order amount details in data tabel
            DataTable orderAmountTable = SetOrderAmountData();

            //create multiple Address
            DataTable multipleAddressTable = CreateOrderAddressTable();

            //create multiple tax address
            DataTable multipleTaxAddressTable = CreateOrderTaxAddressTable();

            //bind line item data
            BuildOrderLineItem(multipleAddressTable, orderlineItemTable, multipleTaxAddressTable);

            ZnodeReceiptHelper receiptHelper = new ZnodeReceiptHelper(template);

            // Parse order table
            receiptHelper.Parse(orderTable.CreateDataReader());

            // Parse order line items table
            receiptHelper.Parse("AddressItems", multipleAddressTable.CreateDataReader());
            foreach (DataRow address in multipleAddressTable.Rows)
            {
                // Parse OrderLineItem
                var filterData = orderlineItemTable.DefaultView;
                filterData.RowFilter = $"OmsOrderShipmentID={address["OmsOrderShipmentID"]}";
                receiptHelper.Parse("LineItems" + address["OmsOrderShipmentID"], filterData.ToTable().CreateDataReader());

                //Parse Tax based on order shipment
                var amountFilterData = multipleTaxAddressTable.DefaultView;
                amountFilterData.RowFilter = $"OmsOrderShipmentID={address["OmsOrderShipmentID"]}";
                receiptHelper.Parse($"AmountLineItems{address["OmsOrderShipmentID"]}", amountFilterData.ToTable().CreateDataReader());
            }
            // Parse order amount table
            receiptHelper.Parse("GrandAmountLineItems", orderAmountTable.CreateDataReader());
            //Replace the Email Template Keys, based on the passed email template parameters.

            // Return the HTML output
            return receiptHelper.Output;
        }

        //to create order table
        public override DataTable CreateOrderTable()
        {
            DataTable orderTable = new DataTable();
            // Additional info
            orderTable.Columns.Add("SiteName");
            orderTable.Columns.Add("StoreLogo");
            orderTable.Columns.Add("ReceiptText");
            orderTable.Columns.Add("CustomerServiceEmail");
            orderTable.Columns.Add("CustomerServicePhoneNumber");
            orderTable.Columns.Add("FeedBack");
            orderTable.Columns.Add("AdditionalInstructions");
            orderTable.Columns.Add("AdditionalInstructLabel");

            // Payment info
            orderTable.Columns.Add("CardTransactionID");
            orderTable.Columns.Add("CardTransactionLabel");
            orderTable.Columns.Add("PaymentName");

            orderTable.Columns.Add("PONumber");
            orderTable.Columns.Add("PurchaseNumberLabel");

            // Customer info
            orderTable.Columns.Add("OrderId");
            orderTable.Columns.Add("OrderDate");
            orderTable.Columns.Add("UserId");
            orderTable.Columns.Add("BillingAddress");
            orderTable.Columns.Add("ShippingAddress");
            orderTable.Columns.Add("PromotionCode");
            orderTable.Columns.Add("TotalCost");
            // Returned total cost
            orderTable.Columns.Add("ReturnedTotalCost");
            orderTable.Columns.Add("StyleSheetPath");

            orderTable.Columns.Add("ShippingName");
            orderTable.Columns.Add("TrackingNumber");
            return orderTable;
        }

        //to create order amount table
        public override DataTable CreateOrderAmountTable()
        {
            DataTable orderAmountTable = new DataTable();
            orderAmountTable.Columns.Add("Title");
            orderAmountTable.Columns.Add("Amount");
            return orderAmountTable;
        }

        // To create returned order amount table
        public override DataTable CreateReturnedOrderAmountTable()
        {
            DataTable orderAmountTable = new DataTable();
            orderAmountTable.Columns.Add("ReturnedTitle");
            orderAmountTable.Columns.Add("ReturnedAmount");
            return orderAmountTable;
        }

        //to create order order line item table
        public override DataTable CreateOrderLineItemTable()
        {
            DataTable orderlineItemTable = new DataTable();
            orderlineItemTable.Columns.Add("Name");
            orderlineItemTable.Columns.Add("SKU");
            orderlineItemTable.Columns.Add("Quantity");
            orderlineItemTable.Columns.Add("Description");
            orderlineItemTable.Columns.Add("UOMDescription");
            orderlineItemTable.Columns.Add("Price");
            orderlineItemTable.Columns.Add("ExtendedPrice");
            orderlineItemTable.Columns.Add("OmsOrderShipmentID");
            orderlineItemTable.Columns.Add("ShortDescription");
            orderlineItemTable.Columns.Add("ShippingId");
            orderlineItemTable.Columns.Add("OrderLineItemState");
            orderlineItemTable.Columns.Add("TrackingNumber");
            orderlineItemTable.Columns.Add("Column1");
            orderlineItemTable.Columns.Add("Column2");
            orderlineItemTable.Columns.Add("Column3");
            orderlineItemTable.Columns.Add("Column4");
            orderlineItemTable.Columns.Add("Column5");
            return orderlineItemTable;
        }

        // To create returned order line item table
        public override DataTable CreateReturnedOrderLineItemTable()
        {
            DataTable returnedOrderlineItemTable = new DataTable();
            returnedOrderlineItemTable.Columns.Add("ReturnedName");
            returnedOrderlineItemTable.Columns.Add("ReturnedSKU");
            returnedOrderlineItemTable.Columns.Add("ReturnedQuantity");
            returnedOrderlineItemTable.Columns.Add("ReturnedDescription");
            returnedOrderlineItemTable.Columns.Add("ReturnedUOMDescription");
            returnedOrderlineItemTable.Columns.Add("ReturnedPrice");
            returnedOrderlineItemTable.Columns.Add("ReturnedExtendedPrice");
            returnedOrderlineItemTable.Columns.Add("ReturnedOmsOrderShipmentID");
            returnedOrderlineItemTable.Columns.Add("ReturnedShortDescription");
            returnedOrderlineItemTable.Columns.Add("ReturnedShippingId");
            return returnedOrderlineItemTable;
        }

        //to create order address table
        public override DataTable CreateOrderAddressTable()
        {
            DataTable multipleAddressTable = new DataTable();
            multipleAddressTable.Columns.Add("ShipTo");
            multipleAddressTable.Columns.Add("OmsOrderShipmentID");
            multipleAddressTable.Columns.Add("ShipmentNo");
            return multipleAddressTable;
        }

        //to create order address table
        public override DataTable CreateOrderTaxAddressTable()
        {
            DataTable multipleTaxAddressTable = new DataTable();
            multipleTaxAddressTable.Columns.Add("OmsOrderShipmentID");
            multipleTaxAddressTable.Columns.Add("Title");
            multipleTaxAddressTable.Columns.Add("Amount");
            return multipleTaxAddressTable;
        }

        //to set order details
        public override DataTable SetOrderData()
        {
            // Create new row
            DataTable orderTable = CreateOrderTable();
            DataRow orderRow = orderTable.NewRow();
            ZnodeOrderHelper helper = new ZnodeOrderHelper();
            PortalModel portal = helper.GetPortalDetailsByPortalId(Order.PortalId);
            _cultureCode = Order.CultureCode;
            // Additional info
            orderRow["SiteName"] = portal?.StoreName ?? ZnodeConfigManager.SiteConfig.StoreName;
            orderRow["StoreLogo"] = helper.SetPortalLogo(Order.PortalId);
            orderRow["ReceiptText"] = string.Empty;
            orderRow["CustomerServiceEmail"] = FormatStringComma(portal?.CustomerServiceEmail) ?? FormatStringComma(ZnodeConfigManager.SiteConfig.CustomerServiceEmail);
            orderRow["CustomerServicePhoneNumber"] = portal?.CustomerServicePhoneNumber.Trim() ?? ZnodeConfigManager.SiteConfig.CustomerServicePhoneNumber.Trim();
            orderRow["FeedBack"] = FeedbackUrl;
            orderRow["ShippingName"] = Order?.ShippingName;

            //Payment info
            if (!String.IsNullOrEmpty(Order.PaymentTrancationToken))
            {
                orderRow["CardTransactionID"] = Order.PaymentTrancationToken;
                orderRow["CardTransactionLabel"] = Admin_Resources.LabelTransactionId;
            }

            orderRow["PaymentName"] = ShoppingCart.Payment.PaymentDisplayName;

            if (!String.IsNullOrEmpty(Order.PurchaseOrderNumber))
            {
                orderRow["PONumber"] = Order.PurchaseOrderNumber;
                orderRow["PurchaseNumberLabel"] = Admin_Resources.LabelPurchaseOrderNumber;
            }

            //Customer info
            orderRow["OrderId"] = Order?.Order?.OrderNumber;
            orderRow["OrderDate"] = Order.OrderDateWithTime;

            orderRow["BillingAddress"] = GetOrderBillingAddress(Order.BillingAddress);
            orderRow["PromotionCode"] = Order.CouponCode;

            var addresses = ((ZnodePortalCart)ShoppingCart).AddressCarts;
            orderRow["ShippingAddress"] = addresses.Count > 1 ? Admin_Resources.MessageKeyShippingMultipleAddress : GetOrderShipmentAddress(Order.OrderLineItems.FirstOrDefault().ZnodeOmsOrderShipment);

            orderRow["TotalCost"] = GetFormatPriceWithCurrency(Order.Total);
            if (Order.AdditionalInstructions != null)
            {
                orderRow["AdditionalInstructions"] = Order.AdditionalInstructions;
                orderRow["AdditionalInstructLabel"] = Admin_Resources.LabelAdditionalNotes;
            }
            // Add rows to order table
            orderTable.Rows.Add(orderRow);
            return orderTable;
        }

        //to set order amount data
        public override DataTable SetOrderAmountData()
        {
            // Create order amount table
            DataTable orderAmountTable = CreateOrderAmountTable();

            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSubTotal")) ? Admin_Resources.LabelSubTotal : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSubTotal").ToString(), Order.SubTotal, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleTotalShippingCost")) ? $"{Admin_Resources.LabelTotalShippingCost}({ Order.ShippingName})" : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleTotalShippingCost").ToString(), (Order.ShippingCost + Order.ShippingDifference), orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleTotalTaxCost")) ? Admin_Resources.LabelTaxCost : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleTotalTaxCost").ToString(), Order.TaxCost, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSalesTax")) ? Admin_Resources.LabelSalesTax : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSalesTax").ToString(), Order.SalesTax, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnVAT")) ? Admin_Resources.LabelVAT : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnVAT").ToString(), Order.VAT, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnHST")) ? Admin_Resources.LabelHST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnHST").ToString(), Order.HST, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnPST")) ? Admin_Resources.LabelPST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnPST").ToString(), Order.PST, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGST")) ? Admin_Resources.LabelGST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGST").ToString(), Order.GST, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleDiscountAmount")) ? Admin_Resources.LabelDiscountAmount : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleDiscountAmount").ToString(), -Order.DiscountAmount, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleCSRDiscountAmount")) ? Admin_Resources.LabelCSRDiscount : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleCSRDiscountAmount").ToString(), -Order.CSRDiscountAmount, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGiftCardAmount")) ? Admin_Resources.LabelGiftCardAmount : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGiftCardAmount").ToString(), -Order.GiftCardAmount, orderAmountTable);

            return orderAmountTable;
        }

        // Builds the order amount table.
        public override void BuildOrderAmountTable(string title, decimal amount, DataTable orderAmountTable)
        {
            if (amount != 0)
            {
                var row = orderAmountTable.NewRow();
                row["Title"] = title;
                row["Amount"] = GetFormatPriceWithCurrency(amount);

                orderAmountTable.Rows.Add(row);
            }
        }

        // Builds the returned order amount table.
        public override void BuildReturnedOrderAmountTable(string title, decimal amount, DataTable orderAmountTable)
        {
            if (amount != 0)
            {
                var row = orderAmountTable.NewRow();
                row["ReturnedTitle"] = title;
                row["ReturnedAmount"] = GetFormatPriceWithCurrency(amount);

                orderAmountTable.Rows.Add(row);
            }
        }

        // Builds the order line item table.
        public override void BuildOrderLineItem(DataTable multipleAddressTable, DataTable orderLineItemTable, DataTable multipleTaxAddressTable)
        {
            List<OrderLineItemModel> OrderLineItemList = Order?.OrderLineItems.GroupBy(p => new { p.OmsOrderShipmentId }).Select(g => g.First()).ToList();
            IEnumerable<OrderShipmentModel> orderShipments = OrderLineItemList.Select(s => s.ZnodeOmsOrderShipment);

            int shipmentCounter = 1;

            foreach (var orderShipment in orderShipments)
            {
                var addressRow = multipleAddressTable.NewRow();

                // If multiple shipping addresses then display the address for each group
                if (orderShipments.Count() > 1)
                {
                    addressRow["ShipmentNo"] = $"Shipment #{shipmentCounter++}{orderShipment.ShipName}";
                    addressRow["ShipTo"] = GetOrderShipmentAddress(orderShipment);
                }

                addressRow["OmsOrderShipmentID"] = orderShipment.OmsOrderShipmentId;
                var counter = 0;

                foreach (OrderLineItemModel lineitem in Order.OrderLineItems.Where(x => x.OmsOrderShipmentId == orderShipment.OmsOrderShipmentId).AsEnumerable().Reverse())
                {
                    var shoppingCartItem = ((ZnodePortalCart)ShoppingCart).AddressCarts.Where(x => x.OrderShipmentID == orderShipment.OmsOrderShipmentId).SelectMany(x => x.ShoppingCartItems.Cast<ZnodeShoppingCartItem>().AsEnumerable().Reverse()).ElementAt(counter++);

                    StringBuilder sb = new StringBuilder();
                    sb.Append(lineitem.ProductName + "<br />");

                    if (!String.IsNullOrEmpty(shoppingCartItem.Product.DownloadLink.Trim()))
                    {
                        sb.Append("<a href='" + shoppingCartItem.Product.DownloadLink + "' target='_blank'>Download</a><br />");
                    }

                    foreach (OrderLineItemModel orderLineItem in lineitem.OrderLineItemCollection)
                        setGroupProductDetails(lineitem, orderLineItem);

                    //For binding personalise attribute to Name
                    sb.Append(GetPersonaliseAttributes(lineitem.PersonaliseValueList));

                    if (orderLineItemTable != null)
                    {
                        DataRow orderlineItemDbRow = orderLineItemTable.NewRow();
                        orderlineItemDbRow["Name"] = sb.ToString();
                        orderlineItemDbRow["SKU"] = lineitem.Sku;
                        orderlineItemDbRow["Description"] = lineitem.Description;
                        orderlineItemDbRow["UOMDescription"] = string.Empty;
                        orderlineItemDbRow["Quantity"] = lineitem.OrderLineItemRelationshipTypeId.Equals((int)ZnodeCartItemRelationshipTypeEnum.Group) ? lineitem.GroupProductQuantity : Convert.ToString(double.Parse(Convert.ToString(lineitem.Quantity)));
                        orderlineItemDbRow["Price"] = GetFormatPriceWithCurrency(shoppingCartItem.UnitPrice, shoppingCartItem.UOM);
                        orderlineItemDbRow["ExtendedPrice"] = GetFormatPriceWithCurrency(shoppingCartItem.ExtendedPrice);
                        orderlineItemDbRow["OmsOrderShipmentID"] = lineitem.OmsOrderShipmentId;
                        orderlineItemDbRow["ShortDescription"] = shoppingCartItem.Product.ShortDescription;
                        orderlineItemDbRow["Column1"] = HttpUtility.HtmlDecode(lineitem.Attributes?.FirstOrDefault(x => x.AttributeCode.Equals("ReceiptCustomText"))?.AttributeValue);
                        orderLineItemTable.Rows.Add(orderlineItemDbRow);
                    }
                }

                var addressCart = ((ZnodePortalCart)ShoppingCart).AddressCarts.FirstOrDefault(y => y.OrderShipmentID == orderShipment.OmsOrderShipmentId);

                if (addressCart != null && orderShipments.Count() > 1)
                {
                    var globalResourceObject = HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleShipmentSubTotal");
                    if (globalResourceObject != null)
                        BuildOrderShipmentTotalLineItem(globalResourceObject.ToString(), addressCart.SubTotal, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem($"Shipping Cost({addressCart.Shipping.ShippingName})", addressCart.ShippingCost, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnSalesTax")) ? Admin_Resources.LabelSalesTax : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnSalesTax").ToString(), addressCart.SalesTax, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnVAT")) ? Admin_Resources.LabelVAT : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnVAT").ToString(), addressCart.VAT, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGST")) ? Admin_Resources.LabelGST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGST").ToString(), addressCart.GST, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnHST")) ? Admin_Resources.LabelHST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnHST").ToString(), addressCart.HST, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnPST")) ? Admin_Resources.LabelPST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnPST").ToString(), addressCart.PST, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);
                }
                multipleAddressTable.Rows.Add(addressRow);
            }
        }

        // Builds the Shipment order line item table.
        public override void BuildOrderShipmentTotalLineItem(string title, decimal amount, int OmsOrderShipmentId, DataTable taxTable)
        {
            if (amount > 0)
            {
                var taxAddressRow = taxTable.NewRow();
                taxAddressRow["Title"] = title;
                taxAddressRow["Amount"] = GetFormatPriceWithCurrency(amount);
                taxAddressRow["OmsOrderShipmentID"] = OmsOrderShipmentId;
                taxTable.Rows.Add(taxAddressRow);
            }
        }

        //to get order shipment address
        public override string GetOrderShipmentAddress(OrderShipmentModel orderShipment)
        {
            if (IsNotNull(orderShipment))
            {
                string street1 = string.IsNullOrEmpty(orderShipment.ShipToStreet2) ? string.Empty : "<br />" + orderShipment.ShipToStreet2;
                orderShipment.ShipToCompanyName = IsNotNull(orderShipment?.ShipToCompanyName) ? $"{orderShipment?.ShipToCompanyName}{"<br />"}" : Order.ShippingAddress.CompanyName;
                return $"{orderShipment?.ShipToFirstName}{" "}{ orderShipment?.ShipToLastName}{"<br />"}{ orderShipment.ShipToCompanyName}{"<br />"}{orderShipment.ShipToStreet1}{street1}{"<br />"}{ orderShipment.ShipToCity}{"<br />"}{orderShipment.ShipToStateCode}{"<br />"}{orderShipment.ShipToPostalCode}{"<br />"}{orderShipment.ShipToCountry}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderShipment.ShipToPhoneNumber}";
            }
            return string.Empty;
        }

        //to get shipping address
        public override string GetOrderBillingAddress(AddressModel orderBilling)
        {
            if (IsNotNull(orderBilling))
            {
                string street2 = string.IsNullOrEmpty(orderBilling.Address2) ? string.Empty : "<br />" + orderBilling.Address2;
                return $"{orderBilling?.FirstName}{" "}{orderBilling?.LastName}{"<br />"}{orderBilling?.CompanyName}{"<br />"}{orderBilling.Address1}{street2}{"<br />"}{ orderBilling.CityName}{","}{"&nbsp;"}{orderBilling.StateName}{"&nbsp;"}{orderBilling.CountryName}{","}{"&nbsp;"}{orderBilling.PostalCode}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderBilling.PhoneNumber}";
            }
            return string.Empty;
        }

        //For Getting personalise attibute.
        public override string GetPersonaliseAttributes(Dictionary<string, object> personaliseValueList)
        {
            string personaliseAttibuteHtml = string.Empty;
            if (IsNotNull(personaliseValueList))
            {
                foreach (var personaliseAttibute in personaliseValueList)
                    personaliseAttibuteHtml += $"{"<p>"} { personaliseAttibute.Key}{":"}{personaliseAttibute.Value}{"</p>"}";

                return personaliseAttibuteHtml;
            }
            return string.Empty;
        }

        //to add space after comma
        public override string FormatStringComma(string input)
        {
            return input.Replace(",", ", ");
        }

        //Create Vendor Product Order Receipt.
        public override string CreateVendorProductOrderReceipt(string template, string vendorCode)
        {
            if (string.IsNullOrEmpty(template))
                return template;

            //order to bind order details in data tabel
            DataTable orderTable = SetOrderData();

            //create order line Item
            DataTable orderlineItemTable = CreateOrderLineItemTable();

            //create multiple Address
            DataTable multipleAddressTable = CreateOrderAddressTable();

            //bind line item data
            BuildOrderLineItemForVendorProduct(multipleAddressTable, orderlineItemTable, vendorCode);

            ZnodeReceiptHelper receiptHelper = new ZnodeReceiptHelper(template);

            // Parse order table
            receiptHelper.Parse(orderTable.CreateDataReader());

            // Parse order line items table
            receiptHelper.Parse("AddressItems", multipleAddressTable.CreateDataReader());
            foreach (DataRow address in multipleAddressTable.Rows)
            {
                // Parse OrderLineItem
                var filterData = orderlineItemTable.DefaultView;
                filterData.RowFilter = $"OmsOrderShipmentID={address["OmsOrderShipmentID"]}";
                receiptHelper.Parse("LineItems" + address["OmsOrderShipmentID"], filterData.ToTable().CreateDataReader());
            }

            // Return the HTML output
            return receiptHelper.Output;
        }

        //Build Order LineI tem For Vendor Products.
        public override void BuildOrderLineItemForVendorProduct(DataTable multipleAddressTable, DataTable orderLineItemTable, string vendorCode)
        {
            List<OrderLineItemModel> OrderLineItemList = Order.OrderLineItems.Where(x => x.Vendor == vendorCode).GroupBy(p => new { p.OmsOrderShipmentId }).Select(g => g.First()).ToList();
            IEnumerable<OrderShipmentModel> orderShipments = OrderLineItemList.Select(s => s.ZnodeOmsOrderShipment);

            int shipmentCounter = 1;
            // get order line item as per order shipment.
            foreach (var orderShipment in orderShipments)
            {
                var addressRow = multipleAddressTable.NewRow();

                // If multiple shipping addresses then display the address for each group
                if (orderShipments.Count() > 1)
                {
                    addressRow["ShipmentNo"] = $"Shipment #{shipmentCounter++}{orderShipment.ShipName}";
                    addressRow["ShipTo"] = GetOrderShipmentAddress(orderShipment);
                }

                addressRow["OmsOrderShipmentID"] = orderShipment.OmsOrderShipmentId;
                var counter = 0;

                // get order line item of same shipment.
                foreach (OrderLineItemModel lineitem in Order.OrderLineItems.Where(x => x.OmsOrderShipmentId == orderShipment.OmsOrderShipmentId && x.Vendor == vendorCode))
                {
                    var shoppingCartItem = ((ZnodePortalCart)ShoppingCart).AddressCarts.Where(x => x.OrderShipmentID == orderShipment.OmsOrderShipmentId).SelectMany(x => x.ShoppingCartItems.Cast<ZnodeShoppingCartItem>().AsEnumerable().Reverse()).ElementAt(counter++);

                    StringBuilder sb = new StringBuilder();
                    sb.Append(lineitem.ProductName + "<br />");

                    //For binding personalise attribute to Name
                    sb.Append(GetPersonaliseAttributes(lineitem.PersonaliseValueList));
                    decimal extendedPrice = lineitem.OrderLineItemRelationshipTypeId.Equals((int)ZnodeCartItemRelationshipTypeEnum.Group) ? lineitem.Price : lineitem.Price * lineitem.Quantity;
                    if (IsNotNull(orderLineItemTable))
                    {
                        DataRow orderlineItemDbRow = orderLineItemTable.NewRow();
                        orderlineItemDbRow["Name"] = sb.ToString();
                        orderlineItemDbRow["SKU"] = lineitem.Sku;
                        orderlineItemDbRow["Description"] = lineitem.Description;
                        orderlineItemDbRow["UOMDescription"] = string.Empty;
                        orderlineItemDbRow["Quantity"] = lineitem.OrderLineItemRelationshipTypeId.Equals((int)ZnodeCartItemRelationshipTypeEnum.Group) ? lineitem.GroupProductQuantity : Convert.ToString(double.Parse(Convert.ToString(lineitem.Quantity)));
                        orderlineItemDbRow["Price"] = GetFormatPriceWithCurrency(shoppingCartItem.UnitPrice, shoppingCartItem.UOM);
                        orderlineItemDbRow["ExtendedPrice"] = GetFormatPriceWithCurrency(shoppingCartItem.ExtendedPrice);
                        orderlineItemDbRow["OmsOrderShipmentID"] = lineitem.OmsOrderShipmentId;
                        orderlineItemDbRow["ShortDescription"] = shoppingCartItem.Product.ShortDescription;
                        orderlineItemDbRow["Column1"] = HttpUtility.HtmlDecode(lineitem.Attributes?.FirstOrDefault(x => x.AttributeCode.Equals("ReceiptCustomText"))?.AttributeValue);
                        orderLineItemTable.Rows.Add(orderlineItemDbRow);
                    }
                }

                multipleAddressTable.Rows.Add(addressRow);
            }
        }

        public override void setGroupProductDetails(OrderLineItemModel lineitem, OrderLineItemModel orderLineItem)
        {
            if (orderLineItem.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Group)
            {
                lineitem.OrderLineItemRelationshipTypeId = (int)ZnodeCartItemRelationshipTypeEnum.Group;
                lineitem.GroupProductQuantity = lineitem.GroupProductQuantity + "<br/>" + double.Parse(orderLineItem.Quantity.ToString());
                lineitem.Description = lineitem.Description + "<br/>" + orderLineItem.ProductName;
                lineitem.GroupProductPrice = lineitem.GroupProductPrice + "<br/>" + GetFormatPriceWithCurrency(orderLineItem.Price);
                lineitem.Sku = orderLineItem.Sku;
            }
            if (orderLineItem.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Configurable)
            {
                lineitem.OrderLineItemRelationshipTypeId = (int)ZnodeCartItemRelationshipTypeEnum.Configurable;
                lineitem.Sku = orderLineItem.Sku;
                lineitem.Price = orderLineItem.Price;
                lineitem.Quantity = orderLineItem.Quantity;
            }
        }

        //to get amount with currency sybmol
        public override string GetFormatPriceWithCurrency(decimal priceValue, string uom = "")
             => ZnodeCurrencyManager.FormatPriceWithCurrency(priceValue, _cultureCode, uom);

        //Set Tracking Url.
        public override string SetTrackingUrl(string trackingNo, string trackingUrl)
            => IsNotNull(trackingUrl) ? $"<a target=_blank href={ trackingUrl + trackingNo }>{trackingNo} </ a >" : trackingNo;

        #endregion Private Methods

        #region Public Methods

        // Gets the HTML used when showing receipts in the UI.
        public override string GetOrderReceiptHtml(string templatePath)
        => GenerateOrderReceipt(templatePath);

        // Gets the HTML used when showing receipts in the UI.
        public override string GetVendorProductOrderReceiptHtml(string templatePath, string vendorCode)
            => GenerateVendorProductOrderReceipt(templatePath, vendorCode);

        public override string GetOrderResendReceiptHtml(string templatePath)
        => GenerateHtmlResendReceiptWithParser(templatePath);

        //Generates the HTML used in email receipts.
        public override string GenerateOrderReceipt(string templateContent)
        {
            //TO set initial template for Order Receipt
            string receiptTemplate = templateContent;

            return CreateOrderReceipt(receiptTemplate);
        }

        #region Downloadable product keys receipt.
        //Generates the HTML used in email receipts.
        public override string GenerateProductKeysOrderReceipt(string templateContent, DownloadableProductKeyListModel key)
        {
            //To set initial template for Order Receipt.
            string receiptTemplate = templateContent;

            return CreateProductKeysOrderReceipt(receiptTemplate, key);
        }

        // Gets the HTML used when showing receipts in the UI.
        public override string GetProductKeysOrderReceiptHtml(string templatePath, DownloadableProductKeyListModel key)
        => GenerateProductKeysOrderReceipt(templatePath, key);


        // Builds the order line item table.
        public override void BuildOrderLineItemOfProductKeys(DataTable orderLineItemTable, DownloadableProductKeyListModel key)
        {
            //Added one column as keys.
            orderLineItemTable.Columns.Add("Keys");

            foreach (DownloadableProductKeyModel lineitem in key?.DownloadableProductKeys.AsEnumerable().Reverse())
            {
                if (orderLineItemTable != null)
                {
                    // Create new row
                    DataRow orderRow = orderLineItemTable.NewRow();
                    ZnodeOrderHelper helper = new ZnodeOrderHelper();
                    PortalModel portal = helper.GetPortalDetailsByPortalId(Order.PortalId);
                    _cultureCode = Order.CultureCode;

                    // Additional info
                    orderRow["SiteName"] = portal?.StoreName ?? ZnodeConfigManager.SiteConfig.StoreName;
                    orderRow["StoreLogo"] = helper.SetPortalLogo(Order.PortalId);
                    orderRow["ReceiptText"] = string.Empty;
                    orderRow["CustomerServiceEmail"] = FormatStringComma(portal?.CustomerServiceEmail) ?? FormatStringComma(ZnodeConfigManager.SiteConfig.CustomerServiceEmail);
                    orderRow["CustomerServicePhoneNumber"] = portal?.CustomerServicePhoneNumber.Trim() ?? ZnodeConfigManager.SiteConfig.CustomerServicePhoneNumber.Trim();
                    orderRow["FeedBack"] = FeedbackUrl;
                    orderRow["ShippingName"] = Order?.ShippingName;

                    //Payment info
                    if (!String.IsNullOrEmpty(Order.PaymentTrancationToken))
                    {
                        orderRow["CardTransactionID"] = Order.PaymentTrancationToken;
                        orderRow["CardTransactionLabel"] = Admin_Resources.LabelTransactionId;
                    }

                    orderRow["PaymentName"] = ShoppingCart.Payment.PaymentDisplayName;

                    if (!String.IsNullOrEmpty(Order.PurchaseOrderNumber))
                    {
                        orderRow["PONumber"] = Order.PurchaseOrderNumber;
                        orderRow["PurchaseNumberLabel"] = Admin_Resources.LabelPurchaseOrderNumber;
                    }

                    orderRow["Name"] = Order?.Order?.OrderNumber;

                    //Customer info
                    orderRow["OrderId"] = Order?.Order?.OrderNumber;
                    orderRow["OrderDate"] = Order.OrderDateWithTime;

                    orderRow["BillingAddress"] = GetOrderBillingAddress(Order.BillingAddress);
                    orderRow["PromotionCode"] = Order.CouponCode;

                    var addresses = ((ZnodePortalCart)ShoppingCart).AddressCarts;
                    orderRow["ShippingAddress"] = addresses.Count > 1 ? Admin_Resources.MessageKeyShippingMultipleAddress : GetOrderShipmentAddress(Order.OrderLineItems.FirstOrDefault().ZnodeOmsOrderShipment);

                    orderRow["TotalCost"] = GetFormatPriceWithCurrency(Order.Total);
                    if (Order.AdditionalInstructions != null)
                    {
                        orderRow["AdditionalInstructions"] = Order.AdditionalInstructions;
                        orderRow["AdditionalInstructLabel"] = Admin_Resources.LabelAdditionalNotes;
                    }

                    // DataRow orderlineItemDbRow = orderLineItemTable.NewRow();
                    orderRow["SKU"] = lineitem.SKU;
                    orderRow["Name"] = lineitem.ProductName;
                    orderRow["Keys"] = lineitem.DownloadableProductKey;
                    orderLineItemTable.Rows.Add(orderRow);
                }
            }
        }
        public  DataTable SetOrderDataForProductKey()
        {
            // Create new row
            DataTable orderTable = CreateOrderTable();
            DataRow orderRow = orderTable.NewRow();
            ZnodeOrderHelper helper = new ZnodeOrderHelper();
            PortalModel portal = helper.GetPortalDetailsByPortalId(Order.PortalId);
            _cultureCode = Order.CultureCode;
            // Additional info
            orderRow["SiteName"] = portal?.StoreName ?? ZnodeConfigManager.SiteConfig.StoreName;
            orderRow["StoreLogo"] = helper.SetPortalLogo(Order.PortalId);
            orderRow["ReceiptText"] = string.Empty;
            orderRow["CustomerServiceEmail"] = FormatStringComma(portal?.CustomerServiceEmail) ?? FormatStringComma(ZnodeConfigManager.SiteConfig.CustomerServiceEmail);
            orderRow["CustomerServicePhoneNumber"] = portal?.CustomerServicePhoneNumber.Trim() ?? ZnodeConfigManager.SiteConfig.CustomerServicePhoneNumber.Trim();
            orderRow["FeedBack"] = FeedbackUrl;
            orderRow["ShippingName"] = Order?.ShippingName;

            //Payment info
            if (!String.IsNullOrEmpty(Order.PaymentTrancationToken))
            {
                orderRow["CardTransactionID"] = Order.PaymentTrancationToken;
                orderRow["CardTransactionLabel"] = Admin_Resources.LabelTransactionId;
            }

            orderRow["PaymentName"] = ShoppingCart.Payment.PaymentDisplayName;

            if (!String.IsNullOrEmpty(Order.PurchaseOrderNumber))
            {
                orderRow["PONumber"] = Order.PurchaseOrderNumber;
                orderRow["PurchaseNumberLabel"] = Admin_Resources.LabelPurchaseOrderNumber;
            }

            //Customer info
            orderRow["OrderId"] = Order?.Order?.OrderNumber;
            orderRow["OrderDate"] = Order.OrderDateWithTime;

            orderRow["BillingAddress"] = GetOrderBillingAddress(Order.BillingAddress);
            orderRow["PromotionCode"] = Order.CouponCode;

            var addresses = ((ZnodePortalCart)ShoppingCart).AddressCarts;
            orderRow["ShippingAddress"] = addresses.Count > 1 ? Admin_Resources.MessageKeyShippingMultipleAddress : GetOrderShipmentAddress(Order.OrderLineItems.FirstOrDefault().ZnodeOmsOrderShipment);

            orderRow["TotalCost"] = GetFormatPriceWithCurrency(Order.Total);
            if (Order.AdditionalInstructions != null)
            {
                orderRow["AdditionalInstructions"] = Order.AdditionalInstructions;
                orderRow["AdditionalInstructLabel"] = Admin_Resources.LabelAdditionalNotes;
            }
            // Add rows to order table
            orderTable.Rows.Add(orderRow);
            return orderTable;
        }
       
        //to create order order line item table
        public override DataTable CreateOrderLineItemTableForProductKeys()
        {
            DataTable orderTable = new DataTable();
            orderTable.Columns.Add("Name");
            orderTable.Columns.Add("SKU");
            orderTable.Columns.Add("Quantity");
            // Additional info
            orderTable.Columns.Add("SiteName");
            orderTable.Columns.Add("StoreLogo");
            orderTable.Columns.Add("ReceiptText");
            orderTable.Columns.Add("CustomerServiceEmail");
            orderTable.Columns.Add("CustomerServicePhoneNumber");
            orderTable.Columns.Add("FeedBack");
            orderTable.Columns.Add("AdditionalInstructions");
            orderTable.Columns.Add("AdditionalInstructLabel");

            // Payment info
            orderTable.Columns.Add("CardTransactionID");
            orderTable.Columns.Add("CardTransactionLabel");
            orderTable.Columns.Add("PaymentName");

            orderTable.Columns.Add("PONumber");
            orderTable.Columns.Add("PurchaseNumberLabel");

            // Customer info
            orderTable.Columns.Add("OrderId");
            orderTable.Columns.Add("OrderDate");
            orderTable.Columns.Add("UserId");
            orderTable.Columns.Add("BillingAddress");
            orderTable.Columns.Add("ShippingAddress");
            orderTable.Columns.Add("PromotionCode");
            orderTable.Columns.Add("TotalCost");
            // Returned total cost
            orderTable.Columns.Add("ReturnedTotalCost");
            orderTable.Columns.Add("StyleSheetPath");

            orderTable.Columns.Add("ShippingName");
            orderTable.Columns.Add("TrackingNumber");

            return orderTable;
        }

        //Method to create keys for order receipt.
        public override string CreateProductKeysOrderReceipt(string template, DownloadableProductKeyListModel key)
        {
            if (string.IsNullOrEmpty(template))
                return template;

            //order to bind order details in data tabel
            DataTable orderTable = SetOrderDataForProductKey();
            //create order line Item
            DataTable orderlineItemTable = CreateOrderLineItemTableForProductKeys();
            //create order line Item
           
            BuildOrderLineItemOfProductKeys(orderlineItemTable, key);

            ZnodeReceiptHelper receiptHelper = new ZnodeReceiptHelper(template);

            receiptHelper.Parse(orderTable.CreateDataReader());
            // Parse order tablea
            receiptHelper.Parse(orderlineItemTable.CreateDataReader());

            // Return the HTML output
            return receiptHelper.Output;
        }

        #endregion
        #endregion Public Methods
    }
}