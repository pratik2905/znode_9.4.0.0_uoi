﻿using System;
using System.Data;
using System.Data.SqlClient;
using Znode.Engine.Api.Model.Custom;
using Znode.Libraries.Data.Helpers;

namespace Znode.Api.Custom.Helper
{
    class iPayHelper
    {
        private enum GetRecordOperation
        {
            SelectByReferenceID,
            SelectByToken
        }

        private enum SaveRecordOperation
        {
            Insert,
            Update
        }

        public iPayRecordModel GetByReferenceId(string ReferenceID)
        {
            return GetRecord(GetRecordOperation.SelectByReferenceID, ReferenceID);
        }

        public iPayRecordModel GetByToken(string Token)
        {
            return GetRecord(GetRecordOperation.SelectByToken, Token);
        }

        public bool Insert(iPayRecordModel model)
        {
            return SendRecord(SaveRecordOperation.Insert, model);
        }

        public bool Update(iPayRecordModel model)
        {
            return SendRecord(SaveRecordOperation.Update, model);
        }

        private bool SendRecord(SaveRecordOperation op, iPayRecordModel model)
        {
            string commandName = op == SaveRecordOperation.Insert ? "IllinoisiPay_Insert" : "IllinoisiPay_Update";
            try
            {
                
                SqlConnection conn = GetSqlConnection();
                SqlCommand cmd = new SqlCommand(commandName, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ReferenceId", model.ReferenceId);
                
                cmd.Parameters.AddWithValue("@TransactionStartTime", model.TransactionStartTime);
                cmd.Parameters.AddWithValue("@Token", model.Token);
                cmd.Parameters.AddWithValue("@TransactionId", string.IsNullOrEmpty(model.TransactionId) ? string.Empty : model.TransactionId);
                cmd.Parameters.AddWithValue("@BannerAccount", model.BannerAccount);
                cmd.Parameters.AddWithValue("@Amount", model.Amount);
                cmd.Parameters.AddWithValue("@RegistrationResponseCode", model.RegistrationResponseCode);
                cmd.Parameters.AddWithValue("@ResultResponseCode",(model.ResultResponseCode==null) ? -1 : model.ResultResponseCode);
                cmd.Parameters.AddWithValue("@CaptureResponseCode", (model.CaptureResponseCode == null) ? -1 : model.CaptureResponseCode);
                cmd.Parameters.AddWithValue("@Testing", model.Testing);
                
                             
                //cmd.Parameters["@Status"].Direction = ParameterDirection.Output;

                if (conn.State.Equals(ConnectionState.Closed))
                    conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private iPayRecordModel GetRecord(GetRecordOperation op, string key)
        {
            iPayRecordModel returnModel = new iPayRecordModel();

            string commandName = op == GetRecordOperation.SelectByReferenceID ? "IllinoisiPay_GetByReferenceId" : "IllinoisiPay_GetByToken";
            string parameterName = op == GetRecordOperation.SelectByReferenceID ? "@ReferenceId" : "@Token";
            try
            {
                SqlConnection conn = GetSqlConnection();
                SqlCommand cmd = new SqlCommand(commandName, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue(parameterName, key);

                if (conn.State.Equals(ConnectionState.Closed))
                    conn.Open();
                var resultReader = cmd.ExecuteReader();
                if (resultReader.Read())
                    FillModelFromReader(resultReader, returnModel);
                conn.Close();
            }
            catch (Exception ex)
            {
                return null;
            }

            return returnModel;
        }

        private void FillModelFromReader(SqlDataReader resultReader, iPayRecordModel returnModel)
        {
            int result;

            returnModel.ReferenceId = resultReader["ReferenceId"].ToString();
            if (int.TryParse(resultReader["RegistrationResponseCode"].ToString(), out result))
                returnModel.RegistrationResponseCode = result;
            if (int.TryParse(resultReader["ResultResponseCode"].ToString(), out result))
                returnModel.ResultResponseCode = result;
            returnModel.Testing = bool.Parse(resultReader["Testing"].ToString());
            returnModel.Token = resultReader["Token"].ToString();
            returnModel.TransactionId = resultReader["TransactionId"].ToString();
            returnModel.TransactionStartTime = DateTime.Parse(resultReader["TransactionStartTime"].ToString());
            returnModel.Amount = decimal.Parse(resultReader["Amount"].ToString());
            returnModel.BannerAccount = resultReader["BannerAccount"].ToString();
            if (int.TryParse(resultReader["CaptureResponseCode"].ToString(), out result))
                returnModel.CaptureResponseCode = result;
        }

        //This method will provide the SQL connection
        private SqlConnection GetSqlConnection() => new SqlConnection(HelperMethods.ConnectionString);
    }
}
