﻿using Autofac;
using Znode.Api.Custom.Cache;
using Znode.Api.Custom.Controller;
using Znode.Api.Custom.Service;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Controllers;
using Znode.Engine.Services;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Helper
{
    public class DependencyRegistration : IDependencyRegistration
    {
        /// <summary>
        /// Register the Dependency Injection types.
        /// </summary>
        /// <param name="builder">Autofac Container Builder</param>
        public virtual void Register(ContainerBuilder builder)
        {
            builder.RegisterType<CustomPortalService>().As<ICustomPortalService>().InstancePerRequest();

            //Here override znode base code method by injecting dependancy mention as below.
            //"In CustomPortalService.cs we have override 'DeletePortal()' of znode base code".
            builder.RegisterType<CustomPortalService>().As<IPortalService>().InstancePerRequest();

            builder.RegisterType<CustomOrderService>().As<ICustomOrderService>().InstancePerRequest();
            builder.RegisterType<CustomOrderService>().As<IOrderService>().InstancePerRequest();

            builder.RegisterType<CustomInventoryService>().As<ICustomInventoryService>().InstancePerRequest();
            builder.RegisterType<CustomInventoryService>().As<BaseService>().InstancePerRequest();
            builder.RegisterType<CustomInventoryController>().As<BaseController>().InstancePerDependency();
            builder.RegisterType<CustomInventoryCache>().As<ICustomInventoryCache>().InstancePerRequest();
            builder.RegisterType<CustomInventoryCache>().As<BaseCache>().InstancePerRequest();

            builder.RegisterType<ShibLoginService>().As<IShibLoginService>().InstancePerRequest();
            builder.RegisterType<ShibLoginService>().As<IUserService>().InstancePerRequest();

            builder.RegisterType<CustomiPayService>().As<ICustomiPayService>().InstancePerRequest();
            builder.RegisterType<CustomiPayService>().As<BaseService>().InstancePerRequest();
            builder.RegisterType<CustomiPayController>().As<BaseController>().InstancePerDependency();
            builder.RegisterType<CustomiPayCache>().As<ICustomiPayCache>().InstancePerRequest();
            builder.RegisterType<CustomiPayCache>().As<BaseCache>().InstancePerRequest();
        }

        /// <summary>
        /// Order method represents Dependency Injection Registration Order.
        /// For znode base code Library the DI registration order set to 0.
        /// For custom code library the DI registration order should be incremental.
        /// </summary>
        public int Order
        {
            get { return 1; }
        }
    }
}
