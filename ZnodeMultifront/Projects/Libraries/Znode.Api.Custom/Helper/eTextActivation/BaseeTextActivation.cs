﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Api.Custom.Helper.eTextActivation
{
    // This is the base class for all ERP Configurator Classes.
    public abstract class BaseeTextActivation
    {
        public virtual bool Activate(string UserID, string ActivationCode) => true;

    }
}
