﻿using System;
using Znode.Api.Custom.edu.illinois.etext;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Helper.eTextActivation.eTextAtIllinois
{
    class eTextAtIllinois : BaseeTextActivation
    {
        public const string ServiceURL = "https://etext.illinois.edu";
        private const string BookSeller = "Illinois Webstore";

        public override bool Activate(string UserID, string ActivationCode)
        {
            activation_wrapService etas = new activation_wrapService();
            bool activationResult = false;
            //string orderNote;

            try
            {
                activationResult = etas.activate(BookSeller, UserID, ActivationCode);
            }
            catch (Exception ex)
            {
                activationResult = false;
                ZnodeLogging.LogMessage("eText@Illinois Activation Exception: " + ex.Message);
            }

            return activationResult;
        }
    }
}
