﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Znode.Api.Custom.Cache;
using Znode.Api.Custom.Service;
using Znode.Engine.Api.Controllers;
using Znode.Engine.Api.Helper;
using Znode.Engine.Api.Model.Custom;
using Znode.Engine.Api.Model.Custom.Responses;
using Znode.Engine.Exceptions;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Controller
{


    public class CustomiPayController : BaseController
    {

        #region Private Variables
        private readonly ICustomiPayCache _customCache;
        private readonly ICustomiPayService _customService;

        #endregion

        #region Constructor
        public CustomiPayController(ICustomiPayService iPayService)
        {
            _customService = iPayService;
            _customCache = new CustomiPayCache(_customService);
        }
        #endregion

        public HttpResponseMessage Index()
        {
            HttpResponseMessage response = CreateOKResponse("Hello iPay World!");
            return response;
        }

        [ResponseType(typeof(CustomiPayRecordResponse))]
        [HttpGet]
        public HttpResponseMessage GetiPayByReferenceID(string ReferenceID)
        {
            HttpResponseMessage response;
            response = CreateNoContentResponse();

            try
            {
                //Get the Custom Portal Details from Cache.
                string data = _customCache.GetiPayByReferenceID(ReferenceID, RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<CustomiPayRecordResponse>(data) : CreateNoContentResponse();
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                CustomiPayRecordResponse ipayResponse = new CustomiPayRecordResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(ipayResponse);
                ZnodeLogging.LogMessage(ex.Message, "Custom iPay Controller: GetByReferenceID");
            }
            return response;
        }

        [ResponseType(typeof(CustomiPayRecordResponse))]
        [HttpGet]
        public HttpResponseMessage GetiPayByToken(string Token)
        {
            HttpResponseMessage response;
            response = CreateNoContentResponse();

            try
            {
                //Get the Custom Portal Details from Cache.
                string data = _customCache.GetiPayByToken(Token, RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<CustomiPayRecordResponse>(data) : CreateNoContentResponse();
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                CustomiPayRecordResponse ipayResponse = new CustomiPayRecordResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(ipayResponse);
                ZnodeLogging.LogMessage(ex.Message, "Custom iPay Controller: GetByToken");
            }
            return response;
        }

        [ResponseType(typeof(CustomiPayRecordResponse))]
        [HttpPost, ValidateModel]
        public HttpResponseMessage CreateiPayRecord([FromBody] iPayRecordModel iPayRecord)
        {
            HttpResponseMessage response;
            try
            {

                //Insert Custom Portal Details.
                iPayRecordModel insertedRecord = _customService.CreateIllinoisiPay(iPayRecord);
                response = HelperUtility.IsNotNull(insertedRecord) ? CreateCreatedResponse(new CustomiPayRecordResponse { iPayRecord = insertedRecord }) : CreateInternalServerErrorResponse();
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                response = CreateInternalServerErrorResponse(new CustomiPayRecordResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
                ZnodeLogging.LogMessage(ex.Message, "Custom iPay Controller: Insert");
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                response = CreateInternalServerErrorResponse(new CustomiPayRecordResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex.Message, "Custom iPay Controller: Insert");
            }
            return response;
        }


        [ResponseType(typeof(CustomiPayRecordResponse))]
        [HttpPut]
        public HttpResponseMessage UpdateiPay([FromBody] iPayRecordModel iPayRecord)
        {
            HttpResponseMessage response;
            try
            {
                //Update the Custom Portal Details.
                bool isUpdated = _customService.UpdateIllinoisiPay(iPayRecord);
                response = isUpdated ? CreateOKResponse(new CustomiPayRecordResponse { iPayRecord = iPayRecord }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                CustomiPayRecordResponse portalResponse = new CustomiPayRecordResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(portalResponse);
                ZnodeLogging.LogMessage(ex.Message, "Custom iPay Controller: Update");
            }
            return response;
        }
    }
}
