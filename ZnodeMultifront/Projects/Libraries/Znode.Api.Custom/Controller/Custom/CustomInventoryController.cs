﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Znode.Api.Custom.Cache;
using Znode.Api.Custom.Service;
using Znode.Engine.Api.Controllers;
using Znode.Engine.Api.Model.Custom.Responses;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Controller
{
    public class CustomInventoryController : BaseController
    {
        #region Private Variables

        private readonly ICustomInventoryService _service;
        private readonly ICustomInventoryCache _cache;

        #endregion

        #region Constructor
        public CustomInventoryController(ICustomInventoryService service)
        {
            _service = service;
            _cache = new CustomInventoryCache(_service);
        }
        #endregion

        /// <summary>
        /// This method will fetch and process the data from uploaded file.
        /// </summary>
        /// <param name="model">Improt Model</param>
        /// <returns>Returns true if successfull else returns false.</returns>
        [ResponseType(typeof(TrueFalseResponse))]
        [HttpPost]
        public virtual HttpResponseMessage ImportData(ImportModel model)
        {
            HttpResponseMessage response;
            response = CreateNoContentResponse();
            try
            {
                int processStarted = _service.ProcessData(model);
                response = processStarted > 0 ? CreateOKResponse(new TrueFalseResponse { IsSuccess = true }) : CreateInternalServerErrorResponse(new TrueFalseResponse { HasError = true, ErrorMessage = "Import process failed.", ErrorCode = ErrorCodes.ProcessingFailed });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Import.ToString(), TraceLevel.Warning);
                response = CreateInternalServerErrorResponse(new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Import.ToString(), TraceLevel.Error);
                response = CreateInternalServerErrorResponse(new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message });
            }
            return response;
        }


        #region Digital Asset

        /// <summary>
        /// Get downloadable product key list.
        /// </summary>
        /// <returns>Keys list</returns>
        [ResponseType(typeof(CustomDownloadableProductKeyListResponse))]
        [HttpGet]
        public HttpResponseMessage GetDownloadableProductKeyList()
        {
            HttpResponseMessage response;
            try
            {
                string data = _cache.GetDownloadableProductKeyList(RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<CustomDownloadableProductKeyListResponse>(data) : CreateNoContentResponse();
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                CustomDownloadableProductKeyListResponse data = new CustomDownloadableProductKeyListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }
        #endregion
    }
}
