﻿using Znode.Engine.Api.Model.Custom;


namespace Znode.Api.Custom.Service
{
    public interface ICustomiPayService
    {
        iPayRecordModel GetIllinoisiPay(string ReferenceId);
        iPayRecordModel CreateIllinoisiPay(iPayRecordModel model);
        bool UpdateIllinoisiPay(iPayRecordModel model);
        iPayRecordModel GetIllinoisiPayByToken(string Token);
    }

}
