﻿using System.Collections.Specialized;
using Znode.Engine.Api.Model.Custom;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Custom.Service
{
    public interface ICustomInventoryService
    {
        /// <summary>
        /// Process the data from the file.  
        /// This method will fetch the data from file and insert it into DB and then
        /// inserted data will be processed.
        /// </summary>
        /// <param name="importModel">ImportModel</param>
        /// <returns>ImportModel</returns>
        int ProcessData(ImportModel importModel);

        #region Digital Asset
        /// <summary>
        /// Get downloadable product key list from database.
        /// </summary>
        /// <param name="expands">Expands collection.</param>
        /// <param name="filters">Filters collection.</param>
        /// <param name="sorts">Sort collection.</param>
        /// <param name="page">Page Number.</param>
        /// <returns>Returns DownloadableProductKeyListModel</returns>
        CustomDownloadableProductKeyListModel CustomGetDownloadableProductKeyList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page);

        #endregion

    }
}
