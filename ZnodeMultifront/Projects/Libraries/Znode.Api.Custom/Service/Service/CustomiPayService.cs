﻿using System;
using Znode.Api.Custom.Helper;
using Znode.Engine.Api.Model.Custom;
using Znode.Engine.Services;

namespace Znode.Api.Custom.Service
{
    public class CustomiPayService : BaseService, ICustomiPayService
    {
        private readonly iPayHelper _iPayHelp;

        public CustomiPayService()
        {
            _iPayHelp = new iPayHelper();
        }

        public iPayRecordModel GetIllinoisiPay(string ReferenceId)
        {
            var iPay = _iPayHelp.GetByReferenceId(ReferenceId);
            return iPay;
        }

        public iPayRecordModel CreateIllinoisiPay(iPayRecordModel model)
        {
            if (model == null)
            {
                throw new Exception("iPayRecord model cannot be null.");
            }

            var iPayInsertSuccess = _iPayHelp.Insert(model);

            if (iPayInsertSuccess)
            {
                var iPay = _iPayHelp.GetByReferenceId(model.ReferenceId);
                return iPay;
            }

            return null;
        }

        public bool UpdateIllinoisiPay(iPayRecordModel model)
        {
            if (string.IsNullOrEmpty(model.ReferenceId))
            {
                throw new Exception("iPay ReferenceId is empty");
            }

            if (model == null)
            {
                throw new Exception("iPayRecord model cannot be null");
            }

            var iPay = _iPayHelp.GetByReferenceId(model.ReferenceId);
            if (iPay != null)
            {

                var updated = _iPayHelp.Update(model);
                //if (updated)
                //{
                //    iPay = _iPayHelp.GetByReferenceId(model.ReferenceId);
                //    return iPay;
                //}
                return updated;
            }

            //return null;
            return false;
        }

        public iPayRecordModel GetIllinoisiPayByToken(string Token)
        {
            var iPay = _iPayHelp.GetByToken(Token);
            return iPay;
        }
    }
}
