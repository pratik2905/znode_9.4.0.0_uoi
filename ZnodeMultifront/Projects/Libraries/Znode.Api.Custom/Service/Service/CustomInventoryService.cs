﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Znode.Api.Custom.Helper;
using Znode.Engine.Api.Model.Custom;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Service
{
    public class CustomInventoryService: BaseService,ICustomInventoryService
    {
        #region Private Variables
       
        private InventoryHelper inventoryHelper;
        #endregion

        public CustomInventoryService()
        {
            inventoryHelper = new InventoryHelper();
        }

        // This method will fetch the data from file and insert it into DB and then inserted data will be processed.
        public virtual int ProcessData(ImportModel importModel)
        {
            try
            {
                int userId = GetLoginUserId();
                return inventoryHelper.ProcessData(importModel, userId);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Failed to Process Import Data", ZnodeLogging.Components.Import.ToString(), TraceLevel.Error, ex);
                throw new ZnodeException(ErrorCodes.ImportError, ex.Message);
            }
        }

        #region Digital Asset
        //Get list of downloadable product keys
        public virtual CustomDownloadableProductKeyListModel CustomGetDownloadableProductKeyList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            //Bind the Filter, sorts & Paging details.
            PageListModel pageListModel = new PageListModel(filters, sorts, page);

            IZnodeViewRepository<CustomDownloadableProductKeyModel> objStoredProc = new ZnodeViewRepository<CustomDownloadableProductKeyModel>();
            //SP parameters
            objStoredProc.SetParameter("@WhereClause", pageListModel.SPWhereClause, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Order_BY", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@RowsCount", pageListModel.TotalRowCount, ParameterDirection.Output, DbType.Int32);

            IList<CustomDownloadableProductKeyModel> productKeyList = objStoredProc.ExecuteStoredProcedureList("Znode_GetPimDownloadableProductKeyList @WhereClause,@Rows,@PageNo,@Order_BY,@RowsCount OUT", 4, out pageListModel.TotalRowCount);

            CustomDownloadableProductKeyListModel listModel = new CustomDownloadableProductKeyListModel { CustomDownloadableProductKeys = productKeyList?.ToList() };
            listModel.BindPageListModel(pageListModel);

            return listModel;
        }
        #endregion
    }
}
