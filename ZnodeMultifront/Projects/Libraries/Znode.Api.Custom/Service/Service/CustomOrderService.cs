﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Znode.Api.Custom.Helper;
using Znode.Api.Custom.Helper.eTextActivation;
using Znode.Api.Custom.Helper.eTextActivation.eTextAtIllinois;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Fulfillment;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Api.Custom.Service
{


    public class CustomOrderService : OrderService, ICustomOrderService
    {
        //CUSTOM CODE START -- UOFI-38 
        private readonly IZnodeRepository<ZnodeAddress> _addressRepository;
        private readonly IZnodeRepository<ZnodePimDownloadableProduct> _pimDownloadableProduct;
        public CustomOrderService()
        {
            _addressRepository = new ZnodeRepository<ZnodeAddress>();
            _pimDownloadableProduct = new ZnodeRepository<ZnodePimDownloadableProduct>();
        }
        //CUSTOM CODE END -- UOFI-38 

        //public virtual string GetDownloadableProductOrderReceipt(ZnodeOrderFulfillment order, IZnodeCheckout checkout, string feedbackUrl, int localeId, DownloadableProductKeyListModel key, bool isUpdate, out bool isEnableBcc);
        public override string GetDownloadableProductOrderReceipt(ZnodeOrderFulfillment order, IZnodeCheckout checkout, string feedbackUrl, int localeId, DownloadableProductKeyListModel key, bool isUpdate , out bool isEnableBcc)
        {
            //Inserted into process here:  Activate eTexts
            try
            {
                ProcessActivations(key, checkout, order);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message.ToString(), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);//jjhvhgchv
            }

            //Commented out following 6 lines to prevent 2nd key email from being sent
            CustomOrderReceipt receipt = new CustomOrderReceipt(order, checkout.ShoppingCart) { FromApi = true, ApiShoppingCart = checkout.ShoppingCart, FeedbackUrl = feedbackUrl };

            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("ProductKeyOrderReceipt", (order.PortalId > 0) ? order.PortalId : PortalId, localeId);
            isEnableBcc = emailTemplateMapperModel.IsEnableBcc;
            if (IsNotNull(emailTemplateMapperModel))
                return EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetProductKeysOrderReceiptHtml(emailTemplateMapperModel.Descriptions, key));

            return string.Empty;
        }

        //to generate order receipt

        private void ProcessActivations(DownloadableProductKeyListModel keys, IZnodeCheckout checkout, ZnodeOrderFulfillment orderfulfill)
        {
            string orderNote = string.Empty;

            this.GetUserDetails(orderfulfill.UserID, orderfulfill.Order);
            foreach (var asset in keys.DownloadableProductKeys)
            {
                //ZNodeLogging.LogMessage(string.Format("DownloadURL: {0} Key: {1} LineItemID: {2} UserID: {3} NetID(Order): {4}",
                //    asset.DownloadableProductURL, asset.DownloadableProductKey, asset.OmsOrderLineItemsId, orderfulfill.UserID, orderfulfill.Order.UserName));
                BaseeTextActivation activator = null;
                switch (asset.DownloadableProductURL)
                {
                    case eTextAtIllinois.ServiceURL:
                        activator = new eTextAtIllinois();
                        break;
                    // other types of etext services will be added as additonal cases for activation
                    default:
                        break;
                }

                if (activator != null)
                {
                    if (activator.Activate(orderfulfill.Order.UserName, asset.DownloadableProductKey))
                    {//mark success

                        orderNote += string.Format("Successful auto-activation for {0}: {1}{2}", asset.ProductName, asset.DownloadableProductKey, Environment.NewLine);
                    }
                    else
                    {//mark failure
                        orderNote += string.Format("Auto-activation attempted and failed for {0}: {1}{2}", asset.ProductName, asset.DownloadableProductKey, Environment.NewLine);
                    }
                }

                if (!string.IsNullOrEmpty(orderNote)) this.AddOrderNote(new OrderNotesModel { OmsOrderDetailsId = orderfulfill.Order.OmsOrderDetailsId, UserName = "Activation", Notes = orderNote });
            }
        }

        //public override string GetDownloadableProductOrderReceipt(ZNodeOrderFulfillment order, ZNodeCheckout checkout, string feedbackUrl, int localeId, DownloadableProductKeyListModel key, bool isUpdate = false)
        //{
        //    CustomOrderReceipt receipt = new CustomOrderReceipt(order, checkout.ShoppingCart) { FromApi = true, ApiShoppingCart = checkout.ShoppingCart, FeedbackUrl = feedbackUrl };

        //    //Method to get Email Template.
        //    EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("ProductKeyOrderReceipt", (order.PortalId > 0) ? order.PortalId : PortalId, localeId);
        //    if (IsNotNull(emailTemplateMapperModel))
        //        return EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetProductKeysOrderReceiptHtml(emailTemplateMapperModel.Descriptions, key));

        //    return string.Empty;
        //}


        public override string GetOrderReceipt(ZnodeOrderFulfillment order, IZnodeCheckout checkout, string feedbackUrl, int localeId, bool isUpdate, out bool isEnableBcc, int accountId = 0)
        {
            CustomOrderReceipt receipt = new CustomOrderReceipt(order, checkout.ShoppingCart) { FromApi = true, ApiShoppingCart = checkout.ShoppingCart, FeedbackUrl = feedbackUrl };

            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("OrderReceipt", (order.PortalId > 0) ? order.PortalId : PortalId, localeId);
            isEnableBcc = emailTemplateMapperModel.IsEnableBcc;
            if (IsNotNull(emailTemplateMapperModel))
                return EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetOrderReceiptHtml(emailTemplateMapperModel.Descriptions));

            return string.Empty;
        }


        //CUSTOM CODE START -- UOFI-38 
        public override string GetOrderShipmentAddress(OrderShipmentModel orderShipment)
        {
            if (IsNotNull(orderShipment))
            {
                string ShippingcompanyName = _addressRepository.Table.Where(x => x.AddressId == orderShipment.AddressId)?.FirstOrDefault()?.CompanyName;

                string street1 = string.IsNullOrEmpty(orderShipment.ShipToStreet2) ? string.Empty : "<br />" + orderShipment.ShipToStreet2;
                orderShipment.ShipToCompanyName = IsNotNull(orderShipment?.ShipToCompanyName) ? $"{orderShipment?.ShipToCompanyName}{"<br />"}" : ShippingcompanyName;
                return $"{orderShipment?.ShipToFirstName}{" "}{ orderShipment?.ShipToLastName}{"<br />"}{ orderShipment.ShipToCompanyName}{"<br />"}{orderShipment.ShipToStreet1}{street1}{"<br />"}{ orderShipment.ShipToCity}{","}{"&nbsp;"}{orderShipment.ShipToStateCode}{","}{"&nbsp;"}{orderShipment.ShipToCountry}{"&nbsp;"}{orderShipment.ShipToPostalCode}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderShipment.ShipToPhoneNumber}{"<br />"}{WebStore_Resources.TitleEmail}{" : "}{orderShipment.ShipToEmailId}";
            }
            return string.Empty;
        }

        public override string GetOrderBillingAddress(OrderModel orderBilling)
        {
            if (IsNotNull(orderBilling))
            {
                string street1 = string.IsNullOrEmpty(orderBilling.BillingAddress.Address2) ? string.Empty : "<br />" + orderBilling.BillingAddress.Address2;
                return $"{orderBilling?.BillingAddress.FirstName}{" "}{orderBilling?.BillingAddress.LastName}{"<br />"}{orderBilling?.BillingAddress.CompanyName}{"<br />"}{orderBilling.BillingAddress.Address1}{street1}{"<br />"}{ orderBilling.BillingAddress.CityName}{"<br />"}{(string.IsNullOrEmpty(orderBilling.BillingAddress.StateCode) ? orderBilling.BillingAddress.StateName : orderBilling.BillingAddress.StateCode)}{"<br />"}{orderBilling.BillingAddress.PostalCode}{"<br />"}{orderBilling.BillingAddress.CountryName}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderBilling.BillingAddress.PhoneNumber}";
            }
            return string.Empty;
        }
        //CUSTOM CODE END -- UOFI-38 

        //Map ShoppingCart related data.
        public override void MapShoppingCartData(OrderModel orderModel)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            orderModel.ShoppingCartModel = GetShoppingCartByOrderId(orderModel.OmsOrderId, orderModel.PortalId, orderModel.UserId, orderModel.PortalCatalogId);

            if (IsNotNull(orderModel.ShoppingCartModel))
            {
                orderModel.ShoppingCartModel.ShippingId = orderModel.ShippingId;
                orderModel.ShoppingCartModel.ShippingAddress = orderModel.ShippingAddress;
                orderModel.ShoppingCartModel.BillingAddress = orderModel.BillingAddress;
                orderModel.ShoppingCartModel.Shipping.ShippingId = orderModel.ShippingId;
                orderModel.ShoppingCartModel.Shipping.ShippingCountryCode = orderModel.ShippingAddress?.CountryName;
                orderModel.PortalCatalogId = orderModel.ShoppingCartModel.PublishedCatalogId;
                if (orderModel.IsTaxCostEdited)
                    orderModel.ShoppingCartModel.CustomTaxCost = orderModel.TaxCost;
                if (orderModel.IsShippingCostEdited)
                    orderModel.ShoppingCartModel.CustomShippingCost = orderModel.ShippingCost;
            }

            List<string> downloadableProductkeys = GetDownloadableProductKeyList(orderModel.ShoppingCartModel.ShoppingCartItems?.Select(x => x.SKU)?.Distinct()?.ToList());
            if (downloadableProductkeys?.Count > 0)
            {
                foreach (ShoppingCartItemModel lineItem in orderModel.ShoppingCartModel.ShoppingCartItems)
                {
                    bool IsDownloadableSKU = downloadableProductkeys.Any(x => x == lineItem.SKU);
                    if (IsDownloadableSKU)
                    {
                        int? parentOmsOrderLineItemsId = orderModel?.OrderLineItems?.FirstOrDefault(x => x.OmsOrderLineItemsId == lineItem.OmsOrderLineItemsId)?.ParentOmsOrderLineItemsId;
                        List<OrderLineItemModel> downloadableItemlist = orderModel.OrderLineItems.Where(x => x.OmsOrderLineItemsId == parentOmsOrderLineItemsId)?.ToList();
                        if (downloadableItemlist?.Count > 0)
                            foreach (OrderLineItemModel item in downloadableItemlist)
                                lineItem.DownloadableProductKey = GetProductKey(item.Sku, item.OmsOrderLineItemsId, downloadableProductkeys);
                    }
                }
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }
        private List<string> GetDownloadableProductKeyList(List<string> lineItemSKU)
        {
            List<string> orderProductKeySKUList = null;
            if (lineItemSKU?.Count > 0)
                orderProductKeySKUList = _pimDownloadableProduct.Table.Where(x => lineItemSKU.Contains(x.SKU)).Select(x => x.SKU)?.ToList();

            return orderProductKeySKUList;
        }
    }
}