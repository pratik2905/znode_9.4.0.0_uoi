﻿using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Service
{
    class ShibLoginService : UserService, IShibLoginService
    {
        private const int CREATED_BY_SHIBBOLETH = -213;

        public ShibLoginService():base()
        { }
        
        public override UserModel Login(int portalId, UserModel model, out int? errorCode, NameValueCollection expand=null )
        {
            UserModel returnUser;

            //ZnodeLogging.LogMessage("Entered Custom ShibLoginService");

            //If user doesn't exist and it is a shib login, then create the user.  Then perform normal login
            if ((!LoginHelper.IsUserExists(model.UserName, portalId)) && model.CreatedBy == CREATED_BY_SHIBBOLETH)
            {
                //ZnodeLogging.LogMessage(string.Format("User {0} Doesn't exist, attempting to create", model.UserName));
                //Create the user
                model.CreatedBy = 0; //reset to default for creation
                //model.Email = model.UserName;
                //model.Addresses = new List<AddressModel>();
                //model.Addresses.Add(GenerateDefaultAddress(model.Email));
                model.IsWebStoreUser = true;
                
                base.CreateCustomer(portalId, model);
            }

            //login the user
            ZnodeLogging.LogMessage("Running base login (user exists)");
            returnUser = base.Login(portalId, model, out errorCode, expand);
            //Update registered profiles?
            //Reload usermodel profiles?

            


            return returnUser;
            
        }

        private AddressModel GenerateDefaultAddress(string emailAddress)
        {
            AddressModel returnAddress = new AddressModel();

            returnAddress.CityName = "Urbana";
            returnAddress.CountryName = "US";
            returnAddress.EmailAddress = emailAddress;
            returnAddress.FirstName = "Webstore";
            returnAddress.IsDefaultBilling = true;
            returnAddress.IsDefaultShipping = true;
            returnAddress.LastName = "Offices";
            returnAddress.DisplayName = "Generated Default Address";
            returnAddress.PostalCode = "61801";
            returnAddress.StateCode = "IL";
            returnAddress.Address1 = "MC 256";
            returnAddress.IsActive = true;
            return returnAddress;
        }
    }
}
