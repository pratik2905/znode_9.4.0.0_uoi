﻿using Znode.Api.Custom.Service;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Model.Custom;
using Znode.Engine.Api.Model.Custom.Responses;
using Znode.Engine.Api.Models.Extensions;


namespace Znode.Api.Custom.Cache
{
    public class CustomInventoryCache : BaseCache, ICustomInventoryCache
    {
        #region Private Variable
        private readonly ICustomInventoryService _customService;
        #endregion

        #region Constructor
        public CustomInventoryCache(ICustomInventoryService customInventoryService)
        {
            _customService = customInventoryService;
        }
        #endregion

        #region Digital asset
        //Get downloadable product key list From Cache.
        public virtual string GetDownloadableProductKeyList(string routeUri, string routeTemplate)
        {
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {
                CustomDownloadableProductKeyListModel list = _customService.CustomGetDownloadableProductKeyList(Expands, Filters, Sorts, Page);
                if (list?.CustomDownloadableProductKeys?.Count > 0)
                {
                    CustomDownloadableProductKeyListResponse response = new CustomDownloadableProductKeyListResponse { CustomDownloadableProductKeys = list.CustomDownloadableProductKeys };
                    response.MapPagingDataFromModel(list);
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
        #endregion
    }
}
