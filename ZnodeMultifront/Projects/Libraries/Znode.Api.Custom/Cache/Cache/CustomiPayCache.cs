﻿using Znode.Api.Custom.Service;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Model.Custom;
using Znode.Engine.Api.Model.Custom.Responses;

namespace Znode.Api.Custom.Cache
{
    public class CustomiPayCache : BaseCache, ICustomiPayCache
    {
        #region Private Variables
        private readonly ICustomiPayService _iPayService;
        #endregion

        #region Constructor
        public CustomiPayCache(ICustomiPayService iPayService)
        {
            _iPayService = iPayService;
        }
        #endregion

        #region Public Methods
       
        public string GetiPayByReferenceID(string ReferenceID, string routeUri, string routeTemplate)
        {
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {
                //Get data from custom Portal Detail service.
                iPayRecordModel iPayModel = _iPayService.GetIllinoisiPay(ReferenceID);
                if (!Equals(iPayModel, null))
                {
                    //Create Response and insert in to cache
                    CustomiPayRecordResponse response = new CustomiPayRecordResponse { iPayRecord = iPayModel };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetiPayByToken(string Token, string routeUri, string routeTemplate)
        {
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {
                //Get data from custom Portal Detail service.
                iPayRecordModel iPayModel = _iPayService.GetIllinoisiPayByToken(Token);
                if (!Equals(iPayModel, null))
                {
                    //Create Response and insert in to cache
                    CustomiPayRecordResponse response = new CustomiPayRecordResponse { iPayRecord = iPayModel };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
        #endregion
    }
}
