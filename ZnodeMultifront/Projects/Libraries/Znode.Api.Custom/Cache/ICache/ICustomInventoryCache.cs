﻿namespace Znode.Api.Custom.Cache
{
    public interface ICustomInventoryCache
    {
        #region Digital Asset
        /// <summary>
        /// Get downloadable product list.
        /// </summary>
        /// <param name="routeUri">route uri.</param>
        /// <param name="routeTemplate">route template.</param>
        /// <returns>Returns list of downloadable product list.</returns>
        string GetDownloadableProductKeyList(string routeUri, string routeTemplate);
        #endregion
    }
}
