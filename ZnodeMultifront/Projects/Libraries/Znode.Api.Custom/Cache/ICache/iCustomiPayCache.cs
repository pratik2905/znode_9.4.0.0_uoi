﻿namespace Znode.Api.Custom.Cache
{
    public interface ICustomiPayCache
    {
        
        /// <summary>
        /// Get the iPay Record by Reference Id.
        /// </summary>
        /// <param name="ReferenceID">ReferenceId of iPay Record</param>
        /// <param name="routeUri">URI to route.</param>
        /// <param name="routeTemplate">Template of route.</param>
        /// <returns>iPay record Model in string format by serializing it.</returns>
        string GetiPayByReferenceID(string ReferenceID, string routeUri, string routeTemplate);

        /// <summary>
        /// Get the iPay Record by associated Token
        /// </summary>
        /// <param name="Token">Token associated with iPay Record</param>
        /// <param name="routeUri">URI to route.</param>
        /// <param name="routeTemplate">Template of route.</param>
        /// <returns>iPay record Model in string format by serializing it.</returns>
        string GetiPayByToken(string Token, string routeUri, string routeTemplate);
    }
}
