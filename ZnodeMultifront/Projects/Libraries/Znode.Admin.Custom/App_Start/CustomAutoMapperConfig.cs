﻿using AutoMapper;
using Znode.Admin.Custom.ViewModels;
using Znode.Engine.Admin.ViewModel;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Model.Custom;
namespace Znode.Admin.Custom
{
    public static class CustomAutoMapperConfig
    {
        public static void Execute()
        {
            Mapper.CreateMap<CustomPortalDetailViewModel, CustomPortalDetailModel>().ReverseMap();
            Mapper.CreateMap<StoreFeatureViewModel, PortalFeatureModel>().ReverseMap();
            Mapper.CreateMap<CustomDownloadableProductKeyViewModel, CustomDownloadableProductKeyModel>().ReverseMap();
        }
    }
}
