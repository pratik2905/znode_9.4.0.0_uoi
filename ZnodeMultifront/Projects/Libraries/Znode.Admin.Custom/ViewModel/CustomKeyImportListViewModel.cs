﻿using System.Collections.Generic;
using Znode.Admin.Custom.ViewModel;
using Znode.Engine.Admin.ViewModels;

namespace Znode.Admin.Custom.ViewModels
{
    public class CustomKeyImportListViewModel: BaseViewModel
    {
        public string SKU { get; set; }
        public List<CustomKeyImportViewModel> KeyImportList;
    }
}
