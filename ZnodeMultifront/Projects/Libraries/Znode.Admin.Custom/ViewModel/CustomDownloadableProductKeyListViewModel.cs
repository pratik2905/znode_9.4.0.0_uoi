﻿using System.Collections.Generic;
using Znode.Engine.Admin.ViewModel;
using Znode.Engine.Admin.ViewModels;

namespace Znode.Admin.Custom.ViewModel
{
    public class CustomDownloadableProductKeyListViewModel : DownloadableProductKeyListViewModel
    {
        public List<CustomDownloadableProductKeyViewModel> CustomDownloadableProductKeys { get; set; }
        public string Username { get; set; }
    }
}
