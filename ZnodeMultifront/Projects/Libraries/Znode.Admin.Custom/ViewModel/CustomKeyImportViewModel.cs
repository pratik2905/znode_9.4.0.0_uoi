﻿using Znode.Engine.Admin.ViewModels;

namespace Znode.Admin.Custom.ViewModel
{
    public class CustomKeyImportViewModel: BaseViewModel
    {
        public string Key { get; set; }
        public string Url { get; set; }
        public string FileName { get; set; }
    }
}
