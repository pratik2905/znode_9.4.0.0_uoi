﻿using System.ComponentModel.DataAnnotations;
using System.Web;
using Znode.Engine.Admin.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Resources;

namespace Znode.Engine.Admin.ViewModel
{
    public class CustomDownloadableProductKeyViewModel : DownloadableProductKeyViewModel
    {
        [Display(Name = "LabelFileName", ResourceType = typeof(Admin_Resources))]
        [FileTypeValidation("csv", ErrorMessageResourceType = typeof(Admin_Resources), ErrorMessageResourceName = "CSVFileTypeErrorMessage")]
        [UIHint("FileUploader")]
        public HttpPostedFileBase FilePath { get; set; }

        public string Username { get; set; }
    }
}