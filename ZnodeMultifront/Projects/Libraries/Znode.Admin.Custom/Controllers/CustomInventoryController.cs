﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Znode.Admin.Custom.Agents;
using Znode.Admin.Custom.Helper;
using Znode.Admin.Custom.ViewModel;
using Znode.Engine.Admin.Agents;
using Znode.Engine.Admin.Controllers;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.ViewModel;
using Znode.Engine.Admin.ViewModels;
using Znode.Libraries.Resources;


namespace Znode.Admin.Custom.Controllers
{
    public class CustomInventoryController : InventoryController
    {
        #region Private Variables       
        //private IInventoryAgent _inventoryAgent;
        private readonly ICustomInventoryAgent _customInventoryAgent;
        #endregion


        #region Public Constructor
       
        public CustomInventoryController(IInventoryAgent inventoryAgent, IProductAgent productAgent, IImportAgent importAgent, ICustomInventoryAgent customInventoryAgent) : base(inventoryAgent, productAgent, importAgent)
        {
            _customInventoryAgent = customInventoryAgent;
        }
        #endregion

        #region Public Methods
        //Get list of all downloadable product keys.
        public override ActionResult GetDownloadableProductKeys([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model, int productId, string sku, int inventoryId = 0)
        {
            //Get the list of all Downloadable Product keys.
            CustomDownloadableProductKeyListViewModel downloadableProductKeys = _customInventoryAgent.CustomGetDownloadableProductKeys(productId, sku, model.Expands, model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            downloadableProductKeys.PimProductId = productId;
            downloadableProductKeys.InventoryId = inventoryId;
            downloadableProductKeys.SKU = sku;
            downloadableProductKeys.IsDownloadable = true;
            //Get the grid model.
            downloadableProductKeys.GridModel = FilterHelpers.GetDynamicGridModel(model, downloadableProductKeys.CustomDownloadableProductKeys, "CustomZnodePimDownloadableProductKeyForInventory", string.Empty, null, true, true, downloadableProductKeys?.GridModel?.FilterColumn?.ToolMenuList);

            //Set the total record count
            downloadableProductKeys.GridModel.TotalRecordCount = downloadableProductKeys.TotalResults;

            return ActionView("~/Views/CustomViews/CustomInventory/DownloadableProductKeyList.cshtml", downloadableProductKeys);
        }

        [HttpPost]
        public virtual JsonResult UploadDownloadableProductKeys(CustomDownloadableProductKeyViewModel model)
        {
            string message = string.Empty;
            CustomDownloadableProductKeyViewModel downloadableProductKeyViewModel = _customInventoryAgent.UpdateDownloadableProductKeys(model, out message);
            if (downloadableProductKeyViewModel.DownloadableProductKeyList.Any(x => x.IsDuplicate == true) || downloadableProductKeyViewModel.HasError)
                return Json(new { status = false, list = downloadableProductKeyViewModel.DownloadableProductKeyList, hasError = downloadableProductKeyViewModel.HasError, message = message }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { status = true, list = downloadableProductKeyViewModel.DownloadableProductKeyList, message = Admin_Resources.SaveMessage }, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult UploadDownloadableProductKeys(int productId, string sku)
      => PartialView("~/Views/CustomViews/CustomInventory/UploadDownloadableProductKeys.cshtml", new CustomDownloadableProductKeyViewModel() { ProductId = productId, SKU = sku });
        [HttpPost]
        public virtual ActionResult UploadKeys(ImportViewModel importModel)
        {
            string message = importModel.IsPartialPage ? Admin_Resources.LinkViewImportLogs : "";
            if (_customInventoryAgent.ImportData(importModel))
                SetNotificationMessage(GetSuccessNotificationMessage(Admin_Resources.ImportProcessInitiated + message));
            else
                SetNotificationMessage(GetErrorNotificationMessage(Admin_Resources.ImportProcessFailed + message));

            //Assign the values back to model
          //importModel = _customInventoryAgent.BindViewModel();
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }
       
        #endregion
    }
}
