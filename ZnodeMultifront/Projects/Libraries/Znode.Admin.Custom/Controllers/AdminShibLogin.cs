﻿using System;
using System.Web.Mvc;
using Znode.Engine.Admin.Agents;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.ViewModels;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Api.Client;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.Admin.Controllers
{
    class AdminShibUserController : UserController
    {
        #region Private Read-only members
        private readonly AuthenticationHelper _authenticationHelper;
        private readonly IUserAgent _userAgent;
        private readonly IRoleAgent _roleAgent;
        private const int CREATED_BY_SHIBBOLETH = -213;
        #endregion

        #region Public Constructor     
        public AdminShibUserController(IUserAgent userAgent, IRoleAgent roleAgent, AuthenticationHelper authenticationHelper)
       : base(userAgent, roleAgent, authenticationHelper)
        {
            _authenticationHelper = authenticationHelper;
            _userAgent = userAgent;
            _roleAgent = roleAgent;

        }
        #endregion

        [AllowAnonymous]
        [HttpGet]
        public override ActionResult Login(string returnUrl)
        {
            ZnodeLogging.LogMessage("In Custom Login controller");

            if (!string.IsNullOrEmpty(Request.Headers["eppn"]))
            {
                ZnodeLogging.LogMessage("Valid eppn found");
                LoginViewModel shibModel = new LoginViewModel();
                shibModel.Username = IllinoisHelper.NormalizeEPPN(Request.Headers["eppn"]);
                shibModel.Password = IllinoisHelper.PasswordMixup(shibModel.Username);
                //Mark as shib generated
                shibModel.SuccessMessage = "ShibUser";
                shibModel.CreatedBy = CREATED_BY_SHIBBOLETH;
                shibModel.IsResetAdmin = false;
                shibModel.IsResetAdminPassword = false;
                shibModel.IsResetPassword = false;
                shibModel.IsAdminUser = true;

                ZnodeLogging.LogMessage("Shibmodel created, passing to Login routine");
                return Login(shibModel, returnUrl);
            }
            return View("Login", new LoginViewModel());
        }


        [AllowAnonymous]
        [HttpPost]
        public override ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                ZnodeLogging.LogMessage("Valid modelstate, calling useragent login");

                var loginviewModel = _userAgent.Login(model);

                ZnodeLogging.LogMessage("UserAgent Login executed");

                if (!loginviewModel.HasError || (loginviewModel.IsResetPassword && model.SuccessMessage == "ShibUser")) //ignore reset password for Illinois Users, handled inside
                {
                    //reset password as needed
                    if (loginviewModel.IsResetPassword || loginviewModel.IsResetAdminPassword)
                    {

                        ZnodeLogging.LogMessage(new Exception("Reset Illinois password requested"));
                        ResetIllinoisPassword(model.Username);
                        loginviewModel = _userAgent.Login(model);

                    }

                    _authenticationHelper.SetAuthCookie(model.Username, true);

                    
                   
                    if (string.IsNullOrEmpty(returnUrl))
                    {
                        ZnodeLogging.LogMessage(new Exception("No ReturnUrl, redirecting to Dashboard"));
                        return RedirectToAction("Dashboard");
                    }

                    ZnodeLogging.LogMessage("Redirecting from Login Page");
                    _authenticationHelper.RedirectFromLoginPage(model.Username, true);
                    return new EmptyResult();
                }

                ZnodeLogging.LogMessage(new Exception("Problem: Not marked as a Shib user"));

                if (loginviewModel != null && loginviewModel.IsResetPassword)
                {
                    if (!string.IsNullOrEmpty(loginviewModel.Username) && !string.IsNullOrEmpty(loginviewModel.PasswordResetToken))
                    {
                        ZnodeLogging.LogMessage(new Exception("Reset Pasword - valid username and password reset token"));
                        return View("Login", loginviewModel);
                    }
                    ZnodeLogging.LogMessage(new Exception("Reset Pasword - invalid username or password reset token"));
                    return View("ResetPassword", new ChangePasswordViewModel());
                }

                ZnodeLogging.LogMessage(new Exception(string.Format("Problem: loginviewmodel error: {0}, {1} or settings for shib not right: Reset Pass-{2}, PassQuest-{3} ", loginviewModel.HasError, loginviewModel.ErrorMessage, loginviewModel.IsResetPassword, model.SuccessMessage)));
                return View("Login", loginviewModel);
            }

            ZnodeLogging.LogMessage(new Exception("Invalid Login Model State"));
            return View("Login", model);


            
        }


        public override ActionResult Logout()
        {
            _userAgent.Logout();

            //Go to defined route that hands off Shib Logout to the Shib SP
            //return RedirectToRoute("ShibLogout");
            return Redirect("~/Shibboleth.sso/Logout?https://shibboleth.illinois.edu/idp/profile/Logout");

        }

        public override ActionResult CreateUser(UsersViewModel usersViewModel)
        {
            //LoginViewModel shibModel = new LoginViewModel();
            //shibModel.Username = IllinoisHelper.NormalizeEPPN(Request.Headers["eppn"]);
            //shibModel.Password = IllinoisHelper.PasswordMixup(shibModel.Username);

            UsersViewModel newAdminModel;

            usersViewModel.UserName = IllinoisHelper.NormalizeEPPNforAdmin(usersViewModel.UserName);
            usersViewModel.Email = IllinoisHelper.NormalizeEPPN(usersViewModel.Email);
            
            
            if (!_userAgent.CheckUserNameExist(usersViewModel.UserName, usersViewModel.PortalId))
            {
                //Create the user
                newAdminModel = _userAgent.CreateUser(usersViewModel);
                ChangePasswordViewModel changePasswordViewModel = new ChangePasswordViewModel();
                changePasswordViewModel.UserName = newAdminModel.UserName;
                changePasswordViewModel.PasswordToken = newAdminModel.Email;
                _userAgent.ChangePassword(changePasswordViewModel);
            }
            else
            {
                //get the existing user
                GetErrorNotificationMessage("User already exists");
            }
            //either edit user or userlist
            return RedirectToAction("UserList");
        }

        private void SetAuthCookie(string userName, string id)
        {
            //Session.Add(WebStoreConstants.UserAccountKey, new UserViewModel { UserName = userName, UserId = Convert.ToInt32(id), RoleName = "admin" });

            //Set the Authentication Cookie.           
            _authenticationHelper.SetAuthCookie(userName, true);
        }

        private bool ResetIllinoisPassword(string userName)
        {
            string passwordMixup = IllinoisHelper.PasswordMixup(userName);
            ChangePasswordViewModel RIPmodel = new ChangePasswordViewModel();
            RIPmodel.UserName = userName;
            RIPmodel.OldPassword = passwordMixup;
            RIPmodel.NewPassword = passwordMixup + "1";
            RIPmodel.ReTypeNewPassword = RIPmodel.NewPassword;
            RIPmodel = _userAgent.ChangePassword(RIPmodel);

            if (!RIPmodel.HasError)
            {
                RIPmodel = new ChangePasswordViewModel();
                RIPmodel.UserName = userName;
                RIPmodel.OldPassword = passwordMixup + "1";
                RIPmodel.NewPassword = passwordMixup;
                RIPmodel.ReTypeNewPassword = passwordMixup;
                RIPmodel = _userAgent.ChangePassword(RIPmodel);
            }
            if (RIPmodel.HasError)
            {
                //log or display something
                ZnodeLogging.LogMessage(string.Format("Error changine password for {0}: {1}", userName, RIPmodel.ErrorMessage));
            }


            return !RIPmodel.HasError;

        }
    }
}

//namespace Znode.Engine.Admin.Agents
//{
//    class AdminShibUserAgent : UserAgent
//    {
//        #region Private Read-only members
//        private readonly IUserClient _userClient;
//        private readonly IPortalClient _portalClient;
//        private readonly IAccountClient _accountClient;
//        private readonly IRoleClient _roleClient;
//        private readonly IDomainClient _domainClient;
//        #endregion

//        public AdminShibUserAgent(IUserClient userClient, IPortalClient portalClient, IAccountClient accountClient, IRoleClient roleClient, IDomainClient domainClient ) : 
//            base(userClient, portalClient, accountClient, roleClient, domainClient)
//        {
//            _userClient = userClient;
//            _portalClient = portalClient;
//            _accountClient = accountClient;
//            _roleClient = roleClient;
//            _domainClient = domainClient;
//        }

//        public UserViewModel GetByUserName(string userName)
//        {
//            _userClient.g
//        }
//    }
//}
