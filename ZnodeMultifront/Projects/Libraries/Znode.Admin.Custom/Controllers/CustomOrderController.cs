﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Znode.Engine.Admin.Controllers;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.ViewModels;
using Znode.Libraries.Resources;

namespace Znode.Admin.Custom.Controllers
{
    class CustomOrderController : OrderController
    {
        private Engine.Admin.Agents.IOrderAgent _orderAgent;
        private Engine.Admin.Agents.IStoreAgent _storeAgent;
        private Engine.Admin.Agents.IWebSiteAgent _websiteAgent;

        public CustomOrderController(Engine.Admin.Agents.IOrderAgent orderAgent, Engine.Admin.Agents.IUserAgent userAgent, Engine.Admin.Agents.IAccountAgent accountAgent,
            Engine.Admin.Agents.ICustomerAgent customerAgent, Engine.Admin.Agents.IShippingAgent shippingAgent,
            Engine.Admin.Agents.ICartAgent cartAgent, Engine.Admin.Agents.IPaymentAgent paymentAgent, Engine.Admin.Agents.IAccountQuoteAgent quoteAgent,
            Engine.Admin.Agents.IStoreAgent storeAgent, Engine.Admin.Agents.IRMARequestAgent rmaRequestAgent,
            Engine.Admin.Agents.IWebSiteAgent websiteAgent) :
            base(orderAgent, userAgent, shippingAgent, cartAgent,
         quoteAgent, storeAgent, rmaRequestAgent, websiteAgent)
        {
            _orderAgent = orderAgent;
            _storeAgent = storeAgent;
        }

        [HttpPost]
        public virtual JsonResult ProcessIPayPayment(SubmitPaymentViewModel paymentmodel)
        {
            //Write custom logic
            //paymentmodel
            return Json(new
            {
                status = "OK",
                message = "Did it"
            }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public virtual ActionResult SubmitIPayOrder(string token, int paymentOptionId,
        int shippingId, string additionalNotes)
        {
            if (string.IsNullOrEmpty(token))
            {
                SetNotificationMessage(GetErrorNotificationMessage(Admin_Resources.ErrorUnablePlaceOrder));
                return RedirectToAction<OrderController>(x => x.CreateOrder(0, 0));
            }
            SubmitPaymentViewModel model = new SubmitPaymentViewModel();
            model.Token = token;
            model.PaymentSettingId = paymentOptionId;
            model.ShippingOptionId = shippingId;
            model.AdditionalInfo = additionalNotes;
            SubmitOrderViewModel order =
            _orderAgent.ProcessCreditCardPayment(model, true);
            if (!order.HasError)
            {
                CreateOrderViewModel receipt = new CreateOrderViewModel();
                receipt.OrderId = order.OrderId;
                receipt.ReceiptHtml =
                WebUtility.HtmlDecode(order.ReceiptHtml);
                return ActionView(AdminConstants.CheckoutReceipt, receipt);
            }
            if (!string.IsNullOrEmpty(order.ErrorMessage))
                SetNotificationMessage(GetErrorNotificationMessage(order.ErrorMessage));
            return RedirectToAction<OrderController>(x => x.CreateOrder(0, 0));
        }

    }
}
