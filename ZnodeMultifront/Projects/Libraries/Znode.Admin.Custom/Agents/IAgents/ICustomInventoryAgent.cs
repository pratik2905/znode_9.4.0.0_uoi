﻿using Znode.Admin.Custom.ViewModel;
using Znode.Engine.Admin.ViewModel;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Admin.Custom.Agents
{
    public interface ICustomInventoryAgent 
    {
        CustomDownloadableProductKeyViewModel UpdateDownloadableProductKeys(CustomDownloadableProductKeyViewModel downloadableProductKeyViewModel, out string message);

        bool ImportData(ImportViewModel model);

        #region Digital Asset
        /// <summary>
        /// Get all the downloadable product keys.
        /// </summary>
        /// <param name="productId">product id to get the downloadable product keys.</param>
        /// <param name="sku">sku to get the downloadable product keys.</param>
        /// <param name="expands">expands across downloadable product.</param>
        /// <param name="filters">filter list across downloadable product keys.</param>
        /// <param name="sortCollection">sort collection for downloadable product keys.</param>
        /// <param name="pageIndex">pageIndex for downloadable product keys record. </param>
        /// <param name="recordPerPage">paging downloadable product keys record per page.</param>
        /// <returns></returns>
        CustomDownloadableProductKeyListViewModel CustomGetDownloadableProductKeys(int productId, string sku, ExpandCollection expands = null, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);
        #endregion
    }
}
