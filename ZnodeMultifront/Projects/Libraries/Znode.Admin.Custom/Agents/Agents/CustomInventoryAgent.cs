﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using Znode.Admin.Custom.ViewModel;
using Znode.Api.Client.Custom.Clients;
using Znode.Engine.Admin.Agents;
using Znode.Engine.Admin.Extensions;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.Maps;
using Znode.Engine.Admin.Models;
using Znode.Engine.Admin.ViewModel;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Model.Custom;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;


using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Admin.Custom.Agents
{
    public class CustomInventoryAgent : BaseAgent, ICustomInventoryAgent
    {
        #region Private Variables

        private readonly IInventoryClient _inventoryClient;
        private readonly ICustomInventoryClient _customInventoryClient;
        
        #endregion

        #region Constructor

        public CustomInventoryAgent(IInventoryClient inventoryClient, ICustomInventoryClient customInventoryClient)
        {
            _inventoryClient = GetClient<IInventoryClient>(inventoryClient);
            _customInventoryClient= GetClient<ICustomInventoryClient>(customInventoryClient);
        }
        #endregion
        

        /// <summary>
        /// Update Downloadable Product Key
        /// </summary>
        /// <param name="downloadableProductKeyViewModel">DownloadableProductViewModel to update downloadable product keys.</param>
        /// <returns>Returns true if keys updated sucessfully else return false.</returns>
        // Add/Save Downloadable Product Keys..
        public CustomDownloadableProductKeyViewModel UpdateDownloadableProductKeys(CustomDownloadableProductKeyViewModel downloadableProductKeyViewModel, out string message)
        {
            message = Admin_Resources.ErrorAddDownloadableProductKey;
            try
            {
                DownloadableProductKeyModel downloadableProductKeyModel = _inventoryClient.AddDownloadableProductKey(InventoryViewModelMap.ToDownloadableProductKeyListModel(downloadableProductKeyViewModel));
                return !Equals(downloadableProductKeyModel, null) ? downloadableProductKeyModel.ToViewModel<CustomDownloadableProductKeyViewModel>() : new CustomDownloadableProductKeyViewModel();
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                switch (ex.ErrorCode)
                {
                    case ErrorCodes.AlreadyExist:
                        return (CustomDownloadableProductKeyViewModel)GetViewModelWithErrorMessage(downloadableProductKeyViewModel, Admin_Resources.DupliacteDownloadableProductKeyErrorMessage);
                    default:
                        return (CustomDownloadableProductKeyViewModel)GetViewModelWithErrorMessage(downloadableProductKeyViewModel, Admin_Resources.ErrorAddDownloadableProductKey);
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                return (CustomDownloadableProductKeyViewModel)GetViewModelWithErrorMessage(downloadableProductKeyViewModel, Admin_Resources.ErrorAddDownloadableProductKey);
            }
        }

        //This method will upoload the file and process the data.
        public virtual bool ImportData(ImportViewModel importviewModel)
        {
            string fileName = importviewModel?.FileName;
            try
            {
                importviewModel.FileName = fileName;
                bool isSuccess = _customInventoryClient.ImportData(ImportViewModelMap.ToModel(importviewModel));
                RemoveTemporaryFiles(fileName);
                return isSuccess;
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Import.ToString(), TraceLevel.Warning);
                //Remove the file if any error comes in.
                RemoveTemporaryFiles(fileName);
                return false;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Import.ToString(), TraceLevel.Error);
                return false;
            }
        }

        #region Digital Asset
        //Get downloadable product keys.
        public virtual CustomDownloadableProductKeyListViewModel CustomGetDownloadableProductKeys(int productId, string sku, ExpandCollection expands = null, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            //checks if the filter collection null
            if (Equals(filters, null))
                filters = new FilterCollection();

            if (productId > 0)
            {
                if (!filters.Exists(x => x.Item1 == ZnodePimDownloadableProductEnum.SKU.ToString()))
                    filters.Add(new FilterTuple(ZnodePimDownloadableProductEnum.SKU.ToString(), FilterOperators.Contains, sku.ToString()));
            }
            var _customInventoryClient = new CustomInventoryClient();
            CustomDownloadableProductKeyListModel list = _customInventoryClient.CustomGetDownloadableProductKeys(expands, filters, sortCollection, pageIndex, recordPerPage);

            CustomDownloadableProductKeyListViewModel listViewModel = new CustomDownloadableProductKeyListViewModel { CustomDownloadableProductKeys = list?.CustomDownloadableProductKeys?.ToViewModel<CustomDownloadableProductKeyViewModel>().ToList() };
            SetListPagingData(listViewModel, list);

            //Set tool menu for custom field list grid view.
            SetDownloadableProductKeyListToolMenu(listViewModel);
            return list?.CustomDownloadableProductKeys?.Count > 0 ? listViewModel
                : new CustomDownloadableProductKeyListViewModel();
        }
        #endregion

        #region Private Methods
        //This method will delete the temporary files from the server.
        private void RemoveTemporaryFiles(string fileName)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(HttpContext.Current.Server.MapPath(AdminConstants.ImportFolderPath));

            //Delete file from the directory.
            foreach (FileInfo file in directoryInfo.GetFiles())
            {
                if (file.FullName.Equals(fileName))
                {
                    file.Delete();
                    break;
                }
            }
        }
        
        //Set tool menu for downloadable product key list grid view.
        private void SetDownloadableProductKeyListToolMenu(CustomDownloadableProductKeyListViewModel model)
        {
            if (IsNotNull(model))
            {
                model.GridModel = new GridModel();
                model.GridModel.FilterColumn = new FilterColumnListModel();
                model.GridModel.FilterColumn.ToolMenuList = new List<ToolMenuModel>();
                model.GridModel.FilterColumn.ToolMenuList.Add(new ToolMenuModel { DisplayText = Admin_Resources.ButtonDelete, JSFunctionName = "EditableText.prototype.DialogDelete('DownloadableProductKeyDeletePopup')", ControllerName = "Products", ActionName = "DeleteDownloadableProductKeys" });
            }
        }

        #endregion

    }
}
