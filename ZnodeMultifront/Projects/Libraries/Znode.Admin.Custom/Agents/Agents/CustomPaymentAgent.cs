﻿using Znode.Engine.Admin.Agents;

namespace Znode.Admin.Custom.Agents
{
    public class CustomPaymentAgent : PaymentAgent
    {
        public const string iPayGateway = "10";
        public const string CFOPALGateway = "11";

        public const string iPayPaymentType = "iPay";

        public const string iPayGatewayView = "~/Views/Payment/_iPay.cshtml";
        public const string CFOPALGatewayView = "~/Views/Payment/_CFOPAL.cshtml";
        public const string stdCreditCardView = "~/Views/Payment/_CreditCartDetails.cshtml";

        public CustomPaymentAgent(Engine.Api.Client.IPaymentClient paymentClient, Engine.Api.Client.IProfileClient profileClient) : 
            base(paymentClient, profileClient)
        {
            //No custom constructor
        }
                               
        public override string GetPaymentGatewayView(string paymentGatewayId)
        {
            switch (paymentGatewayId)
            {
                case iPayGateway:
                    return iPayGatewayView;
                case CFOPALGateway:
                    return CFOPALGatewayView;
                default:
                    return base.GetPaymentGatewayView(paymentGatewayId);

            }

            
        }

        public override string GetPaymentTypeView(string paymentTypeId)
        {
            switch (paymentTypeId)
            {
                case iPayPaymentType:
                    return iPayGatewayView;
                default:
                    return base.GetPaymentTypeView(paymentTypeId);
            }
        }
    }
}
