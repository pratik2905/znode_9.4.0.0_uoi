﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;
using Znode.Engine.WebStore.ViewModels;

namespace Illinois.Libraries.iPay
{
    //public class iPayGateway : ZNodeBusinessBase
    public class iPayGateway
    {

        private const string iPayRegisterUrlKey = "iPayRegisterAddress";
        private const string iPayResultUrlKey = "iPayResultAddress";
        private const string iPayCaptureUrlKey = "iPayCaptureAddress";

        private const string iPayTestRegisterUrlKey = "iPayTestRegisterAddress";
        private const string iPayTestResultUrlKey = "iPayTestResultAddress";
        private const string iPayTestCaptureUrlKey = "iPayTestCaptureAddress";

        #region Private Member Variables

        private PaymentSettingViewModel _paymentSetting = new PaymentSettingViewModel();

        private string _sendKey;
        private string _receiveKey;
        private string _siteID;
        private bool _isTestMode;
        private string _bannerAccount;

        #endregion

        #region Protected vars and accessors

        public string BannerAccount
        {
            get { return _bannerAccount; }
        }
        public bool IsTestMode
        {
            get { return _isTestMode; }
        }
        protected string _referenceID;
        public string ReferenceId
        {
            get { return _referenceID; }

        }
        protected int? _RegistrationResponseCode;
        public int? RegistrationResponseCode
        {
            get { return _RegistrationResponseCode; }
        }

        protected int? _ResultResponseCode;
        public int? ResultResponseCode
        {
            get { return _ResultResponseCode; }
        }

        protected int? _CaptureResponseCode;
        public int? CaptureResponseCode
        {
            get { return _CaptureResponseCode; }
        }

        protected string _token;
        public string Token
        {
            get { return _token; }
        }

        protected string _redirectURL;
        public string RedirectUrl
        {
            get { return _redirectURL; }
        }

        protected decimal? _Amount;
        public decimal? Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        protected string _TransactionId;
        public string TransactionId
        {
            get { return _TransactionId; }
        }

        protected DateTime _TransactionStartTime;
        public DateTime TransactionStartTime
        {
            get { return _TransactionStartTime; }
        }

        #endregion

        public iPayGateway()
        {
            //Default constructor
            //ZNodeLogging.LogMessage("Default iPay constructor");
        }
        public iPayGateway(PaymentSettingViewModel PaymentSettingVM, SubmitOrderViewModel SubmitOrderPayment)
        {
            //ZNodeLogging.LogMessage("In iPay Gateway");

            this._paymentSetting = PaymentSettingVM;


            _sendKey = PaymentSettingVM.GatewayUsername;
            _receiveKey = PaymentSettingVM.GatewayPassword;
            _siteID = PaymentSettingVM.TransactionKey;
            _isTestMode = bool.Parse(PaymentSettingVM.ViewParamField);

            //ZNodeLogging.LogMessage("Assigned values from PaymentSettingVM");

            _bannerAccount = PaymentSettingVM.Title.Replace("-", string.Empty); //find a place to store, was vendor.  Custom paymentagent put it in Title

            //ZNodeLogging.LogMessage("parsed banner account");
            _Amount = SubmitOrderPayment.Total;
            if (!string.IsNullOrEmpty(SubmitOrderPayment.PaymentToken))
            {
                _token = SubmitOrderPayment.PaymentToken;
            }
            else
            {
                //no token, so this starts a new transaction and need a referenceid
                //ZNodeLogging.LogMessage("about to generate GUID");
                _referenceID = Guid.NewGuid().ToString();
            }

        }

        /// <summary>
        /// Performs the CyberCash registration function.
        /// </summary>
        /// <returns>The CyberCash status code for the operation.</returns>
        public int Register()
        {
            //ZNodeLoggingBase.LogMessage("Register: Make sure that the needed attributes are set");
            //Make sure that the needed attributes are set
            if (
                string.IsNullOrEmpty(_siteID) ||
                !(_Amount.HasValue) ||
                string.IsNullOrEmpty(_referenceID) ||
                string.IsNullOrEmpty(_sendKey) ||
                string.IsNullOrEmpty(_receiveKey)
              )
            {
                throw (new Exception("Cannot perform credit card register process.", new Exception("Required attributes are not set.")));
            }

            if (_RegistrationResponseCode != null)
            {
                throw (new Exception("Cannot perform credit card register process.", new Exception("Registration already done.")));
            }

            //ZnodeLogging.LogMessage("Register: Calculate the certification hash");
            //Calculate the certification hash
            string timeStamp = GetGmt();
            //ZnodeLogging.LogMessage("Register: Got GMT, creating Concatted string");

            string concattedString = string.Format("{0:f2}{1}{2}{3}", _Amount, _referenceID, _siteID, timeStamp);
            //ZnodeLogging.LogMessage("Register: Created Concatted string, computing hash with " + concattedString + " and " + _sendKey + " as parameters");
            string certification = ComputeHash(concattedString, _sendKey);

            //ZnodeLogging.LogMessage("Register: Build up the REST string we will send to iPay");
            //Build up the REST string we will send to iPay
            string postVars = string.Format("amount={0:f2}&referenceid={1}&siteid={2}&timeStamp={3}&certification={4}", _Amount,
                         _referenceID, _siteID, timeStamp, certification);

            //ZnodeLogging.LogMessage("Register: Post the request to the iPay server");
            //Post the request to the iPay server
            string result = HttpPost(GetiPayRegisterUrl(), postVars);

            //ZnodeLogging.LogMessage("Register: Put the result string into a collection for easy use");
            //Put the result string into a collection for easy use
            NameValueCollection resultCollection = ConvertResponseStringToNameValues(result);

            //ZnodeLogging.LogMessage("Register: Check the hash of the results so that you know you have a valid transmission of data");
            //Check the hash of the results so that you know you have a valid transmission of data
            string resultHashString = string.Format("{0}{1}{2}{3}", resultCollection["Redirect"],
                                resultCollection["ResponseCode"], resultCollection["TimeStamp"],
                                resultCollection["Token"]);

            string resultCertification = ComputeHash(resultHashString, _receiveKey);
            if (resultCertification != resultCollection["Certification"])
            {
                throw (new Exception("Invalid certification returned from credit card register process."));
            }

            //ZnodeLogging.LogMessage("Register: Get the ResponseCode from the registration.  If it's not a valid integer, then throw an exception.");
            //Get the ResponseCode from the registration.  If it's not a valid integer, then throw an exception.
            int registrationResponseCode;
            if (int.TryParse(resultCollection["ResponseCode"], out registrationResponseCode))
            {
                this._RegistrationResponseCode = registrationResponseCode;
            }
            else
            {
                throw (new Exception("Invalid Response Code received from credit card register process."));
            }

            //ZnodeLogging.LogMessage("Register: Save the Token and RedirectUrl if the registration succeeded.");
            //Save the Token and RedirectUrl if the registration succeeded.
            if (_RegistrationResponseCode == 0)
            {
                _token = resultCollection["Token"];
                _redirectURL = resultCollection["Redirect"] + "?token=" + _token;
            }
            else
            {
                _token = string.Empty;
                _redirectURL = string.Empty;
            }

            return registrationResponseCode;
        }

        /// <summary> 
        /// Performs the CyberCash result function.
        /// </summary>
        /// <returns>The CyberCash status code for the operation.</returns>
        public int Result()
        {

            //Make sure that the needed attributes are set
            if (
                string.IsNullOrEmpty(_siteID) ||
                !(_Amount.HasValue) ||
                string.IsNullOrEmpty(_sendKey) ||
                string.IsNullOrEmpty(_receiveKey) ||
                string.IsNullOrEmpty(_token)
              )
            {
                throw (new Exception("Cannot perform credit card result process.", new Exception("Required attributes are not set.")));
            }

            //ZnodeLogging.LogMessage("In iPayResult, Calc cert hash");

            //Calculate the certification hash
            string timeStamp = GetGmt();
            string concattedString = string.Format("{0}{1}{2}", _siteID, timeStamp, _token);
            string certification = ComputeHash(concattedString, _sendKey);

            string postVars = string.Format("siteid={0}&timestamp={1}&token={2}&certification={3}", _siteID, timeStamp, _token,
                                            certification);

            //ZnodeLogging.LogMessage("Posting to iPay");
            //Post the request to the CyberCash server
            string result = HttpPost(GetiPayResultUrl(), postVars);

            //ZnodeLogging.LogMessage("Store result in collection");
            //Put the result string into a collection for easy use
            NameValueCollection resultCollection = ConvertResponseStringToNameValues(result);

            //Get the ResponseCode from the result.  If it's not a valid integer, then throw an exception.
            int resultResponseCode;
            if (int.TryParse(resultCollection["ResponseCode"], out resultResponseCode))
            {
                this._ResultResponseCode = resultResponseCode;
            }
            else
            {
                throw (new Exception("Invalid Response Code received from credit card result process."));
            }

            if (resultResponseCode != 0)
            {
                return resultResponseCode;
            }

            //ZnodeLogging.LogMessage("Check hash of results");

            //Check the hash of the results so that you know you have a valid transmission of data
            concattedString = string.Format("{0}{1}{2}", resultCollection["ResponseCode"], resultCollection["TimeStamp"],
                                            resultCollection["TransactionID"]);

            string resultCertification = ComputeHash(concattedString, _receiveKey);
            if (resultCertification != resultCollection["Certification"])
            {
                throw (new Exception("Invalid certification returned from credit card result process."));
            }

            //ZnodeLogging.LogMessage("Save the TransactionID");

            //Save the Token and RedirectUrl if the registration succeeded.
            _TransactionId = (_ResultResponseCode == 0) ? resultCollection["TransactionID"] : string.Empty;

            return resultResponseCode;
        }

        /// <summary>
        /// Performs the CyberCash capture function.
        /// </summary>
        /// <returns>The CyberCash status code for the operation.</returns>
        public int Capture()
        {
            //Make sure that the needed attributes are set
            if (
                string.IsNullOrEmpty(_siteID) ||
                !(_Amount.HasValue) ||
                string.IsNullOrEmpty(_sendKey) ||
                string.IsNullOrEmpty(_receiveKey) ||
                string.IsNullOrEmpty(_token) ||
                string.IsNullOrEmpty(_bannerAccount)
              )
            {
                throw (new Exception("Cannot perform credit card capture process.", new Exception("Required attributes are not set.")));
            }

            //ZnodeLogging.LogMessage("In iPay Capture, Calc Cert hash");

            //Calculate the certification hash
            string timeStamp = GetGmt();

            string concattedString = string.Format("{0}{1:f2}1{2}{3}{4}", _bannerAccount, _Amount, _siteID, timeStamp, _token);
            //ZnodeLogging.LogMessage(string.Format("Calcing hash with {0} and {1}", concattedString, _sendKey));
            string certification = ComputeHash(concattedString, _sendKey);
            string postVars = string.Format(
                "siteid={0}&token={1}&timestamp={2}&numaccounts=1&account1={3}&amount1={4:f2}&certification={5}",
                _siteID, _token, timeStamp, _bannerAccount, _Amount, certification);

            //ZnodeLogging.LogMessage("Banner account is '" + _bannerAccount + "'");

            //ZnodeLogging.LogMessage("Posting capture message to iPay");
            //Post the request to the CyberCash server
            string result = HttpPost(GetiPayCaptureUrl(), postVars);

            //ZnodeLogging.LogMessage("put results in colleciton");
            //Put the result string into a collection for easy use
            NameValueCollection resultCollection = ConvertResponseStringToNameValues(result);

            //Get the ResponseCode from the capture.  If it's not a valid integer, then throw an exception.
            int captureResponseCode;
            if (int.TryParse(resultCollection["ResponseCode"], out captureResponseCode))
            {
                this._CaptureResponseCode = captureResponseCode;
            }
            else
            {
                throw (new Exception("Invalid Response Code received from credit card capture process."));
            }

            if (captureResponseCode != 0)
            {
                return captureResponseCode;
            }

            //ZnodeLogging.LogMessage("calc result hash from capture");
            //Check the hash of the results so that you know you have a valid transmission of data
            string resultHashString = string.Format("{0}{1}{2}{3}", resultCollection["CaptureAmount"],
                                            resultCollection["ResponseCode"], resultCollection["TimeStamp"],
                                            resultCollection["TransactionID"]);

            //ZnodeLogging.LogMessage(string.Format("Calcing hash with {0} and {1}", resultHashString, _receiveKey));
            string resultCertification = ComputeHash(resultHashString, _receiveKey);

            //ZnodeLogging.LogMessage("Made it past ComputeHash with this result: " + resultCertification);
            //ZnodeLogging.LogMessage("Passed result: " + resultCollection["Certification"].ToString() + " to compare");

            if (resultCertification != resultCollection["Certification"])
            {
                StringBuilder message = new StringBuilder();
                message.Append("Expected cert '" + resultCertification);
                message.Append("' but got '" + resultCollection["Certification"]);
                message.Append("' Whole response was '" + result);
                throw new Exception(message.ToString());
                //throw (new Exception("Invalid certification returned from credit card capture process."));
            }

            return captureResponseCode;
        }

        #region private methods

        /// <summary>
        /// Returns the current time (GMT) as a string.
        /// </summary>
        /// <returns>A string representing the current time (GMT).</returns>
        private static string GetGmt()
        {
            return DateTime.Now.ToUniversalTime().ToString("MM-dd-yyyy HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo);
        }

        /// <summary>
        /// This function computes the HMAC-SHA1 hash of a string.
        /// </summary>
        /// <param name="ClearString">The string to be encrypted.</param>
        /// <param name="HexKey">A string that contains the key as a series of hexadecimal characters.</param>
        /// <returns>The HMAC-SHA1 Hash</returns>
        private static string ComputeHash(string ClearString, string HexKey)
        {
            byte[] key = ConvertHexStringToByteArray(HexKey);
            byte[] data = Encoding.UTF8.GetBytes(ClearString);
            System.Security.Cryptography.HMACSHA1 hmac = new HMACSHA1(key);
            System.Security.Cryptography.CryptoStream cs = new CryptoStream(Stream.Null, hmac, CryptoStreamMode.Write);

            cs.Write(data, 0, data.Length);
            cs.Close();

            return ConvertByteArrayToHexString(hmac.Hash);
        }

        /// <summary>
        /// Helper function to convert a hexadecimal string to a byte array .
        /// </summary>
        /// <param name="HexString">The hexadecimal string to be converted.</param>
        /// <returns>A byte array with the encoded hexadecimal values from the string.</returns>
        private static byte[] ConvertHexStringToByteArray(string HexString)
        {
            if (HexString.Length % 2 == 1)
            {
                throw (new Exception("Hexadecimal string cannot be converted to a byte[] because it contains an odd number of characters."));
            }

            int hexWords = ((HexString.Length / 2));
            byte[] bytes = new byte[hexWords];
            for (int i = 0; i < hexWords; i++)
            {
                bytes[i] = byte.Parse(HexString.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
            }
            return bytes;
        }

        /// <summary>
        /// Helper function to convert a byte array to a string.
        /// </summary>
        /// <param name="Bytes">The byte array to be converted.</param>
        /// <returns>A string with the hexadecimal encoded values for byte array.</returns>
        private static string ConvertByteArrayToHexString(IEnumerable<byte> Bytes)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in Bytes)
            {
                sb.Append(b.ToString("X2"));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Sends a web request to a URL using HTTP POST and gets the response string.
        /// </summary>
        /// <param name="url">The URL to send the web request.</param>
        /// <param name="postValues">The values you want to send to the web request.</param>
        /// <returns>The response string.</returns>
        private static string HttpPost(string url, string postValues)
        {
            //Set up the request

            HttpContext.Current.Trace.Write("Posting to " + url + " values " + postValues);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "application/x-www-form-urlencoded";
            request.Method = WebRequestMethods.Http.Post;
            request.ContentLength = postValues.Length;

            //Send the request

            StreamWriter sw = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
            sw.Write(postValues);
            sw.Close();



            //Get the response
            string result;
            try
            {
                StreamReader sr = new StreamReader(request.GetResponse().GetResponseStream());
                result = sr.ReadToEnd();
                sr.Close();
            }
            catch (Exception ex)
            {
                ex.ToString(); // Gets rid of the unused variable warning
                result = null;
            }

            return result;
        }

        /// <summary>
        /// Takes the response string generated by iPay and breaks up the values.
        /// </summary>
        /// <param name="NameValues">The response string from iPay.</param>
        /// <returns></returns>
        private static NameValueCollection ConvertResponseStringToNameValues(string NameValues)
        {
            NameValueCollection values = new NameValueCollection();
            const char cr = '\r';
            const char lf = '\n';
            string[] pairs = NameValues.Split(cr, lf);

            string[] pairArray;

            foreach (string pair in pairs)
            {
                pairArray = pair.Split('=');

                if (pairArray.Length == 2)
                {
                    values.Add(pairArray[0], pairArray[1]);
                }
            }

            return values;
        }

        /// <summary>
        /// Takes a iPay return code (int) and returns a string
        ///  that defines the error a little better.
        /// </summary>
        /// <param name="ResponseCode">The iPay response code</param>
        /// <returns>A string that defines the error.</returns>
        public static string ResponseCodeDescription(int ResponseCode)
        {
            string description;
            switch (ResponseCode)
            {
                case 0:
                    description = "Success";
                    break;
                case 1:
                    description = "Request Attribute Parse Error";
                    break;
                case 2:
                    description = "Certification Match Failure";
                    break;
                case 3:
                    description = "Site Note Active";
                    break;
                case 4:
                    description = "Tender Mismatch";
                    break;
                case 5:
                    description = "Transaction Token Expired";
                    break;
                case 6:
                    description = "Authorization Failure";
                    break;
                case 7:
                    description = "Token Not Found";
                    break;
                case 8:
                    description = "Capture Failure";
                    break;
                case 9:
                    description = "TimeStamp offset too large";
                    break;
                case 10:
                    description = "Mismatched Auth Amount";
                    break;
                case 100:
                    description = "System Error";
                    break;
                default:
                    description = "Unknown Response Code";
                    break;
            }

            return description;
        }


        private string GetiPayRegisterUrl()
        {
            string mykey = _isTestMode ? iPayTestRegisterUrlKey : iPayRegisterUrlKey;
            return WebConfigurationManager.AppSettings[mykey].Trim();
        }

        private string GetiPayResultUrl()
        {
            string mykey = _isTestMode ? iPayTestResultUrlKey : iPayResultUrlKey;
            return WebConfigurationManager.AppSettings[mykey].Trim();
        }

        private string GetiPayCaptureUrl()
        {
            string mykey = _isTestMode ? iPayTestCaptureUrlKey : iPayCaptureUrlKey;
            return WebConfigurationManager.AppSettings[mykey].Trim();
        }

        #endregion

    }
}
