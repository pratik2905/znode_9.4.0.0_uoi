﻿using Znode.Engine.Api.Client;
using Znode.Engine.Api.Model.Custom;

namespace Znode.Api.Client.Custom.Clients
{
    public interface ICustomiPayClient : IBaseClient
    {
        iPayRecordModel GetIllinoisiPay(string ReferenceId);
        iPayRecordModel CreateIllinoisiPay(iPayRecordModel model);
        iPayRecordModel UpdateIllinoisiPay(iPayRecordModel model);
        iPayRecordModel GetIllinoisiPayByToken(string Token);
    }
}
