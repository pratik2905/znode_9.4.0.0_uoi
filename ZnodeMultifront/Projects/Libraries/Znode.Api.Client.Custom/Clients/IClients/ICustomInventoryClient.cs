﻿using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Model.Custom;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Client.Custom.Clients
{
    public interface ICustomInventoryClient: IBaseClient
    {
         bool ImportData(ImportModel model);

        #region Digital Asset
        /// <summary>
        /// Get downloadable product key list.
        /// </summary>
        /// <param name="expands">Expands for downloadable product  </param>
        /// <param name="filters">Filters for downloadable product</param>
        /// <param name="sorts">Sorts for downloadable product</param>
        /// <param name="pageIndex">pageIndex</param>
        /// <param name="pageSize">pageSize</param>
        /// <returns>DownloadableProductKeyListModel</returns>
        CustomDownloadableProductKeyListModel CustomGetDownloadableProductKeys(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
        #endregion
    }
}
