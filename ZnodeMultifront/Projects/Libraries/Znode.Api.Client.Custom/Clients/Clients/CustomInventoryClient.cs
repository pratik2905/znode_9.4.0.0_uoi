﻿using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Api.Client.Custom.Endpoints;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Model.Custom;
using Znode.Engine.Api.Model.Custom.Responses;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Client.Custom.Clients
{
    public class CustomInventoryClient : BaseClient, ICustomInventoryClient
    {
        #region "Properties"
        public int LoginAs
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public bool RefreshCache
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public int UserId
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion

        #region "Public methods"
        public bool ImportData(ImportModel model)
        {
            string endpoint = CustomInventoryEndPoint.ImportData();
            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PostResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return response.IsSuccess;
        }

        #region Digital Asset 
        //Get downloadable product keys associated to product.
        public virtual CustomDownloadableProductKeyListModel CustomGetDownloadableProductKeys(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            string endpoint = CustomInventoryEndPoint.GetDownloadableProductKeyList();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            ApiStatus status = new ApiStatus();
            CustomDownloadableProductKeyListResponse response = GetResourceFromEndpoint<CustomDownloadableProductKeyListResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            CustomDownloadableProductKeyListModel downloadableProductKeyListModel = new CustomDownloadableProductKeyListModel { CustomDownloadableProductKeys = response?.CustomDownloadableProductKeys };
            downloadableProductKeyListModel.MapPagingDataFromResponse(response);

            return downloadableProductKeyListModel;
        }
        #endregion
        #endregion
    }
}
