﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Api.Client.Custom.Endpoints.Custom;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Model.Custom;
using Znode.Engine.Api.Model.Custom.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Api.Client.Custom.Clients
{
    public class CustomiPayClient : BaseClient, ICustomiPayClient
    {
        public iPayRecordModel GetIllinoisiPay(string ReferenceId)
        {
            //Create Endpoint to get the Custom Portal Details.
            string endpoint = CustomiPayEndPoint.GetiPayRecord(ReferenceId);
            
            ApiStatus status = new ApiStatus();
            CustomiPayRecordResponse response = GetResourceFromEndpoint<CustomiPayRecordResponse>(endpoint, status);

            //Check the status of response of Custom Portal Detail.
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return response?.iPayRecord;
            
        }
        public iPayRecordModel CreateIllinoisiPay(iPayRecordModel model)
        {
            //Create Endpoint to Custom Portal Detail.
            string endpoint = CustomiPayEndPoint.CreateiPayRecord();

            ApiStatus status = new ApiStatus();
            CustomiPayRecordResponse response = PostResourceToEndpoint<CustomiPayRecordResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            //Check the status of response of Custom Portal Detail.
            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);
            return response?.iPayRecord;
            
        }
        public iPayRecordModel UpdateIllinoisiPay(iPayRecordModel model)
        {
            //Create Endpoint to Custom Portal Detail.
            string endpoint = CustomiPayEndPoint.UpdateiPayRecord();

            ApiStatus status = new ApiStatus();
            CustomiPayRecordResponse response = PutResourceToEndpoint<CustomiPayRecordResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            //Check the status of response of Custom Portal Detail.
            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);
            return response?.iPayRecord;
        }
        public iPayRecordModel GetIllinoisiPayByToken(string Token)
        {
            //Create Endpoint to get the Custom Portal Details.
            string endpoint = CustomiPayEndPoint.GetiPayRecordByToken(Token);
            ApiStatus status = new ApiStatus();
            CustomiPayRecordResponse response = GetResourceFromEndpoint<CustomiPayRecordResponse>(endpoint, status);

            //Check the status of response of Custom Portal Detail.
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return response?.iPayRecord;
        }
    }
}
