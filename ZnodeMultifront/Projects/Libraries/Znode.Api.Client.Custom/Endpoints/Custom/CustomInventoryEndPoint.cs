﻿using Znode.Engine.Api.Client.Endpoints;

namespace Znode.Api.Client.Custom.Endpoints
{
    public class CustomInventoryEndPoint:BaseEndpoint
    {
        //Post and process import data
        public static string ImportData() => $"{ApiRoot}/custominventory/importdata";

        #region Digital Asset
        //Downloadable product key List endpoint
        public static string GetDownloadableProductKeyList() => $"{ApiRoot}/CustomInventory/getdownloadableproductkeylist";
        #endregion
    }
}
