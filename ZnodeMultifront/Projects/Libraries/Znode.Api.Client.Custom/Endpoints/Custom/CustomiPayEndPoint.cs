﻿using Znode.Engine.Api.Client.Endpoints;

namespace Znode.Api.Client.Custom.Endpoints.Custom
{
   public class CustomiPayEndPoint : BaseEndpoint
    {
        //IllinoisiPayModel GetIllinoisiPay(string ReferenceId);
        //IllinoisiPayModel CreateIllinoisiPay(IllinoisiPayModel model);
        //IllinoisiPayModel UpdateIllinoisiPay(string ReferenceId, IllinoisiPayModel model);
        //IllinoisiPayModel GetIllinoisiPayByToken(string Token);

        //Post and process import data
        public static string CreateiPayRecord() => $"{ApiRoot}/customipay/create";

        public static string UpdateiPayRecord() => $"{ApiRoot}/customipay/update";

        public static string GetiPayRecord(string ReferenceID) => $"{ApiRoot}/customipay/getipay/{ReferenceID}";

        public static string GetiPayRecordByToken(string Token) => $"{ApiRoot}/customipay/getbytoken/{Token}";


    }
}
