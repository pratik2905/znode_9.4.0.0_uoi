﻿using System.Web.Routing;
using System.Web.Mvc;

namespace Znode.Engine.WebStore
{
    public class CustomRouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            //Don't try to process calls to the Shib SP
            routes.IgnoreRoute("Shibboleth.sso/{*pathInfo}"); 
        }
    }
}