var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var CustomCheckout = /** @class */ (function (_super) {
    __extends(CustomCheckout, _super);
    function CustomCheckout() {
        return _super.call(this) || this;
    }
    CustomCheckout.prototype.Init = function () {
    };
    CustomCheckout.prototype.SubmitOrder = function () {
        Checkout.prototype.ShowLoader();
        //Set recipient name if recipient textbox is available
        Checkout.prototype.SaveRecipientNameAddressData('shipping', function (response) {
            Checkout.prototype.isPayMentInProcess = true;
            Checkout.prototype.HidePaymentLoader();
            if (!Checkout.prototype.IsCheckoutDataValid()) {
                Checkout.prototype.isPayMentInProcess = false;
                ZnodeBase.prototype.HideLoader();
            }
            else {
                if (!Checkout.prototype.ShippingErrorMessage()) {
                    Checkout.prototype.HideLoader();
                    Checkout.prototype.isPayMentInProcess = false;
                    return false;
                }
                if ($("#dynamic-allowesterritories").length > 0) {
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("AllowedTerritories"), "error", false, 0);
                    Checkout.prototype.isPayMentInProcess = false;
                    Checkout.prototype.HideLoader();
                    return false;
                }
                else {
                    var isNotGuest = (parseInt($('#hdnAnonymousUser').val()) > 0);
                    var paymentOptionId = $("input[name='PaymentOptions']:checked").attr("id");
                    var paymentType = Checkout.prototype.GetPaymentType(paymentOptionId);
                    var isApprovalRequired = "";
                    var isOABRequired = "";
                    if ($("input[name='PaymentOptions']:checked").length > 0) {
                        isApprovalRequired = $("input[name='PaymentOptions']:checked").attr("data-isApprovalRequired").toLowerCase();
                        isOABRequired = $("input[name='PaymentOptions']:checked").attr("data-isOABRequired").toLowerCase();
                    }
                    var isStoreLevelAppoverOn = $("#EnableApprovalRouting").val().toLowerCase();
                    var userType = ZnodeBase.prototype.GetParameterValues("mode");
                    if (userType == undefined) {
                        userType = "";
                    }
                    userType = (userType != "") ? userType.replace("#", "") : userType;
                    switch (paymentType.toLowerCase()) {
                        case "credit_card":
                            if (userType != "guest") {
                                if (Checkout.prototype.SetFlagForApprovalRouting(isApprovalRequired, isOABRequired, isStoreLevelAppoverOn)) {
                                    Checkout.prototype.SubmitForApproval();
                                }
                                else {
                                    Checkout.prototype.SubmitPayment();
                                }
                            }
                            else {
                                Checkout.prototype.SubmitPayment();
                            }
                            break;
                        case "cod":
                            $("#btnCompleteCheckout").prop("disabled", false);
                            $("#btnCompleteCheckout").show();
                            $('#txtPurchaseOrderNumber').val('');
                            if (userType != "guest") {
                                if (Checkout.prototype.SetFlagForApprovalRouting(isApprovalRequired, isOABRequired, isStoreLevelAppoverOn) && isNotGuest) {
                                    Checkout.prototype.SubmitForApproval();
                                }
                                else {
                                    Checkout.prototype.SubmitCheckOutForm();
                                }
                            }
                            else {
                                Checkout.prototype.SubmitCheckOutForm();
                            }
                            break;
                        case "ipay":
                            $("#btnCompleteCheckout").removeAttr("disabled");
                            $("#btnCompleteCheckout").show();
                            $('#txtPurchaseOrderNumber').val('');
                            if (userType != "guest") {
                                if (Checkout.prototype.SetFlagForApprovalRouting(isApprovalRequired, isOABRequired, isStoreLevelAppoverOn) && isNotGuest) {
                                    Checkout.prototype.SubmitForApproval();
                                }
                                else {
                                    CustomCheckout.prototype.SubmitCheckOutForm();
                                }
                            }
                            else {
                                CustomCheckout.prototype.SubmitCheckOutForm();
                            }
                            break;
                        default:
                            $("#btnCompleteCheckout").prop("disabled", false);
                            $("#btnCompleteCheckout").show();
                            // global data
                            if (Checkout.prototype.CheckValidPODocument()) {
                                if (Checkout.prototype.SetFlagForApprovalRouting(isApprovalRequired, isOABRequired, isStoreLevelAppoverOn)) {
                                    Checkout.prototype.SubmitForApproval();
                                }
                                else {
                                    Checkout.prototype.SubmitCheckOutForm();
                                }
                            }
                            else {
                                Checkout.prototype.HideLoader();
                                return false;
                            }
                            break;
                    }
                }
            }
        });
    };
    ;
    //Get all the selected values required to submit order.
    CustomCheckout.prototype.SetOrderFormData = function (data) {
        data["ShippingOptionId"] = $("input[name='ShippingOptions']:checked").val();
        data["PaymentSettingId"] = $("input[name='PaymentOptions']:checked").val();
        data["ShippingAddressId"] = $("#shipping-content").find("#AddressId").val();
        data["BillingAddressId"] = $("#billing-content").find("#AddressId").val();
        data["AdditionalInstruction"] = $("#AdditionalInstruction").val();
        data["PurchaseOrderNumber"] = $("#txtPurchaseOrderNumber").val();
        data["PODocumentName"] = $("#po-document-path").val();
        data["AccountNumber"] = $("#AccountNumber").val();
        data["ShippingMethod"] = $("#ShippingMethod").val();
        data["Total"] = $("#hdnTotalOrderAmount").val().replace(',', '.');
    };
    CustomCheckout.prototype.SubmitCheckOutForm = function () {
        var data = {};
        //Get all the selected values required to submit order.
        CustomCheckout.prototype.SetOrderFormData(data);
        //Create form to submit order.
        var form = Checkout.prototype.CreateForm(data);
        // submit form
        form.submit();
        form.remove();
    };
    CustomCheckout.prototype.ShippingOptions = function () {
        $("#loaderId").html(" <div class='loader-inner' style='margin:0 auto;text-align:center;padding:20px;'><img src= '../Content/Images/throbber.gif' alt= 'Loading' class='dashboard-loader' /></div>");
        Endpoint.prototype.ShippingOptions(true, function (response) {
            $("#loaderId").html("");
            $(".shipping-method").html(response);
            //Znode Custom Code UOFI-2: START
            CustomCheckout.prototype.DisableShippingForFreeShippingAndDownloadableProduct();
            //Znode Custom Code UOFI-2: END
        });
    };
    //Znode Custom Code UOFI-2: START
    CustomCheckout.prototype.DisableShippingForFreeShippingAndDownloadableProduct = function () {
        if ($("#cartFreeShipping").val() == "True" && $("#hdnIsFreeShipping").val() == "True") {
            $('input[name="ShippingOptions"]').prop('checked', false);
            $('input[name="ShippingOptions"]').next('label').addClass('disable-radio');
            $("#FreeShipping").attr("checked", "checked");
            var form = $("#form0");
            var shippingOptionId = $("#FreeShipping").val();
            var shippingAddressId = $("#shipping-content").find("#AddressId").val();
            var shippingCode = $("#FreeShipping").attr("data-shippingCode");
            $("#hndShippingclassName").val('ZnodeShippingCustom');
            if (shippingOptionId != null || shippingOptionId != undefined || shippingOptionId != "") {
                if (form.attr('action').match("shippingOptionId")) {
                    var url = form.attr('action').split('?')[0];
                    form.attr('action', "");
                    form.attr('action', url);
                }
                form.attr('action', form.attr('action') + "?shippingOptionId=" + shippingOptionId + "&shippingAddressId=" + shippingAddressId + "&shippingCode=" + shippingCode + "");
                form.submit();
            }
            $("#message-freeshipping").show();
        }
        else {
            $('input[name="ShippingOptions"]').next('label').removeClass('disable-radio');
            if ($("input[name='ShippingOptions']").length > 0)
                $("input[name='ShippingOptions']")[0].click();
        }
    };
    return CustomCheckout;
}(ZnodeBase));
//# sourceMappingURL=CustomCheckout.js.map