var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var BarcodeReader = /** @class */ (function (_super) {
    __extends(BarcodeReader, _super);
    function BarcodeReader() {
        var _this = _super.call(this) || this;
        _this._iptIndex = 0;
        _this._scanner = null;
        return _this;
    }
    BarcodeReader.prototype.InitiateBarcodeScanner = function (licenseKey, barcodeFormats, UIElement, callbackOnLoadMethod, callbackResultMethod) {
        var _this = this;
        var dbr = window['dbr'];
        //let barcodeFormats: string[] = ["ONED", "QR_CODE", "CODABAR"];
        if (dbr != null) {
            if (this._scanner != null) {
                this._scanner.onUnduplicatedRead = undefined;
                BarcodeReader.prototype.StopScanner();
            }
            dbr.licenseKey = licenseKey;
            dbr.BarcodeScanner.createInstance().then(function (s) {
                _this._scanner = s;
                _this._iptIndex = 0;
                _this._scanner.bAddSearchRegionCanvasToResult = true;
                var runtimeSettings = _this._scanner.getRuntimeSettings();
                barcodeFormats.forEach(function (item, index) {
                    if (index == 0) {
                        runtimeSettings.BarcodeFormatIds = BarcodeReader.prototype.GetBarcodeFormatCode(item, dbr);
                    }
                    else {
                        runtimeSettings.BarcodeFormatIds += BarcodeReader.prototype.GetBarcodeFormatCode(item, dbr);
                    }
                });
                // Only decode OneD and QR
                ////runtimeSettings.BarcodeFormatIds = dbr.EnumBarcodeFormat.OneD | dbr.EnumBarcodeFormat.QR_CODE | dbr.EnumBarcodeFormat.CODABAR;
                //runtimeSettings.BarcodeFormatIds += dbr.EnumBarcodeFormat.OneD;
                //runtimeSettings.BarcodeFormatIds += dbr.EnumBarcodeFormat.QR_CODE;
                //runtimeSettings.BarcodeFormatIds += dbr.EnumBarcodeFormat.CODABAR;
                _this._scanner.updateRuntimeSettings(runtimeSettings);
                if (dbr.BarcodeReader.isLoaded()) {
                    callbackOnLoadMethod(_this._scanner);
                    console.log('Is the loading completed? ' + dbr.BarcodeReader.isLoaded());
                    console.log('indes? ' + _this._iptIndex);
                    _this._scanner.UIElement = document.getElementById(UIElement);
                    _this._scanner.onFrameRead = function (results) { };
                    _this._scanner.onUnduplicatedRead = function (txt, result) {
                        console.log('result? ' + result);
                        callbackResultMethod(txt, result);
                        if (3 == ++_this._iptIndex) {
                            _this._scanner.onUnduplicatedRead = undefined;
                            BarcodeReader.prototype.StopScanner();
                        }
                    };
                }
            });
        }
    };
    BarcodeReader.prototype.StartScanner = function (callbackSuccess, callbackFailed) {
        if (this._scanner != null) {
            this._scanner.show().then(function (_) { callbackSuccess(); }).catch(function (error) {
                callbackFailed(error);
            });
        }
    };
    BarcodeReader.prototype.StartScannerOnElement = function (UIElement, callbackSuccess, callbackFailed) {
        if (this._scanner != null) {
            this._scanner.UIElement = document.getElementById(UIElement);
            BarcodeReader.prototype.StartScanner(callbackSuccess, callbackFailed);
        }
    };
    BarcodeReader.prototype.StopScanner = function () {
        if (this._scanner != null) {
            this._scanner.stop();
            this._scanner.hide();
        }
    };
    BarcodeReader.prototype.PauseScanner = function () {
        if (this._scanner != null) {
            this._scanner.pause();
        }
    };
    BarcodeReader.prototype.GetBarcodeFormatCode = function (Code, dbr) {
        var barcodeFormatCode;
        switch (Code) {
            case "ONED": {
                barcodeFormatCode = dbr.EnumBarcodeFormat.OneD;
                break;
            }
            case "QR_CODE": {
                barcodeFormatCode = dbr.EnumBarcodeFormat.QR_CODE;
                break;
            }
            case "CODABAR": {
                barcodeFormatCode = dbr.EnumBarcodeFormat.CODABAR;
                break;
            }
            default: {
                barcodeFormatCode = dbr.EnumBarcodeFormat.QR_CODE;
                break;
            }
        }
        return barcodeFormatCode;
    };
    return BarcodeReader;
}(ZnodeBase));
//# sourceMappingURL=BarcodeReader.js.map