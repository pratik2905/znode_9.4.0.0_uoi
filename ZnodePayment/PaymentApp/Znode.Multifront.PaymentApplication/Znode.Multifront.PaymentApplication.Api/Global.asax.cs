﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Swashbuckle.Application;
using System.Linq;
using System.Configuration;
using System.Web;

namespace Znode.Multifront.PaymentApplication.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            string buildVersion = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["SwaggerBuildVersion"]) ? $"Znode {ConfigurationManager.AppSettings["SwaggerBuildVersion"]}" : "Znode";
            GlobalConfiguration.Configuration.EnableSwagger(c =>
            {
                c.SingleApiVersion(buildVersion, "Znode Payment Web API");
                c.IncludeXmlComments(GetXmlCommentsPath());
                c.ResolveConflictingActions(x => x.First());
            }).EnableSwaggerUi(c => { c.DisableValidator(); });
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperConfig.Execute();
        }

        protected void Application_BeginRequest()
        {
            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
            {
                AddResponseHeader();
            }
        }

        protected void AddResponseHeader()
        {
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept, X-Authorization-Token, X-HTTP-Method-Override,Authorization");
            HttpContext.Current.Response.End();
        }

        protected static string GetXmlCommentsPath() 
            => $@"{System.AppDomain.CurrentDomain.BaseDirectory}\bin\Znode.Multifront.PaymentApplication.Api.XML";
    }
}
